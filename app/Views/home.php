<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <a href="login">Login</a>
    <h3>Home</h3>
    <h4>Cari Data R7</h4>
    <!-- Handle Message Error -->
    <?php
        $inputs = session()->getFlashdata('inputs');
        $errors = session()->getFlashdata('errors');
        $hasil = session()->getFlashdata('hasil');
        $nomor_r7Input =  isset($inputs['nomor_r7']) ? $inputs['nomor_r7'] : '';

        $nomor_r7Error =  isset($errors['nomor_r7']) ? $errors['nomor_r7'] : '';

        $hasilCari = isset($hasil['response']) ? $hasil['response'] : '';
    ;?>
    <form action="lacak" method="post">
        <input type="text" name="nomor_r7" id="nomor_r7" placeholder="Nomor R7" value="<?=$nomor_r7Input;?>" required>
        <p><?=$nomor_r7Error;?></p>
        <button type="submit">Cari!</button>
    </form>

    <?php if ($hasilCari != ''): ?>
        <p>
            <?=json_encode($hasilCari);?>
        </p>
    <?php endif;?>
</body>
</html>