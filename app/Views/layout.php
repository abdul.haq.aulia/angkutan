<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Dashboard</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="<?= base_url('assets/adminlte/plugins/fontawesome-free/css/all.min.css'); ?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url('assets/adminlte/dist/css/adminlte.min.css'); ?>">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- Datatable -->
    <?php if(uri_string() == 'dashboard' || uri_string() == 'dashboard/add' || uri_string() == 'dashboard/report' || uri_string() == 'dashboard/trayek' || uri_string() == 'dashboard/users'): ?>
    <link rel="stylesheet" href="<?=base_url('assets/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css');?>">
    <link rel="stylesheet" href="<?=base_url('assets/adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css');?>">
    <?php endif;?>
    <?= $this->renderSection('style') ?>
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="index3.html" class="brand-link">
                <img src="<?= base_url('assets/adminlte/dist/img/AdminLTELogo.png'); ?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light">R7</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="<?= base_url('assets/adminlte/dist/img/user2-160x160.jpg'); ?>" class="img-circle elevation-2" alt="User Image">
                    </div>
                    <div class="info">
                        <a href="#" class="d-block"><?= session()->get('nama'); ?></a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <?php 
                        $url    = explode("/", uri_string());
                        $first  = isset($url[0]) ? $url[0].'/' : '';
                        $second = isset($url[1]) ? $url[1].'/' : '';
                        $third  = isset($url[2]) ? $url[2].'/' : '';
                        $urlFull = $first.$second.$third;
                        $half = $first.$second;
                    ;?>
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                        <li class="nav-item">
                            <a href="<?=site_url('dashboard');?>" class="nav-link <?=($urlFull == 'dashboard/') ? 'active': '';?>">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                    Dashboard
                                </p>
                            </a>
                        </li>
                        <?php if(session()->get('level') == '2'):?>
                        <li class="nav-item">
                            <a href="<?=site_url('dashboard/add');?>" class="nav-link <?=($urlFull == 'dashboard/add/' || $second == 'edit/') ? 'active': '';?>">
                                <i class="nav-icon fas fa-qrcode"></i>
                                <p>
                                    Input Data R7
                                </p>
                            </a>
                        </li>
                        <?php endif;?>
                        <?php if(session()->get('level') == '1'):?>
                        <li class="nav-item">
                            <a href="<?=site_url('dashboard/users');?>" class="nav-link <?=($urlFull == 'dashboard/users/') ? 'active': '';?>">
                                <i class="nav-icon fas fa-users"></i>
                                <p>
                                    Users
                                </p>
                            </a>
                        </li>
                        <li class="nav-item has-treeview  <?=($urlFull == 'dashboard/trayek/' || $urlFull == 'dashboard/trayek/urutan/') ? 'menu-open': '';?>">
                            <a href=href="<?=site_url('dashboard/trayek');?>" class="nav-link <?=($urlFull == 'dashboard/trayek/' || $urlFull == 'dashboard/trayek/urutan/' || $half == 'dashboard/trayek/') ? 'active': '';?>">
                                <i class="nav-icon fas fa-route"></i>
                                <p>
                                    Trayek Angkutan
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview ml-3">
                                <li class="nav-item">
                                    <a href="<?=site_url('dashboard/trayek');?>" class="nav-link <?=($urlFull == 'dashboard/trayek/') ? 'active': '';?>">
                                    <i class="nav-icon fas fa-route"></i>
                                    <p>Trayek</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?=site_url('dashboard/trayek/urutan');?>" class="nav-link <?=($urlFull == 'dashboard/trayek/urutan/' ) ? 'active': '';?>">
                                    <i class="nav-icon fas fa-route"></i>
                                    <p>Setting Urutan</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview  <?=($urlFull == 'dashboard/refferensi/mobil/' || $urlFull == 'dashboard/refferensi/base/' || $urlFull == 'dashboard/refferensi/regional/' || $urlFull == 'dashboard/refferensi/nodes/' || $half == 'dashboard/editMobil/' || $half == 'dashboard/editHomeBase/' || $half == 'dashboard/editRegional/' || $half == 'dashboard/editNodes/') ? 'menu-open': '';?>">
                            <a href="<?=site_url('dashboard/refferensi');?>" class="nav-link <?=($urlFull == 'dashboard/refferensi/mobil/' || $urlFull == 'dashboard/refferensi/base/' || $urlFull == 'dashboard/refferensi/regional/' || $urlFull == 'dashboard/refferensi/nodes/' || $half == 'dashboard/editMobil/' || $half == 'dashboard/editHomeBase/' || $half == 'dashboard/editRegional/' || $half == 'dashboard/editNodes/') ? 'active': '';?>">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                    Refferensi
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview ml-3">
                                <li class="nav-item">
                                    <a href="<?=site_url('dashboard/refferensi/mobil');?>" class="nav-link <?=($urlFull == 'dashboard/refferensi/mobil/' || $half == 'dashboard/editMobil/') ? 'active': '';?>">
                                    <i class="fas fa-truck-moving nav-icon"></i>
                                    <p>Mobil</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?=site_url('dashboard/refferensi/base');?>" class="nav-link <?=($urlFull == 'dashboard/refferensi/base/' || $half == 'dashboard/editHomeBase/') ? 'active': '';?>">
                                    <i class="fas fa-thumbtack nav-icon"></i>
                                    <p>Home Base</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?=site_url('dashboard/refferensi/regional');?>" class="nav-link <?=($urlFull == 'dashboard/refferensi/regional/' || $half == 'dashboard/editRegional/') ? 'active': '';?>">
                                    <i class="fas fa-building nav-icon"></i>
                                    <p>Regional</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?=site_url('dashboard/refferensi/nodes');?>" class="nav-link <?=($urlFull == 'dashboard/refferensi/nodes/' || $half == 'dashboard/editNodes/') ? 'active': '';?>">
                                    <i class="fas fa-warehouse nav-icon"></i>
                                    <p>Nodes</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php endif;?>
                        <li class="nav-item">
                            <a href="<?=site_url('dashboard/report');?>" class="nav-link <?=($urlFull == 'dashboard/report/') ? 'active': '';?>">
                                <i class="nav-icon fas fa-chart-bar"></i>
                                <p>
                                    Laporan
                                </p>
                            </a>
                        </li>
                        <li class="nav-header">ACCOUNT</li>
                        <li class="nav-item">
                            <a href="<?=site_url('logout');?>" class="nav-link">
                                <i class="nav-icon fas fa-sign-out-alt"></i>
                                <p>
                                    Logout
                                </p>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <?= $this->renderSection('content') ?>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
            <div class="p-3">
                <h5>Title</h5>
                <p>Sidebar content</p>
            </div>
        </aside>
        <!-- /.control-sidebar -->

        <!-- Main Footer -->
        <footer class="main-footer">
            <!-- To the right -->
            <div class="float-right d-none d-sm-inline">
                Anything you want
            </div>
            <!-- Default to the left -->
            <strong>Copyright &copy; 2020 <a href="mailto:abdul.haq.aulia@gmail.com">Moh. Abdul Haq Aulia</a>.</strong> All rights reserved.
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    <!-- jQuery -->
    <script src="<?= base_url('assets/adminlte/plugins/jquery/jquery.min.js'); ?>"></script>
    <!-- Bootstrap 4 -->
    <script src="<?= base_url('assets/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?= base_url('assets/adminlte/dist/js/adminlte.min.js'); ?>"></script>
    <!-- Datatable -->
    <?php if(uri_string() == 'dashboard' || uri_string() == 'dashboard/add' || uri_string() == 'dashboard/report' || uri_string() == 'dashboard/trayek' || uri_string() == 'dashboard/users'): ?>
    <script src="<?=base_url('assets/adminlte/plugins/datatables/jquery.dataTables.min.js');?>"></script>
    <script src="<?=base_url('assets/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js');?>"></script>
    <script src="<?=base_url('assets/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js');?>"></script>
    <script src="<?=base_url('assets/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js');?>"></script>
    <script>
        $(function () {
            $('#datar7').DataTable({
                "responsive": true,
                "autoWidth": false,
            });
        });
    </script>
    <?php endif;?>
    <?php if(session()->get('level') == '2'):?>
        <script>
            var timeoutInMiliseconds = 5 * 60 * 1000;
            var timeoutId; 
            
            function startTimer() { 
                // window.setTimeout returns an Id that can be used to start and stop a timer
                timeoutId = window.setTimeout(doInactive, timeoutInMiliseconds)
            }
            
            function doInactive() {
                var r = confirm("Anda dile terlalu lama, apakah ingin keluar?");
                if (r == true) {
                    window.location = '/logout'
                } else {
                    setupTimers();
                }
                // does whatever you need it to actually do - probably signs them out or stops polling the server for info
            }

            function resetTimer() { 
                window.clearTimeout(timeoutId)
                startTimer();
            }
            
            function setupTimers () {
                document.addEventListener("mousemove", resetTimer, false);
                document.addEventListener("mousedown", resetTimer, false);
                document.addEventListener("keypress", resetTimer, false);
                document.addEventListener("touchmove", resetTimer, false);
                
                startTimer();
            }

            $(function () {
                setupTimers();
            });
        </script>
    <?php endif?>
    <?= $this->renderSection('javascript') ?>
</body>

</html>