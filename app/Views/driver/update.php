<?= $this->extend('layout') ?>

<?= $this->section('content') ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Input Data R7</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><a href="dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="<?=site_url('dashboard/add');?>">Add R7</a></li>
                        <li class="breadcrumb-item active">Add Data R7</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                <?php
                    $inputs = session()->getFlashdata('inputs');
                    $errors = session()->getFlashdata('errors');
                    $pesan = session()->getFlashdata('pesan');
                    $pesanError = session()->getFlashdata('pesan_error');
                    $nomorR7Input =  isset($inputs['nomor']) ? $inputs['nomor'] : '';
                    $jumlahInput =  isset($inputs['jumlah']) ? $inputs['jumlah'] : '';
                    $beratInput =  isset($inputs['berat']) ? $inputs['berat'] : '';
                    $trayekInput =  isset($inputs['trayek']) ? $inputs['trayek'] : '';

                    $nomorR7Error =  isset($errors['nomor']) ? $errors['nomor'] : '';
                    $jumlahError =  isset($errors['jumlah']) ? $errors['jumlah'] : '';
                    $beratError =  isset($errors['berat']) ? $errors['berat'] : '';
                    $trayekError =  isset($errors['trayek']) ? $errors['trayek'] : '';

                    if ($nomorR7Input != '' || isset($data->nomor)) {
                        $nomorR7Input = $data->nomor;
                    }

                    if ($jumlahInput != '' || isset($data->jumlah)) {
                        $jumlahInput = $data->jumlah;
                    }

                    if ($beratInput != '' || isset($data->berat)) {
                        $beratInput = $data->berat;
                    }

                    if ($trayekInput != '' || isset($data->kode_trayek)) {
                        $trayekInput = $data->kode_trayek;
                    }
                ;?>
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Input Data R7</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" action="<?=site_url('dashboard/updateR7');?>" method="POST">
                            <div class="card-body">
                                <?php if($pesan): ?>
                                    <div class="alert alert-success" role="alert">
                                        <?=$pesan;?>
                                    </div>
                                <?php endif;?>
                                <?php if($pesanError): ?>
                                    <div class="alert alert-danger" role="alert">
                                        <?=$pesanError;?>
                                    </div>
                                <?php endif;?>
                                <div class="form-group">
                                    <label for="inputR7">Nomor R7</label>
                                    <input type="text" name="nomor" class="form-control" id="inputR7" placeholder="Nomor R7" value="<?=$nomorR7Input;?>" readonly>
                                    <?php if($nomorR7Error != '') : ?>
                                        <small id="nomorR7Helper" class="form-text text-danger"><?=$nomorR7Error;?></small>
                                    <?php endif;?>
                                </div>
                                <div class="form-group">
                                    <label for="inputTrayek">Trayek</label>
                                    <select id="inputTrayek" name="trayek" class="form-control">
                                        <option value="" selected disabled>PILIH</option>
                                        <?php foreach($trayek->getResult() as $item) : ?>
                                            <option value="<?=$item->kode;?>" ><?="$item->trayek_awal -> $item->trayek_akhir";?></option>
                                        <?php endforeach;?>
                                    </select>
                                    <?php if($trayekError != '') : ?>
                                        <small id="trayekHelper" class="form-text text-danger"><?=$trayekError;?></small>
                                    <?php endif;?>
                                </div>
                                <div class="form-group">
                                    <label for="inputJumlah">Jumlah Kantong</label>
                                    <input type="number" name="jumlah" class="form-control" id="inputJumlah" placeholder="Jumlah Kantong" value="<?=$jumlahInput;?>">
                                    <?php if($jumlahError != '') : ?>
                                        <small id="jumlahHelper" class="form-text text-danger"><?=$jumlahError;?></small>
                                    <?php endif;?>
                                </div>
                                <div class="form-group">
                                    <label for="inputBerat">Total Berat</label>
                                    <input type="number" name="berat" class="form-control" id="inputBerat" placeholder="Total Berat" value="<?=$beratInput;?>">
                                    <?php if($beratError != '') : ?>
                                        <small id="beratHelper" class="form-text text-danger"><?=$beratError;?></small>
                                    <?php endif;?>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>

<?php if($trayekInput != ''): ?>
    <script>
        var trayek = document.getElementById('inputTrayek')
        setTimeout(() => {
            trayek.value = "<?=$trayekInput;?>"
        }, 500);
    </script>
<?php endif;?>
<?= $this->endSection() ?>