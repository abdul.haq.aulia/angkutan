<?= $this->extend('layout') ?>

<?= $this->section('content') ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Input Data R7</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><a href="dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item active">Add Data R7</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
        <div class="row">
                <div class="col">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Data Trayek Angkutan</h3>
                        </div>
                        <div class="card-body">
                            <div class="alert alert-info" role="alert">
                                Untuk Akses lebih Mudah memilih trayek, silahkan Klik tombol Trayek untuk memasukkan entri R7, atau anda dapat melakukan secara manual di bagian Input Data R7
                            </div>
                            <div class="angkutan-berangkat">
                                <h4>Rute Berangkat</h4>
                                <?php foreach($trayek->getResult() as $item) : ?>
                                    <?php if($item->keberangkatan == 'BERANGKAT'): ;?>
                                    <div class="row my-2">
                                        <div class="col-md-12">
                                            <button class="btn btn-outline-primary btn-block" onclick="setTrayek('<?=$item->kode;?>')"><?= $item->trayek_awal ." - ". $item->trayek_akhir  . " [ " . $item->keterangan ."]" ;?></button>
                                        </div>
                                    </div>
                                    <?php endif;?>
                                <?php endforeach;?>
                            </div>

                            <div class="angkutan-berangkat">
                                <h4>Rute Pulang</h4>
                                <?php foreach($trayek->getResult() as $item) : ?>
                                    <?php if($item->keberangkatan == 'PULANG'): ;?>
                                        <div class="row my-2">
                                            <div class="col-md-12">
                                                <button class="btn btn-outline-primary btn-block" onclick="setTrayek('<?=$item->kode;?>')"><?= $item->trayek_awal ." - ". $item->trayek_akhir . " [ " . $item->keterangan ."]" ;?></button>
                                            </div>
                                        </div>
                                    <?php endif;?>
                                <?php endforeach;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                <?php
                    $inputs = session()->getFlashdata('inputs');
                    $errors = session()->getFlashdata('errors');
                    $pesan = session()->getFlashdata('pesan');
                    $pesanError = session()->getFlashdata('pesan_error');
                    $nomorR7Input =  isset($inputs['nomor']) ? $inputs['nomor'] : '';
                    $jumlahInput =  isset($inputs['jumlah']) ? $inputs['jumlah'] : '';
                    $beratInput =  isset($inputs['berat']) ? $inputs['berat'] : '';
                    $trayekInput =  isset($inputs['trayek']) ? $inputs['trayek'] : '';

                    $nomorR7Error =  isset($errors['nomor']) ? $errors['nomor'] : '';
                    $jumlahError =  isset($errors['jumlah']) ? $errors['jumlah'] : '';
                    $beratError =  isset($errors['berat']) ? $errors['berat'] : '';
                    $trayekError =  isset($errors['trayek']) ? $errors['trayek'] : '';
                ;?>
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Input Data R7</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" action="addR7" method="POST">
                            <div class="card-body">
                                <?php if($pesan): ?>
                                    <div class="alert alert-success" role="alert">
                                        <?=$pesan;?>
                                    </div>
                                <?php endif;?>
                                <?php if($pesanError): ?>
                                    <div class="alert alert-danger" role="alert">
                                        <?=$pesanError;?>
                                    </div>
                                <?php endif;?>
                                <div class="form-group">
                                    <label for="inputTrayek">Trayek</label>
                                    <select id="inputTrayek" name="trayek" class="form-control" required>
                                        <option value="" selected disabled>PILIH</option>
                                        <?php foreach($trayek->getResult() as $item) : ?>
                                            <option value="<?=$item->kode;?>" ><?="$item->trayek_awal -> $item->trayek_akhir";?></option>
                                        <?php endforeach;?>
                                    </select>
                                    <?php if($trayekError != '') : ?>
                                        <small id="trayekHelper" class="form-text text-danger"><?=$trayekError;?></small>
                                    <?php endif;?>
                                </div>
                                <div class="form-group">
                                    <label for="inputR7">Nomor R7</label>
                                    <div class="input-group">
                                        <input type="text" name="nomor" class="form-control" id="inputR7" placeholder="Nomor R7" value="<?=$nomorR7Input;?>" required>
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-info btn-flat" data-toggle="modal" data-target="#exampleModal" id="qrButton">Acan QR</button>
                                        </span>
                                    </div>
                                    <?php if($nomorR7Error != '') : ?>
                                        <small id="nomorR7Helper" class="form-text text-danger"><?=$nomorR7Error;?></small>
                                    <?php endif;?>
                                </div>
                                <div class="form-group">
                                    <label for="inputJumlah">Jumlah Kantong</label>
                                    <input type="number" name="jumlah" class="form-control" id="inputJumlah" placeholder="Jumlah Kantong" value="<?=$jumlahInput;?>" required>
                                    <?php if($jumlahError != '') : ?>
                                        <small id="jumlahHelper" class="form-text text-danger"><?=$jumlahError;?></small>
                                    <?php endif;?>
                                </div>
                                <div class="form-group">
                                    <label for="inputBerat">Total Berat</label>
                                    <input type="number" name="berat" class="form-control" id="inputBerat" placeholder="Total Berat" value="<?=$beratInput;?>" required>
                                    <?php if($beratError != '') : ?>
                                        <small id="beratHelper" class="form-text text-danger"><?=$beratError;?></small>
                                    <?php endif;?>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                                <button type="reset" class="btn btn-default btn-block">Batal</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h4>Data R7 Hari Ini</h4>
                        </div>
                        <div class="card-body">
                            <table id="datar7" class="table table-bordered table-striped dataTable dtr-inline" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nomor R7</th>
                                        <th>Jumlah Kantong</th>
                                        <th>Berat</th>
                                        <th>Angkutan</th>
                                        <th>Trayek Awal</th>
                                        <th>Trayek Akhir</th>
                                        <th>Tanggal</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1;
                                    foreach ($table->getResult() as $item) : ?>
                                        <tr>
                                            <td><?= $i; ?></td>
                                            <td><?= $item->nomor; ?></td>
                                            <td><?= number_format($item->jumlah, 0, ',', '.'); ?></td>
                                            <td><?= number_format($item->berat, 2, ',', '.'); ?></td>
                                            <td><?= "$item->nopol, $item->type"; ?></td>
                                            <td><?= $item->trayek_awal; ?></td>
                                            <td><?= $item->trayek_akhir; ?></td>
                                            <td><?= $item->created_at; ?></td>
                                            <td align="center"><a href="<?=site_url('dashboard/edit/'.$item->id);?>" class="btn btn-info"><i class="fas fa-edit"></i></a></td>
                                        </tr>
                                    <?php $i++;
                                    endforeach; ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Nomor R7</th>
                                        <th>Jumlah Kantong</th>
                                        <th>Berat</th>
                                        <th>Angkutan</th>
                                        <th>Trayek Awal</th>
                                        <th>Trayek Akhir</th>
                                        <th>Tanggal</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Scan QR</h5>
        <button type="button" class="close" data-dismiss="modal" id="qrCloseButton" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="alert alert-info" role="alert">
                <strong>Tutup</strong> bagian ini jika ingin input manual
            </div>
          <div class="row">
              <div class="col-md-12">
                  <video id="preview" style="width: 100%;"></video>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>
<!-- <script type="text/javascript" src="<?=base_url('assets/js/instascan.min.js');?>"></script> -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/webrtc-adapter/3.3.3/adapter.min.js"></script>
<script type="text/javascript" src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
<?php if($trayekInput != ''): ?>
    <script>
        var trayek = document.getElementById('inputTrayek')
        setTimeout(() => {
            trayek.value = "<?=$trayekInput;?>"
        }, 500);

    </script>
<?php endif;?>
<script type="text/javascript">
    let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
    scanner.addListener('scan', function (content) {
        console.log(content);
        let buttonClose = document.getElementById('qrCloseButton')
        let inputR7 = document.getElementById('inputR7')
        buttonClose.click()
        inputR7.value = content
    });
    Instascan.Camera.getCameras().then(function (cameras) {
    if (cameras.length > 0) {
        scanner.start(cameras[0]);
    } else {
        console.error('No cameras found.');
    }
    }).catch(function (e) {
    console.error(e);
    });
    
    function setTrayek(kode) {
        let inputTrayek = document.getElementById('inputTrayek')
        let inputR7 = document.getElementById('inputR7')
        let openQr = document.getElementById('qrButton')
        inputTrayek.value = kode
        inputR7.focus()
        openQr.click()
    }
</script>
<?= $this->endSection() ?>