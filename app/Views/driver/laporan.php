<?= $this->extend('layout') ?>

<?= $this->section('content') ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Laporan</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><a href="dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item active">Laporan</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <?php if(session()->get('level') == '1'): ;?>
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h3>Filter Laporan</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="">Pilih Periode Laporan</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="date" name="startDate" id="startDate" class="form-control" value="<?=date('Y-m-01');?>">
                                </div>
                                <div class="col-md-4">
                                <input type="date" name="endDate" id="endDate" class="form-control" value="<?=date('Y-m-d');?>">
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-info btn-block" onclick="refreshRegional()">Cari</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h3>Data Per Regional</h3>
                        </div>
                        <div class="card-body">
                            <table id="datar7Regional" class="table table-bordered table-striped dataTable dtr-inline" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Regional</th>
                                        <th>Alamat</th>
                                        <th>Transaksi</th>
                                        <th>Berat</th>
                                        <th>Biaya</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Regional</th>
                                        <th>Alamat</th>
                                        <th>Transaksi</th>
                                        <th>Berat</th>
                                        <th>Biaya</th>
                                        <th>Actions</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
        <?php endif;?>
    </div>
    <!-- /.content -->
</div>
<?= $this->endSection() ?>

<?= $this->section('javascript') ?>
<script>
    var starDate = document.getElementById('startDate')
    var endDate = document.getElementById('endDate')

    if (starDate != null && endDate != null) {        
        $('#datar7Regional').DataTable({
            "responsive": true,
            "autoWidth": false,
            "pageLength": 25,
            "ajax": {
                "url":  "datatable/regional",
                "type": "POST",
                "data": {
                    "startDate" : function() { return $('#startDate').val() },
                    "endDate": function() { return $('#endDate').val() }
                }
            }
        });
    }
    var filter = document.getElementById('filter');

    var bulan1 = document.getElementById('bulan1');
    var bulan2 = document.getElementById('bulan2');
    var tahun = document.getElementById('tahun')
    var bulan = document.getElementById('bulan')

    var tanggal1 = document.getElementById('tanggal1');
    var tanggal2 = document.getElementById('tanggal2');
    var mulai = document.getElementById('mulai');
    var akhir = document.getElementById('akhir');

    var buttonCari = document.getElementById('buttonCari');

    if (filter != null && tanggal1 != null && tanggal2 != null) {
        filter.value = 'bulan';
        tanggal1.style.display  = "none";
        tanggal2.style.display  = "none";

        filter.addEventListener('change', function(e) {
            if (filter.value == 'bulan') {
                console.log('bulan')
                tanggal1.style.display  = "none";
                tanggal2.style.display  = "none";
                bulan1.style.display  = "block";
                bulan2.style.display  = "block";
            } else if (filter.value == 'tanggal') {
                console.log('tanggal')
                bulan1.style.display  = "none";
                bulan2.style.display  = "none";
                tanggal1.style.display  = "block";
                tanggal2.style.display  = "block";
            }
        });

        buttonCari.addEventListener('click', function(e) {
            var url = window.location.host
            var endpoint = ''
            var one = ''
            var two = ''
            if (filter.value == 'bulan') {
                if (tahun.value == '' || bulan.value == '') {
                    alert('Pilih bulan terlebih dahulu')
                    return
                }
                one = tahun.value
                two = bulan.value
            } else if (filter.value == 'tanggal') {
                if (mulai.value == '' || akhir.value == '') {
                    alert('Pilih tanggal terlebih dahulu')
                    return
                }
                one = mulai.value
                two = akhir.value
            }

            var urldata = 'http://' + url + '/dashboard/report/' + filter.value + "/" + one + "/" + two

            window.open(urldata, '_blank');
        })
    }

    function refreshRegional() {
        $('#datar7Regional').DataTable().ajax.reload( null, false );
    }
</script>
<?= $this->endSection() ?>