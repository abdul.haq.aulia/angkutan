<?= $this->extend('layout') ?>

<?= $this->section('content') ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active">Dashboard</li>
                        <!-- <li class="breadcrumb-item active"><a href="#">Home</a></li> -->
                        <!-- <li class="breadcrumb-item active">Starter Page</li> -->
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-success">
                    <div class="inner">
                        <h3><?=$data['alltime'];?></h3>

                        <p>All time Trayek</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-truck-moving"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                    <div class="inner">
                        <h3><?=$data['month'];?></h3>

                        <p>Trayek Bulan ini</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-chart-line"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-warning">
                    <div class="inner">
                        <h3><?=$data['week'];?></h3>

                        <p>Trayek Minggu Ini</p>
                    </div>
                    <div class="icon">
                        <i class="far fa-chart-bar"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-primary">
                    <div class="inner">
                        <h3><?=$data['day'];?></h3>

                        <p>Trayek Hari ini</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-chart-area"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
            </div>
            <?php if(session()->get('level') == '1'): ;?>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col">
                            <div class="card">
                                <div class="card-header">
                                    <h3>Data Per Regional</h3>
                                </div>
                                <div class="card-body">
                                    <table id="datar7" class="table table-bordered table-striped dataTable dtr-inline" width="100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Regional</th>
                                                <th>Alamat</th>
                                                <th>Transaksi</th>
                                                <th>Berat</th>
                                                <th>Biaya</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; foreach($regional as $item): ?>
                                                <tr>
                                                    <td><?=$i;?></td>
                                                    <td><?=$item->nama;?></td>
                                                    <td><?=$item->alamat;?></td>
                                                    <td align="right"><?=number_format($item->total_r7,0,',','.');?></td>
                                                    <td align="right"><?=number_format($item->total_berat,2,',','.');?></td>
                                                    <td align="right"><?=number_format($item->total_harga,2,',','.');?></td>
                                                    <?php if($item->total_r7 > 0): ;?>
                                                        <td align="center"><a href="dashboard/detailTrayek/<?=$item->kode;?>" class="btn btn-info">Lihat Detail Trayek</a></td>
                                                    <?php else:;?>
                                                        <td></td>
                                                    <?php endif;?>
                                                </tr>
                                            <?php $i++; endforeach;?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>Regional</th>
                                                <th>Alamat</th>
                                                <th>Transaksi</th>
                                                <th>Berat</th>
                                                <th>Biaya</th>
                                                <th>Actions</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
                <?php endif;?>
                <hr>
            <!-- /.row -->
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h4>Data R7</h4>
                        </div>
                        <div class="card-body">
                            <table id="datar7" class="table table-bordered table-striped dataTable dtr-inline" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Supir</th>
                                        <th>Nomor R7</th>
                                        <th>Jumlah Kantong</th>
                                        <th>Berat</th>
                                        <th>Angkutan</th>
                                        <th>Trayek Awal</th>
                                        <th>Trayek Akhir</th>
                                        <th>Tanggal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; foreach($table->getResult() as $item): ?>
                                        <tr>
                                            <td><?=$i;?></td>
                                            <td><?=$item->name;?></td>
                                            <td><?=$item->nomor;?></td>
                                            <td><?=number_format($item->jumlah,0,',','.');?></td>
                                            <td><?=number_format($item->berat,2,',','.');?></td>
                                            <td><?="$item->type, $item->nopol";?></td>
                                            <td><?=$item->trayek_awal;?></td>
                                            <td><?=$item->trayek_akhir;?></td>
                                            <td><?=$item->created_at;?></td>
                                        </tr>
                                    <?php $i++; endforeach;?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Supir</th>
                                        <th>Nomor R7</th>
                                        <th>Jumlah Kantong</th>
                                        <th>Berat</th>
                                        <th>Angkutan</th>
                                        <th>Trayek Awal</th>
                                        <th>Trayek Akhir</th>
                                        <th>Tanggal</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<?= $this->endSection() ?>