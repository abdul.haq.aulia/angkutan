<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="content-type" content="text/plain; charset=UTF-8"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- font awsome -->
    <!-- Font Awsome -->
    <link href="<?=base_url('assets/fontAwsome/css/all.css');?>" rel="stylesheet">

    <title>Laporan R7</title>
  </head>
  <body>
    <div class="container">
        <div id="section-to-print">
            <div id="header" class="text-center my-5">
                <h5>Laporan Data R7</h5>
                <?php if($jenis['filter'] == 'bulan'): ?>
                    <?php
                        $bulan = '';
                        if ($jenis['dua'] == 1) {
                            $bulan = 'Januari';
                        }
                        if ($jenis['dua'] == 2) {
                            $bulan = 'Februari';
                        }
                        if ($jenis['dua'] == 3) {
                            $bulan = 'Maret';
                        }
                        if ($jenis['dua'] == 4) {
                            $bulan = 'April';
                        }
                        if ($jenis['dua'] == 5) {
                            $bulan = 'Mei';
                        }
                        if ($jenis['dua'] == 6) {
                            $bulan = 'Juni';
                        }
                        if ($jenis['dua'] == 7) {
                            $bulan = 'Juli';
                        }
                        if ($jenis['dua'] == 8) {
                            $bulan = 'Agustus';
                        }
                        if ($jenis['dua'] == 9) {
                            $bulan = 'September';
                        }
                        if ($jenis['dua'] == 10) {
                            $bulan = 'Oktober';
                        }
                        if ($jenis['dua'] == 11) {
                            $bulan = 'November';
                        }
                        if ($jenis['dua'] == 12) {
                            $bulan = 'Desember';
                        }
                    ;?>
                    <?php $judul = "Bulan $bulan tahun {$jenis['satu']}";?>
                    <p><?=$judul;?></p>
                <?php elseif($jenis['filter'] == 'tanggal'): ?>
                    <?php $judul = "Periode Tanggal <br> {$jenis['satu']} s.d. {$jenis['dua']}";?>
                    <p><?=$judul;?></p>
                <?php endif;?>
            </div>
            <table class="table table-bordered" id="laporan-r7">
                <thead>
                    <tr align="center" style="font-weight: bold;">
                        <td rowspan="2"><br>No</td>
                        <td rowspan="2"><br>Nopol</td>
                        <td colspan="2">Kantor Pos</td>
                        <td rowspan="2"><br>Jarak</td>
                        <td rowspan="2"><br>Harga per kg (Rp)</td>
                        <td rowspan="2"><br>Jml Kantong Kirpos</td>
                        <td rowspan="2"><br>Berat Kirpos (Kg)</td>
                        <td rowspan="2"><br>Jumlah Tarif Kirpos (Rp)</td>
                        <td rowspan="2"><br>Supir</td>
                    </tr>
                    <tr align="center"  style="font-weight: bold;">
                        <td>Awal</td>
                        <td>Tujuan</td>
                    </tr>
                    <!-- <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Mobil</th>
                        <th>Trayek Awal</th>
                        <th>Trayek Akhir</th>
                        <th>Supir</th>
                        <th>Jumlah Kantong</th>
                        <th>Total Berat</th>
                    </tr> -->
                </thead>
                <tbody>
                    <?php $i = 1; $totalBerat = 0; $totalKantong = 0; $totalBiaya = 0; $supir = ""; $nopol = ""; foreach($table->getResult() as $item): ?>
                        <tr>
                            <td><?=$i;?></td>
                            <td><?=$item->nopol;?></td>
                            <td><?=$item->trayek_awal;?></td>
                            <td><?=$item->trayek_akhir;?></td>
                            <td align="right"><?=number_format($item->plpi,2,',','.');?></td>
                            <td align="right"><?=number_format($item->harga_perkg,2,',','.');?></td>
                            <td align="right"><?=number_format($item->jumlah,2,',','.');?></td>
                            <td align="right"><?=number_format($item->berat,2,',','.');?></td>
                            <td align="right"><?=number_format($item->berat * $item->harga_perkg,2,',','.');?></td>
                            <td align="right"><?=$item->name;?></td>
                        </tr>
                    <?php $i++; $totalBerat += $item->berat; $totalKantong += $item->jumlah; $totalBiaya += $item->berat * $item->harga_perkg; $supir = $item->name; $nopol = $item->nopol;  endforeach;?>
                    <tr>
                        <td align="center" colspan="6"><strong>Total Kantong</strong></td>
                        <td align="right" ><strong><?=number_format($totalKantong,2,',','.');?></strong></td>
                        <td align="right" ><strong><?=number_format($totalBerat,2,',','.');?></strong></td>
                        <td align="right" ><strong><?=number_format($totalBiaya,2,',','.');?></strong></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            <br>
            <div class="float-right">............................, <?=date('d-m-Y');?></div>
            <br>
            <div class="float-right">dibuat oleh.</div>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <div class="float-right" style="text-decoration: underline; font-weight: bold;"><?=session()->get('username');?></div>
            <br>
            <br>
            <br>
            <br>
            <div class="float-right">dicetak pada tanggal, <?=date('d-m-Y H:i:s');?></div>
        </div>
        <div class="button-cetak float-left">
            <button class="btn btn-info" id="cetak">Cetak &nbsp;<i class="fas fa-print"></i></button>
        </div>
        <div class="button-cetak float-left mx-3">
            <button class="btn btn-success" id="cetakCSV">Export&nbsp;<i class="far fa-file-excel"></i></button>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script defer src="<?=base_url('assets/fontAwsome/js/all.js');?>"></script>
    <script>
        document.getElementById('cetak').addEventListener('click', function(e) {
            var printContents = document.getElementById('section-to-print').innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        });

        document.getElementById('cetakCSV').addEventListener('click', function(e) {
            window.open('data:application/vnd.ms-excel,' + encodeURIComponent( $('div[id$=section-to-print]').html()));
            e.preventDefault();
        });
    </script>
  </body>
</html>