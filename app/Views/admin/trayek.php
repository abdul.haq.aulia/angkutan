<?= $this->extend('layout') ?>

<?= $this->section('content') ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Trayek</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><a href="<?= site_url('dashboard'); ?>">Dashboard</a></li>
                        <li class="breadcrumb-item active">Trayek</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
        <div class="row">
                <div class="col">
                <?php
                    $inputs = session()->getFlashdata('inputs');
                    $errors = session()->getFlashdata('errors');
                    $pesan = session()->getFlashdata('pesan');
                    $pesanError = session()->getFlashdata('pesan_error');

                    // echo json_encode($inputs);
                    // echo json_encode($errors);
                    // echo json_encode($pesan);
                    // echo json_encode($pesanError);

                    $kodeInput =  isset($inputs['kode']) ? $inputs['kode'] : '';
                    $keberangkatanInput =  isset($errors['keberangkatan']) ? $errors['keberangkatan'] : '';
                    $kodebaseInput =  isset($inputs['kodebase']) ? $inputs['kodebase'] : '';
                    $kodebaseakhirInput = isset($inputs['kodebaseakhir']) ? $inputs['kodebaseakhir'] : '';
                    $koderegionalInput =  isset($inputs['koderegional']) ? $inputs['koderegional'] : '';
                    $kodemobilInput =  isset($inputs['kodemobil']) ? $inputs['kodemobil'] : '';
                    $trayekAwalInput =  isset($inputs['trayek_awal']) ? $inputs['trayek_awal'] : '';
                    $trayekAkhirInput =  isset($inputs['trayek_akhir']) ? $inputs['trayek_akhir'] : '';
                    $plpiInput =  isset($inputs['plpi']) ? $inputs['plpi'] : '';
                    $hargaperkmInput =  isset($inputs['hargaperkm']) ? $inputs['hargaperkm'] : '';
                    $hargaperkgInput =  isset($inputs['hargaperkg']) ? $inputs['hargaperkg'] : '';
                    $jumlahkbmInput =  isset($inputs['jumlahkbm']) ? $inputs['jumlahkbm'] : '';
                    $akhirpksInput =  isset($inputs['akhirpks']) ? $inputs['akhirpks'] : '';
                    $kappksInput =  isset($inputs['kappks']) ? $inputs['kappks'] : '';
                    $kaprealInput =  isset($inputs['kapreal']) ? $inputs['kapreal'] : '';
                    $keteranganInput =  isset($inputs['keterangan']) ? $inputs['keterangan'] : '';

                    $kodeError =  isset($errors['kode']) ? $errors['kode'] : '';
                    $keberangkatanError =  isset($errors['keberangkatan']) ? $errors['keberangkatan'] : '';
                    $kodebaseError =  isset($errors['kodebase']) ? $errors['kodebase'] : '';
                    $kodebaseakhirError = isset($errors['kodebaseakhir']) ? $errors['kodebaseakhir'] : '';
                    $koderegionalError =  isset($errors['koderegional']) ? $errors['koderegional'] : '';
                    $kodemobilError =  isset($errors['kodemobil']) ? $errors['kodemobil'] : '';
                    $trayekAwalError =  isset($errors['trayek_awal']) ? $errors['trayek_awal'] : '';
                    $trayekAkhirError =  isset($errors['trayek_akhir']) ? $errors['trayek_akhir'] : '';
                    $plpiError =  isset($errors['plpi']) ? $errors['plpi'] : '';
                    $hargaperkmError =  isset($errors['hargaperkm']) ? $errors['hargaperkm'] : '';
                    $hargaperkgError =  isset($errors['hargaperkg']) ? $errors['hargaperkg'] : '';
                    $jumlahkbmError =  isset($errors['jumlahkbm']) ? $errors['jumlahkbm'] : '';
                    $akhirpksError =  isset($errors['akhirpks']) ? $errors['akhirpks'] : '';
                    $kappksError =  isset($errors['kappks']) ? $errors['kappks'] : '';
                    $kaprealError =  isset($errors['kapreal']) ? $errors['kapreal'] : '';
                    $keteranganError =  isset($errors['keterangan']) ? $errors['keterangan'] : '';
                ;?>
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Tambah Data Trayek</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" action="<?=site_url('dashboard/addTrayek');?>" method="POST">
                            <div class="card-body">
                                <?php if($pesan): ?>
                                    <div class="alert alert-success" role="alert">
                                        <?=$pesan;?>
                                    </div>
                                <?php endif;?>
                                <?php if($pesanError): ?>
                                    <div class="alert alert-danger" role="alert">
                                        <?=$pesanError;?>
                                    </div>
                                <?php endif;?>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="inputKeberangkatan">Jenis Keberangkatan</label>
                                        <select name="keberangkatan" id="inputKeberangkatan" class="form-control">
                                            <option value="" selected disabled>== PILIH ==</option>
                                            <option value="BERANGKAT">BERANGKAT</option>
                                            <option value="PULANG">PULANG</option>
                                        </select>
                                        <script>
                                            const keberangkatan = document.getElementById('inputKeberangkatan');
                                            keberangkatan.value = '<?=$keberangkatanInput;?>';
                                        </script>
                                        <?php if($keberangkatanError != '') : ?>
                                            <small id="inputKeberangkatan" class="form-text text-danger"><?=$keberangkatanError;?></small>
                                        <?php endif;?>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputRegional">Regional</label>
                                        <select name="koderegional" id="koderegional" class="form-control">
                                            <option value="" selected disabled>PILIH</option>
                                            <?php foreach($regional as $item) : ?>
                                                <option value="<?=$item->kode;?>"><?=$item->nama;?></option>
                                            <?php endforeach;?>
                                        </select>
                                        <?php if($koderegionalError != '') : ?>
                                            <small id="koderegionalHelper" class="form-text text-danger"><?=$koderegionalError;?></small>
                                        <?php endif;?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="inputBase">Base Awal</label>
                                        <select name="kodebase" id="kodebase" class="form-control">
                                            <option value="" selected disabled>PILIH</option>
                                            <?php foreach($base as $item) : ?>
                                                <option value="<?=$item->kode;?>"><?=$item->home_base;?></option>
                                            <?php endforeach;?>
                                        </select>
                                        <?php if($kodebaseError != '') : ?>
                                            <small id="kodebaseHelper" class="form-text text-danger"><?=$kodebaseError;?></small>
                                        <?php endif;?>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputBaseAkhir">Base Akhir</label>
                                        <select name="kodebaseakhir" id="inputBaseAkhir" class="form-control">
                                            <option value="" selected disabled>PILIH</option>
                                            <?php foreach($base as $item) : ?>
                                                <option value="<?=$item->kode;?>"><?=$item->home_base;?></option>
                                            <?php endforeach;?>
                                        </select>
                                        <?php if($kodebaseakhirError != '') : ?>
                                            <small id="kodebaseakhirHelper" class="form-text text-danger"><?=$kodebaseakhirError;?></small>
                                        <?php endif;?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="inputTrayekAwal">Trayek Awal</label>
                                        <select name="trayekAwal" id="inputTrayekAwal" class="form-control">
                                            <option value="" selected disabled>PILIH</option>
                                            <?php foreach($nodes->getResult() as $item) : ?>
                                                <option value="<?=$item->name;?>"><?=$item->name;?></option>
                                            <?php endforeach;?>
                                        </select>
                                        <?php if($trayekAwalError != '') : ?>
                                            <small id="trayekAwalHelper" class="form-text text-danger"><?=$trayekAwalError;?></small>
                                        <?php endif;?>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputTrayekAkhir">Trayek Akhir</label>
                                        <select name="trayekAkhir" id="inputTrayekAkhir" class="form-control">
                                            <option value="" selected disabled>PILIH</option>
                                            <?php foreach($nodes->getResult() as $item) : ?>
                                                <option value="<?=$item->name;?>"><?=$item->name;?></option>
                                            <?php endforeach;?>
                                        </select>
                                        <?php if($trayekAkhirError != '') : ?>
                                            <small id="trayekAkhirHelper" class="form-text text-danger"><?=$trayekAkhirError;?></small>
                                        <?php endif;?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="inputMobil">Mobil</label>
                                        <select name="kodemobil" id="kodemobil" class="form-control">
                                            <option value="" selected disabled>PILIH</option>
                                            <?php foreach($mobil as $item) : ?>
                                                <option value="<?=$item->id;?>"><?="$item->kode $item->type";?></option>
                                            <?php endforeach;?>
                                        </select>
                                        <?php if($kodemobilError != '') : ?>
                                            <small id="kodemobilHelper" class="form-text text-danger"><?=$kodemobilError;?></small>
                                        <?php endif;?>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputHargaperkg"> Harga Per KG</label>
                                        <input type="number" name="hargaperkg" class="form-control" id="inputHargaperkg" placeholder="Harga Per Kilogram" value="<?=$hargaperkgInput;?>" required>
                                        <?php if($hargaperkgError != '') : ?>
                                            <small id="hargaperkgHelper" class="form-text text-danger"><?=$hargaperkgError;?></small>
                                        <?php endif;?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputKeterangan">Keterangan</label>
                                    <input type="text" name="keterangan" class="form-control" id="inputKeterangan" placeholder="Keterangan" value="<?=$keteranganInput;?>" required>
                                    <?php if($keteranganError != '') : ?>
                                        <small id="keteranganHelper" class="form-text text-danger"><?=$keteranganError;?></small>
                                    <?php endif;?>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h4>Data Trayek</h4>
                        </div>
                        <div class="card-body">
                            <table id="datar7" class="table table-bordered table-striped dataTable dtr-inline" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Trayek</th>
                                        <th>Trayek Awal</th>
                                        <th>Trayek Akhir</th>
                                        <th>Harga / Kg</th>
                                        <th>Nopol</th>
                                        <th>Akhir PKS</th>
                                        <th>KBM Eskisting Tipe</th>
                                        <th>KBM Eksisting Tahun</th>
                                        <th>Keterangan</th>
                                        <th width="100">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1;
                                    foreach ($trayek->getResult() as $item) : ?>
                                        <tr>
                                            <td><?= $i; ?></td>
                                            <td><?= $item->home_base ." - ". $item->home_base_akhir; ?></td>
                                            <td><?= $item->trayek_awal; ?></td>
                                            <td><?= $item->trayek_akhir; ?></td>
                                            <td align="right"><?= number_format($item->harga_perkg,2,',','.'); ?></td>
                                            <td><?= $item->nopol; ?></td>
                                            <td><?= $item->akhir_pks; ?></td>
                                            <td><?= $item->type; ?></td>
                                            <td><?= $item->tahun; ?></td>
                                            <td><?= $item->keberangkatan . " ~ " . $item->keterangan; ?></td>
                                            <td align="center">
                                                <a href="<?=site_url('dashboard/editTrayek/'.$item->id);?>" class="btn btn-info"><i class="fas fa-edit"></i></a>
                                                <a href="<?=site_url('dashboard/deleteTrayek/'.$item->id);?>" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    <?php $i++;
                                    endforeach; ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Trayek</th>
                                        <th>Trayek Awal</th>
                                        <th>Trayek Akhir</th>
                                        <th>Harga / Kg</th>
                                        <th>Nopol</th>
                                        <th>Akhir PKS</th>
                                        <th>KBM Eskisting Tipe</th>
                                        <th>KBM Eksisting Tahun</th>
                                        <th>Keterangan</th>
                                        <th width="100">Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<?= $this->endSection() ?>