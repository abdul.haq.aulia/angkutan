<?= $this->extend('layout') ?>

<?= $this->section('content') ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Trayek</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><a href="<?= site_url('dashboard'); ?>">Dashboard</a></li>
                        <li class="breadcrumb-item active">Trayek</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
        <div class="row">
                <div class="col">
                <?php
                    $inputs = session()->getFlashdata('inputs');
                    $errors = session()->getFlashdata('errors');
                    $pesan = session()->getFlashdata('pesan');
                    $pesanError = session()->getFlashdata('pesan_error');

                    // echo json_encode($inputs);
                    // echo json_encode($errors);
                    // echo json_encode($pesan);
                    // echo json_encode($pesanError);

                    $kodeInput =  isset($inputs['kode']) ? $inputs['kode'] : '';
                    $keberangkatanInput =  isset($errors['keberangkatan']) ? $errors['keberangkatan'] : '';
                    $kodebaseInput =  isset($inputs['kodebase']) ? $inputs['kodebase'] : '';
                    $kodebaseakhirInput =  isset($inputs['kodebaseakhir']) ? $inputs['kodebaseakhir'] : '';
                    $koderegionalInput =  isset($inputs['koderegional']) ? $inputs['koderegional'] : '';
                    $kodemobilInput =  isset($inputs['kodemobil']) ? $inputs['kodemobil'] : '';
                    $trayekAwalInput =  isset($inputs['trayek_awal']) ? $inputs['trayek_awal'] : '';
                    $trayekAkhirInput =  isset($inputs['trayek_akhir']) ? $inputs['trayek_akhir'] : '';
                    $plpiInput =  isset($inputs['plpi']) ? $inputs['plpi'] : '';
                    $hargaperkmInput =  isset($inputs['hargaperkm']) ? $inputs['hargaperkm'] : '';
                    $hargaperkgInput =  isset($inputs['hargaperkg']) ? $inputs['hargaperkg'] : '';
                    $jumlahkbmInput =  isset($inputs['jumlahkbm']) ? $inputs['jumlahkbm'] : '';
                    $akhirpksInput =  isset($inputs['akhirpks']) ? $inputs['akhirpks'] : '';
                    $kappksInput =  isset($inputs['kappks']) ? $inputs['kappks'] : '';
                    $kaprealInput =  isset($inputs['kapreal']) ? $inputs['kapreal'] : '';
                    $keteranganInput =  isset($inputs['keterangan']) ? $inputs['keterangan'] : '';

                    $kodeError =  isset($errors['kode']) ? $errors['kode'] : '';
                    $keberangkatanError =  isset($errors['keberangkatan']) ? $errors['keberangkatan'] : '';
                    $kodebaseError =  isset($errors['kodebase']) ? $errors['kodebase'] : '';
                    $kodebaseakhirError =  isset($errors['kodebaseakhir']) ? $errors['kodebaseakhir'] : '';
                    $koderegionalError =  isset($errors['koderegional']) ? $errors['koderegional'] : '';
                    $kodemobilError =  isset($errors['kodemobil']) ? $errors['kodemobil'] : '';
                    $trayekAwalError =  isset($errors['trayek_awal']) ? $errors['trayek_awal'] : '';
                    $trayekAkhirError =  isset($errors['trayek_akhir']) ? $errors['trayek_akhir'] : '';
                    $plpiError =  isset($errors['plpi']) ? $errors['plpi'] : '';
                    $hargaperkmError =  isset($errors['hargaperkm']) ? $errors['hargaperkm'] : '';
                    $hargaperkgError =  isset($errors['hargaperkg']) ? $errors['hargaperkg'] : '';
                    $jumlahkbmError =  isset($errors['jumlahkbm']) ? $errors['jumlahkbm'] : '';
                    $akhirpksError =  isset($errors['akhirpks']) ? $errors['akhirpks'] : '';
                    $kappksError =  isset($errors['kappks']) ? $errors['kappks'] : '';
                    $kaprealError =  isset($errors['kapreal']) ? $errors['kapreal'] : '';
                    $keteranganError =  isset($errors['keterangan']) ? $errors['keterangan'] : '';

                    if ($kodeInput != '' || isset($data->kode)) {
                        $kodeInput = $data->kode;
                    }

                    if ($keberangkatanInput != '' || isset($data->keberangkatan)) {
                        $keberangkatanInput = $data->keberangkatan;
                    }

                    if ($kodebaseInput != '' || isset($data->kode_base)) {
                        $kodebaseInput = $data->kode_base;
                    }

                    if ($kodebaseakhirInput != '' || isset($data->kode_base_akhir)) {
                        $kodebaseakhirInput = $data->kode_base_akhir;
                    }

                    if ($koderegionalInput != '' || isset($data->kode_regional)) {
                        $koderegionalInput = $data->kode_regional;
                    }

                    if ($kodemobilInput != '' || isset($data->kode_mobil)) {
                        $kodemobilInput = $data->kode_mobil;
                    }

                    if ($trayekAwalInput != '' || isset($data->trayek_awal)) {
                        $trayekAwalInput = $data->trayek_awal;
                    }

                    if ($trayekAkhirInput != '' || isset($data->trayek_akhir)) {
                        $trayekAkhirInput = $data->trayek_akhir;
                    }

                    if ($plpiInput != '' || isset($data->plpi)) {
                        $plpiInput = $data->plpi;
                    }

                    if ($hargaperkmInput != '' || isset($data->harga_perkm)) {
                        $hargaperkmInput = $data->harga_perkm;
                    }

                    if ($hargaperkgInput != '' || isset($data->harga_perkg)) {
                        $hargaperkgInput = $data->harga_perkg;
                    }

                    if ($jumlahkbmInput != '' || isset($data->jumlah_kbm)) {
                        $jumlahkbmInput = $data->jumlah_kbm;
                    }

                    if ($akhirpksInput != '' || isset($data->akhir_pks)) {
                        $akhirpksInput = $data->akhir_pks;
                    }

                    if ($kappksInput != '' || isset($data->kap_pks)) {
                        $kappksInput = $data->kap_pks;
                    }

                    if ($kaprealInput != '' || isset($data->kap_real)) {
                        $kaprealInput = $data->kap_real;
                    }

                    if ($keteranganInput != '' || isset($data->keterangan)) {
                        $keteranganInput = $data->keterangan;
                    }
                ;?>
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Update Data Trayek</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" action="<?=site_url('dashboard/updateTrayek');?>" method="POST">
                            <div class="card-body">
                                <?php if($pesan): ?>
                                    <div class="alert alert-success" role="alert">
                                        <?=$pesan;?>
                                    </div>
                                <?php endif;?>
                                <?php if($pesanError): ?>
                                    <div class="alert alert-danger" role="alert">
                                        <?=$pesanError;?>
                                    </div>
                                <?php endif;?>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="inputKeberangkatan">Jenis Keberangkatan</label>
                                        <select name="keberangkatan" id="inputKeberangkatan" class="form-control">
                                            <option value="" selected disabled>== PILIH ==</option>
                                            <option value="BERANGKAT">BERANGKAT</option>
                                            <option value="PULANG">PULANG</option>
                                        </select>
                                        <script>
                                            const keberangkatan = document.getElementById('inputKeberangkatan');
                                            keberangkatan.value = '<?=$keberangkatanInput;?>';
                                        </script>
                                        <?php if($keberangkatanError != '') : ?>
                                            <small id="inputKeberangkatan" class="form-text text-danger"><?=$keberangkatanError;?></small>
                                        <?php endif;?>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputRegional">Regional</label>
                                        <select name="koderegional" id="koderegional" class="form-control">
                                            <option value="" selected disabled>PILIH</option>
                                            <?php foreach($regional as $item) : ?>
                                                <option value="<?=$item->kode;?>"><?=$item->nama;?></option>
                                            <?php endforeach;?>
                                        </select>
                                        <?php if($koderegionalError != '') : ?>
                                            <small id="koderegionalHelper" class="form-text text-danger"><?=$koderegionalError;?></small>
                                        <?php endif;?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="inputBase">Base Awal</label>
                                        <input type="hidden" name="kode" class="form-control" id="inputKode" placeholder="Kode Trayek" value="<?=$kodeInput;?>" required readonly>
                                        <select name="kodebase" id="kodebase" class="form-control">
                                            <option value="" selected disabled>PILIH</option>
                                            <?php foreach($base as $item) : ?>
                                                <option value="<?=$item->kode;?>"><?=$item->home_base;?></option>
                                            <?php endforeach;?>
                                        </select>
                                        <?php if($kodebaseError != '') : ?>
                                            <small id="kodebaseHelper" class="form-text text-danger"><?=$kodebaseError;?></small>
                                        <?php endif;?>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputBaseAkhir">Base Akhir</label>
                                        <select name="kodebaseakhir" id="kodebaseakhir" class="form-control">
                                            <option value="" selected disabled>PILIH</option>
                                            <?php foreach($base as $item) : ?>
                                                <option value="<?=$item->kode;?>"><?=$item->home_base;?></option>
                                            <?php endforeach;?>
                                        </select>
                                        <?php if($kodebaseakhirError != '') : ?>
                                            <small id="kodebaseakhirHelper" class="form-text text-danger"><?=$kodebaseakhirError;?></small>
                                        <?php endif;?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="inputTrayekAwal">Trayek Awal</label>
                                        <select name="trayekAwal" id="inputTrayekAwal" class="form-control">
                                            <option value="" selected disabled>PILIH</option>
                                            <?php foreach($nodes->getResult() as $item) : ?>
                                                <option value="<?=$item->name;?>"><?=$item->name;?></option>
                                            <?php endforeach;?>
                                        </select>
                                        <?php if($trayekAwalError != '') : ?>
                                            <small id="trayekAwalHelper" class="form-text text-danger"><?=$trayekAwalError;?></small>
                                        <?php endif;?>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputTrayekAkhir">Trayek Akhir</label>
                                        <select name="trayekAkhir" id="inputTrayekAkhir" class="form-control">
                                            <option value="" selected disabled>PILIH</option>
                                            <?php foreach($nodes->getResult() as $item) : ?>
                                                <option value="<?=$item->name;?>"><?=$item->name;?></option>
                                            <?php endforeach;?>
                                        </select>
                                        <?php if($trayekAkhirError != '') : ?>
                                            <small id="trayekAkhirHelper" class="form-text text-danger"><?=$trayekAkhirError;?></small>
                                        <?php endif;?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="inputMobil">Mobil</label>
                                        <select name="kodemobil" id="kodemobil" class="form-control">
                                            <option value="" selected disabled>PILIH</option>
                                            <?php foreach($mobil as $item) : ?>
                                                <option value="<?=$item->id;?>"><?="$item->kode $item->type";?></option>
                                            <?php endforeach;?>
                                        </select>
                                        <?php if($kodemobilError != '') : ?>
                                            <small id="kodemobilHelper" class="form-text text-danger"><?=$kodemobilError;?></small>
                                        <?php endif;?>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputHargaperkg">Harga/Kg</label>
                                        <input type="number" name="hargaperkg" class="form-control" id="inputHargaperkg" placeholder="Harga Per Kilogram" value="<?=$hargaperkgInput;?>" required>
                                        <?php if($hargaperkgError != '') : ?>
                                            <small id="hargaperkgHelper" class="form-text text-danger"><?=$hargaperkg;?></small>
                                        <?php endif;?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputKeterangan">Keterangan</label>
                                    <input type="text" name="keterangan" class="form-control" id="inputKeterangan" placeholder="Keterangan" value="<?=$keteranganInput;?>" required>
                                    <?php if($keteranganError != '') : ?>
                                        <small id="keteranganHelper" class="form-text text-danger"><?=$keteranganError;?></small>
                                    <?php endif;?>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<?php if($kodebaseInput != ''): ?>
    <script>
        var kodebase = document.getElementById('kodebase')
        setTimeout(() => {
            kodebase.value = "<?=$kodebaseInput;?>"
        }, 500);
    </script>
<?php endif;?>
<?php if($kodebaseakhirInput != ''): ?>
    <script>
        var kodebaseakhir = document.getElementById('kodebaseakhir')
        setTimeout(() => {
            kodebaseakhir.value = "<?=$kodebaseakhirInput;?>"
        }, 500);
    </script>
<?php endif;?>
<?php if($koderegionalInput != ''): ?>
    <script>
        var koderegional = document.getElementById('koderegional')
        setTimeout(() => {
            koderegional.value = "<?=$koderegionalInput;?>"
        }, 500);
    </script>
<?php endif;?>
<?php if($kodemobilInput != ''): ?>
    <script>
        var kodemobil = document.getElementById('kodemobil')
        setTimeout(() => {
            kodemobil.value = "<?=$kodemobilInput;?>"
        }, 500);
    </script>
<?php endif;?>
<?php if($trayekAwalInput != ''): ?>
    <script>
        var trayekAwal = document.getElementById('inputTrayekAwal')
        setTimeout(() => {
            trayekAwal.value = "<?=$trayekAwalInput;?>"
        }, 500);
    </script>
<?php endif;?>
<?php if($trayekAkhirInput != ''): ?>
    <script>
        var trayekAkhir = document.getElementById('inputTrayekAkhir')
        setTimeout(() => {
            trayekAkhir.value = "<?=$trayekAkhirInput;?>"
        }, 500);
    </script>
<?php endif;?>
<?php if($keberangkatanInput != ''): ?>
    <script>
        var keberangkatan = document.getElementById('inputKeberangktakan')
        setTimeout(() => {
            keberangkatan.value = "<?=$keberangkatanInput;?>"
        }, 500);
    </script>
<?php endif;?>
<?= $this->endSection() ?>

