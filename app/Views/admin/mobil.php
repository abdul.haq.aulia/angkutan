<?= $this->extend('layout') ?>

<?= $this->section('style') ?>
<link rel="stylesheet" href="<?=base_url('assets/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css');?>">
<link rel="stylesheet" href="<?=base_url('assets/adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css');?>">
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Mobil</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><a href="<?= site_url('dashboard'); ?>">Dashboard</a></li>
                        <li class="breadcrumb-item active">Refferensi Mobil</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
        <div class="row">
                <div class="col">
                <?php
                    $inputs = session()->getFlashdata('inputs');
                    $errors = session()->getFlashdata('errors');
                    $pesan = session()->getFlashdata('pesan');
                    $pesanError = session()->getFlashdata('pesan_error');

                    $kodeInput =  isset($inputs['kode']) ? $inputs['kode'] : '';
                    $typeInput =  isset($inputs['type']) ? $inputs['type'] : '';
                    $tahunInput =  isset($inputs['tahun']) ? $inputs['tahun'] : '';

                    $kodeError =  isset($errors['kode']) ? $errors['kode'] : '';
                    $typeError =  isset($errors['type']) ? $errors['type'] : '';
                    $tahunError =  isset($errors['tahun']) ? $errors['tahun'] : '';
                ;?>
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Tambah Data Mobil</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" action="<?=site_url('dashboard/addMobil');?>" method="POST">
                            <div class="card-body">
                                <?php if($pesan): ?>
                                    <div class="alert alert-success" role="alert">
                                        <?=$pesan;?>
                                    </div>
                                <?php endif;?>
                                <?php if($pesanError): ?>
                                    <div class="alert alert-danger" role="alert">
                                        <?=$pesanError;?>
                                    </div>
                                <?php endif;?>
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="inputKode">Nomor Polisi</label>
                                        <input type="text" name="kode" class="form-control" id="inputKode" placeholder="Nomor Polisi" value="<?=$kodeInput;?>" required>
                                        <?php if($kodeError != '') : ?>
                                            <small id="kodeHelper" class="form-text text-danger"><?=$kodeError;?></small>
                                        <?php endif;?>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputType">Tipe</label>
                                        <input type="text" name="type" class="form-control" id="inputType" placeholder="Type Mobil" value="<?=$typeInput;?>" required>
                                        <?php if($typeError != '') : ?>
                                            <small id="typeHelper" class="form-text text-danger"><?=$typeError;?></small>
                                        <?php endif;?>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputTahun">Tahun</label>
                                        <input type="number" name="tahun" class="form-control" id="inputTahun" placeholder="Tahun" value="<?=$tahunInput;?>" required>
                                        <?php if($tahunError != '') : ?>
                                            <small id="tahunHelper" class="form-text text-danger"><?=$tahunError;?></small>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h4>Data Trayek</h4>
                        </div>
                        <div class="card-body">
                            <table id="datar7" class="table table-bordered table-striped dataTable dtr-inline" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nomor Polisi</th>
                                        <th>Tyoe</th>
                                        <th>Tahun</th>
                                        <th width="150">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1;
                                    foreach ($mobil->getResult() as $item) : ?>
                                        <tr>
                                            <td><?= $i; ?></td>
                                            <td><?= $item->kode; ?></td>
                                            <td><?= $item->type; ?></td>
                                            <td><?= $item->tahun; ?></td>
                                            <td align="center">
                                                <a href="<?=site_url('dashboard/editMobil/'.$item->id);?>" class="btn btn-info"><i class="fas fa-edit"></i></a>
                                                <a href="<?=site_url('dashboard/deleteMobil/'.$item->id);?>" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    <?php $i++;
                                    endforeach; ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                    <th>No</th>
                                        <th>Nomor Polisi</th>
                                        <th>Tyoe</th>
                                        <th>Tahun/th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<?= $this->endSection() ?>

<?= $this->section('javascript') ?>
<script src="<?=base_url('assets/adminlte/plugins/datatables/jquery.dataTables.min.js');?>"></script>
    <script src="<?=base_url('assets/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js');?>"></script>
    <script src="<?=base_url('assets/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js');?>"></script>
    <script src="<?=base_url('assets/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js');?>"></script>
    <script>
        $(function () {
            $('#datar7').DataTable({
                "responsive": true,
                "autoWidth": false,
            });
        });
</script>
<?= $this->endSection() ?>