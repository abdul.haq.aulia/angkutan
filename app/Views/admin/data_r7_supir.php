<?= $this->extend('layout') ?>

<?= $this->section('content') ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Laporan</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><a href="dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item active">Data Transaksi R7</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <?php if(session()->get('level') == '1'): ;?>
        <div class="container-fluid" id="section-to-print">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h3>Data Transaksi R7 <?=$user->name;?></h3>
                            <div class="button-cetak float-right">
                                <button class="btn btn-info" id="cetak">Cetak &nbsp;<i class="fas fa-print"></i></button>
                            </div>
                            <div class="button-cetak float-right mx-3">
                                <button class="btn btn-success" id="cetakCSV">Export&nbsp;<i class="far fa-file-excel"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="datar7" class="table table-bordered table-striped dataTable dtr-inline" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nomor R7</th>
                                        <th>Trayek Awal</th>
                                        <th>Trayek Akhir</th>
                                        <th>Harga PerKg</th>
                                        <th>Jumlah Kantong</th>
                                        <th>Berat</th>
                                        <th>Total Biaya</th>
                                        <th>Keterangan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; $totalBerat = 0; $totalHarga = 0; $totalJumlah = 0; foreach($r7->getResult() as $item): ?>
                                        <tr>
                                            <td><?=$i;?></td>
                                            <td><?=$item->nomor;?></td>
                                            <td><?=$item->trayek_awal;?></td>
                                            <td><?=$item->trayek_akhir;?></td>
                                            <td align="right"><?=number_format($item->harga_perkg, 2, ',','.');?></td>
                                            <td align="right"><?=number_format($item->jumlah, 2, ',','.');?></td>
                                            <td align="right"><?=number_format($item->berat, 2, ',','.');?></td>
                                            <td align="right"><?=number_format($item->total, 2, ',','.');?></td>
                                            <td>Mobil : <?=$item->type_mobil ." [". $item->nopol ."] Tahun.". $item->tahun;?></td>
                                        </tr>
                                    <?php $i++; $totalBerat += $item->berat; $totalHarga += $item->total; $totalJumlah += $item->jumlah; endforeach;?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th style="text-align:center" colspan="5">Total</th>
                                        <th style="text-align:right"><?=number_format($totalJumlah, 2, ',','.');?></th>
                                        <th style="text-align:right"><?=number_format($totalBerat, 2, ',','.');?></th>
                                        <th style="text-align:right"><?=number_format($totalHarga, 2, ',','.');?></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
        <?php endif;?>
    </div>
    <!-- /.content -->
</div>

<script>
    document.getElementById('cetak').addEventListener('click', function(e) {
        var printContents = document.getElementById('section-to-print').innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    });

    document.getElementById('cetakCSV').addEventListener('click', function(e) {
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent( $('div[id$=section-to-print]').html()));
        e.preventDefault();
    });
</script>
<?= $this->endSection() ?>