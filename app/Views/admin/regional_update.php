<?= $this->extend('layout') ?>

<?= $this->section('content') ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Regional</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><a href="<?= site_url('dashboard'); ?>">Dashboard</a></li>
                        <li class="breadcrumb-item active">Refferensi Regional</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
        <div class="row">
                <div class="col">
                <?php
                    $inputs = session()->getFlashdata('inputs');
                    $errors = session()->getFlashdata('errors');
                    $pesan = session()->getFlashdata('pesan');
                    $pesanError = session()->getFlashdata('pesan_error');

                    $kodeInput =  isset($inputs['kode']) ? $inputs['kode'] : '';
                    $namaInput =  isset($inputs['nama']) ? $inputs['nama'] : '';
                    $alamatInput =  isset($inputs['alamat']) ? $inputs['alamat'] : '';

                    $kodeError =  isset($errors['kode']) ? $errors['kode'] : '';
                    $namaError =  isset($errors['nama']) ? $errors['nama'] : '';
                    $alamatError =  isset($errors['alamat']) ? $errors['alamat'] : '';

                    if ($kodeInput != '' || isset($data->kode)) {
                        $kodeInput = $data->kode;
                    }

                    if ($namaInput != '' || isset($data->nama)) {
                        $namaInput = $data->nama;
                    }

                    if ($alamatInput != '' || isset($data->alamat)) {
                        $alamatInput = $data->alamat;
                    }
                ;?>
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Update Data Regional</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" action="<?=site_url('dashboard/updateRegional');?>" method="POST">
                            <div class="card-body">
                                <?php if($pesan): ?>
                                    <div class="alert alert-success" role="alert">
                                        <?=$pesan;?>
                                    </div>
                                <?php endif;?>
                                <?php if($pesanError): ?>
                                    <div class="alert alert-danger" role="alert">
                                        <?=$pesanError;?>
                                    </div>
                                <?php endif;?>
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="inputKode">Kode Regional</label>
                                        <input type="text" name="kode" class="form-control" id="inputKode" placeholder="Kode Regional" value="<?=$kodeInput;?>" required readonly>
                                        <?php if($kodeError != '') : ?>
                                            <small id="kodeHelper" class="form-text text-danger"><?=$kodeError;?></small>
                                        <?php endif;?>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputNama">Nama</label>
                                        <input type="text" name="nama" class="form-control" id="inputNama" placeholder="Nama Regional" value="<?=$namaInput;?>" required>
                                        <?php if($namaError != '') : ?>
                                            <small id="namaHelper" class="form-text text-danger"><?=$namaError;?></small>
                                        <?php endif;?>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputAlamat">Alamat</label>
                                        <input type="text" name="alamat" class="form-control" id="inputAlamat" placeholder="Alaamt" value="<?=$alamatInput;?>" required>
                                        <?php if($alamatError != '') : ?>
                                            <small id="alamatHelper" class="form-text text-danger"><?=$alamatError;?></small>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<?= $this->endSection() ?>