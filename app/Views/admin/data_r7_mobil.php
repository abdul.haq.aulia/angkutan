<?= $this->extend('layout') ?>

<?= $this->section('content') ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Laporan</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><a href="dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item active">Data Transaksi R7</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
</div>
<script
      src="https://code.jquery.com/jquery-3.5.1.min.js"
      integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
      integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
      integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
      crossorigin="anonymous"
    ></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>
    const searcButton = document.getElementById('buttonCari');
    const mulai = document.getElementById('mulai')
    const akhir = document.getElementById('akhir')
    const nopol = window.location.pathname.split('/')[3]
    const currentReport = document.getElementById('current-report')
    const reportFilter = document.getElementById('report-filter');

    if (searcButton != null) {
        searcButton.addEventListener('click', function() {
            let payload = {
                'nopol': nopol,
                'mulai': mulai.value,
                'akhir': akhir.value
            }
            axios.post('../getR7mobile', payload)
            .then((res) => {
                currentReport.style.display = 'none';
                reportFilter.style.display = 'block';
                document.getElementById('judul').innerHTML = `Tanggal : ${mulai.value} - ${akhir.value}`
                let data = res.data
                const hasil = $("#hasil");
                hasil.append(`<table id="table-read" class="table table-bordered table-hover" width="100%"></table>`);
                $("#table-read").append(`<thead id="table-head"></thead>`);
                $("#table-head").append(
                `<tr align="center" style="font-weight: bold;">
                            <td rowspan="2"><br>No</td>
                            <td rowspan="2"><br>Nopol</td>
                            <td colspan="2">Kantor Pos</td>
                            <td rowspan="2"><br>Jarak</td>
                            <td rowspan="2"><br>Harga per kg (Rp)</td>
                            <td rowspan="2"><br>Jml Kantong Kirpos</td>
                            <td rowspan="2"><br>Berat Kirpos (Kg)</td>
                            <td rowspan="2"><br>Jumlah Tarif Kirpos (Rp)</td>
                            <td rowspan="2"><br>Supir</td>
                        </tr>
                        <tr align="center"  style="font-weight: bold;">
                            <td>Awal</td>
                            <td>Tujuan</td>
                        </tr>`
                );
                $("#table-read").append(`<tbody id="table-body"></tbody>`);
                let totalKantong = 0;
                let totalBerat = 0;
                let totalBiaya = 0;
                for (let i = 0; i < data.length; i++) {
                    let nomor = i+1
                    let value = data[i];
                    let tarif = Number(value['berat']) * Number(value['harga_perkg'])
                    totalKantong += Number(value['jumlah'])
                    totalBerat += Number(value['berat'])
                    totalBiaya += tarif
                    $("#table-body").append(
                        `<tr>
                                    <td>${nomor}</td>
                                    <td>${value['nopol']}</td>
                                    <td>${value['trayek_awal']}</td>
                                    <td>${value['trayek_akhir']}</td>
                                    <td align="right">${Number(value['plpi']).toLocaleString(['ID'])}</td>
                                    <td align="right">${Number(value['harga_perkg']).toLocaleString(['ID'])}</td>
                                    <td align="right">${Number(value['jumlah']).toLocaleString(['ID'])}</td>
                                    <td align="right">${Number(value['berat']).toLocaleString(['ID'])}</td>
                                    <td align="right">${Number(tarif).toLocaleString(['ID'])}</td>
                                    <td align="right">${value['nama_supir']}</td>
                                </tr>`
                    );
                }
                $("#table-body").append(
                        `<tr>
                            <td align="center" colspan="6"><strong>Total Kantong</strong></td>
                            <td align="right" ><strong>${totalKantong.toLocaleString(['ID'])}</strong></td>
                            <td align="right" ><strong>${totalBerat.toLocaleString(['ID'])}</strong></td>
                            <td align="right" ><strong>${totalBiaya.toLocaleString(['ID'])}</strong></td>
                            <td></td>
                        </tr>`
                );
            })
            .catch((err) => {
                console.log(err)
            })
        })

        document.getElementById('cetak').addEventListener('click', function(e) {
            console.log('diklik cetak')
            var printContents = document.getElementById('section-to-print').innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        });

        document.getElementById('cetakCSV').addEventListener('click', function(e) {
            console.log('diklik CSV')
            window.open('data:application/vnd.ms-excel,' + encodeURIComponent( $('div[id$=section-to-print]').html()));
            e.preventDefault();
        });

        document.getElementById('cetakFilter').addEventListener('click', function(e) {
            console.log('diklik cetak')
            var printContents = document.getElementById('section-to-print-filter').innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        });

        document.getElementById('cetakCSVFilter').addEventListener('click', function(e) {
            console.log('diklik CSV')
            window.open('data:application/vnd.ms-excel,' + encodeURIComponent( $('div[id$=section-to-print-filter]').html()));
            e.preventDefault();
        });
    }
    
</script>
<?= $this->endSection() ?>