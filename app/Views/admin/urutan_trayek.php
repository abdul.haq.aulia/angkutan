<?= $this->extend('layout') ?>

<?= $this->section('content') ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Trayek</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><a href="<?= site_url('dashboard'); ?>">Dashboard</a></li>
                        <li class="breadcrumb-item active">Urutan Trayek</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h4>Data Trayek</h4>
                        </div>
                        <div class="card-body">
                            <table id="datar7" class="table table-bordered table-striped dataTable dtr-inline" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Trayek Asal</th>
                                        <th>Trayek Akhir</th>
                                        <th>Jumlah Rute</th>
                                        <th>Keterangan</th>
                                        <th width="100">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $i = 1;
                                        foreach ($trayek->getResult() as $item) {
                                            echo "<tr>
                                                <td>$i</td>
                                                <td>{$item->home_base}</td>
                                                <td>{$item->home_base_akhir}</td>
                                                <td>{$item->total}</td>
                                                <td>{$item->nama_regional}</td>
                                                <td align='center'>
                                                    <a href='" . site_url('dashboard/trayek/urutan/detail/'.$item->kode_awal.'/'.$item->kode_akhir.'/'.$item->kode_regional) ."' class='btn btn-info'><i class='fas fa-search'></i></a>
                                                </td>
                                            </tr>";
                                            $i++;
                                        }
                                    ;?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Trayek Asal</th>
                                        <th>Trayek Akhir</th>
                                        <th>Jumlah Rute</th>
                                        <th>Keterangan</th>
                                        <th width="100">Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<?= $this->endSection() ?>