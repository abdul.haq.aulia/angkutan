<?= $this->extend('layout') ?>

<?= $this->section('content') ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Laporan</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><a href="dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item active">Data Transaksi R7</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="content">
        <?php if(session()->get('level') == '1'): ;?>
        <div class="container-fluid" id="section-to-print">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h4>Laporan Actual Weight</h4>
                            <hr>
                            <h5>Nomor Polisi : <?=$mobil->kode;?></h5>
                            <h5>KBM          : <?=$mobil->type;?></h5>
                            <h5>Tujuan       : <?=$rute['awal'] .' - '.$rute['akhir'];?></h5>
                            <h5>Periode      : <?=$tanggal['startDate'] ." s.d. ". $tanggal['endDate'];?></h5>
                            <div class="button-cetak float-right" id="cetak">
                                <button class="btn btn-info" id="cetak">Cetak &nbsp;<i class="fas fa-print"></i></button>
                            </div>
                            <div class="button-cetak float-right mx-3" id="export">
                                <button class="btn btn-success" id="cetakCSV">Export&nbsp;<i class="far fa-file-excel"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="datar7" class="table table-bordered table-striped dataTable dtr-inline" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal</th>
                                        <th>No. R7</th>
                                        <th>Jumlah Kantong</th>
                                        <th>Berat</th>
                                        <th>Total Biaya</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $i = 1;
                                        $totalKantong = 0;
                                        $totalBerat = 0;
                                        $totalBiaya = 0;
                                        foreach($dataR7 as $item) {
                                            echo "<tr>
                                                <td>$i</td>
                                                <td>$item->created_at</td>
                                                <td>$item->nomor</td>
                                                <td>". number_format($item->jumlah,0,'.',','). "</td>
                                                <td>". number_format($item->berat,0,'.',','). "</td>
                                                <td>". number_format($item->total,0,'.',','). "</td>
                                            </tr>";
                                            $totalKantong += $item->jumlah;
                                            $totalBerat += $item->berat;
                                            $totalBiaya += $item->total;
                                            $i++;
                                        }
                                    ;?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="3">Total</th>
                                        <th><?= number_format($totalKantong,0,'.',',') ;?></th>
                                        <th><?= number_format($totalBerat,0,'.',',') ;?></th>
                                        <th><?= number_format($totalBiaya,0,'.',',') ;?></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
        <?php endif;?>
    </div>
</div>

<script>
    document.getElementById('cetak').addEventListener('click', function(e) {
        document.getElementById('cetak').style.display = 'none';
        document.getElementById('export').style.display = 'none';
        var printContents = document.getElementById('section-to-print').innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
        document.getElementById('cetak').style.display = 'block';
        document.getElementById('export').style.display = 'block';
    });

    document.getElementById('cetakCSV').addEventListener('click', function(e) {
        document.getElementById('cetak').style.display = 'none';
        document.getElementById('export').style.display = 'none';
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent( $('div[id$=section-to-print]').html()));
        e.preventDefault();
        document.getElementById('cetak').style.display = 'block';
        document.getElementById('export').style.display = 'block';
    });
</script>
<?= $this->endSection() ?>