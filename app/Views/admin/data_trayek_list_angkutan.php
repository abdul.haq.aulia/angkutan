<?= $this->extend('layout') ?>

<?= $this->section('content') ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Laporan</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><a href="dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item active">Data Trayek</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <?php if(session()->get('level') == '1'): ;?>
        <div class="container-fluid" id="section-to-print">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h3>Data Trayek <?=$regional->nama;?> <small><?=$tanggal['startDate'] ." s.d. ". $tanggal['endDate'];?></small> </h3>
                            <h4>Trayek <?=$trayek['asal'] . " - " .  $trayek['akhir'];?></h4>
                            <div class="button-cetak float-right">
                                <button class="btn btn-info" id="cetak">Cetak &nbsp;<i class="fas fa-print"></i></button>
                            </div>
                            <div class="button-cetak float-right mx-3">
                                <button class="btn btn-success" id="cetakCSV">Export&nbsp;<i class="far fa-file-excel"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="datar7" class="table table-bordered table-striped dataTable dtr-inline" width="100%">
                                <thead>
                                    <tr align="center">
                                        <th>No</th>
                                        <th>KBM</th>
                                        <th>Jumlah Transaksi</th>
                                        <th>Total Berat</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; $totalTransaksi = 0; $totalBerat = 0; foreach($dataRute as $item): ?>
                                        <tr>
                                            <td><?=$i;?></td>
                                            <td><?=$item->kbm . " " . $item->type ;?></td>
                                            <td><?=number_format($item->total_transaksi,0,',','.');?></td>
                                            <td><?=number_format($item->total_berat,0,',','.');?></td>
                                            <?php if($item->total_transaksi != 0 && $item->total_berat != 0): ;?>
                                                <td align="center"><a href="../detailKendaraanTrayekList/<?=$trayek['asal']."::".$trayek['akhir']."::".$trayek['kode_regional']."::".$tanggal['startDate']."::".$tanggal['endDate']."::".$item->kbm;?>" class="btn btn-info">Lihat Detail</a></td>
                                            <?php else:;?>
                                                <td></td>
                                            <?php endif;?>
                                        </tr>
                                    <?php $i++; $totalTransaksi += $item->total_transaksi; $totalBerat += $item->total_berat;  endforeach;?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="2" style="width: 230px;">Total</th>
                                        <th style="width: 110;"><?=number_format($totalTransaksi,0,'.',',');?></th>
                                        <th style="width: 150;"><?=number_format($totalBerat,0,'.',',');?></th>
                                        <th style="width: 150;"></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
        <?php endif;?>
    </div>
    <!-- /.content -->
</div>

<script>
    document.getElementById('cetak').addEventListener('click', function(e) {
        var printContents = document.getElementById('section-to-print').innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    });

    document.getElementById('cetakCSV').addEventListener('click', function(e) {
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent( $('div[id$=section-to-print]').html()));
        e.preventDefault();
    });
</script>
<?= $this->endSection() ?>