<?= $this->extend('layout') ?>

<?= $this->section('content') ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Laporan</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><a href="dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item active">Data Trayek</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <?php if(session()->get('level') == '1'): ;?>
        <div class="container-fluid" id="section-to-print">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h3>Data Trayek <?=$regional->nama;?> <small><?=$tanggal['startDate'] ." s.d. ". $tanggal['endDate'];?></small> </h3>
                            <h4>Trayek <?=$trayek['asal'] . " - " .  $trayek['akhir'];?></h4>
                            <div class="button-cetak float-right">
                                <button class="btn btn-info" id="cetak">Cetak &nbsp;<i class="fas fa-print"></i></button>
                            </div>
                            <div class="button-cetak float-right mx-3">
                                <button class="btn btn-success" id="cetakCSV">Export&nbsp;<i class="far fa-file-excel"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <h4>A. Berangkat</h4>
                            <table id="datar7" class="table table-bordered table-striped dataTable dtr-inline" width="100%">
                                <thead>
                                    <tr align="center">
                                        <th rowspan="2">No</th>
                                        <th colspan="2">Etape</th>
                                        <th rowspan="2">KBM</th>
                                        <th rowspan="2">Harga</th>
                                        <th rowspan="2">Jumlah Transaksi</th>
                                        <th rowspan="2">Total Berat</th>
                                        <th rowspan="2">Biaya</th>
                                        <th rowspan="2">Keterangan</th>
                                        <th rowspan="2">Actions</th>
                                    </tr>
                                    <tr>
                                        <th>Awal</th>
                                        <th>Akhir</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; $totalTransaksi = 0; $totalBerat = 0; $totalBiaya = 0; foreach($dataRute as $item): ?>
                                        <?php if($item->keberangkatan == 'BERANGKAT'): ;?>
                                        <tr>
                                            <td><?=$i;?></td>
                                            <td><?=$item->trayek_awal;?></td>
                                            <td><?=$item->trayek_akhir;?></td>
                                            <td><?=$item->kbm ." " . $item->type;?></td>
                                            <td><?=number_format($item->harga,0,',','.');?></td>
                                            <td><?=number_format($item->total_transaksi,0,',','.');?></td>
                                            <td><?=number_format($item->total_berat,0,',','.');?></td>
                                            <td><?=number_format($item->total_biaya,0,',','.');?></td>
                                            <td><?=$item->keberangkatan;?></td>
                                            <?php if($item->kode_base != "" && $item->kode_base_akhir != ""): ;?>
                                                <td align="center"><a href="../detailKendaraanTrayek/<?=$item->trayek_awal."::".$item->trayek_akhir."::".$item->kode_regional."::".$tanggal['startDate']."::".$tanggal['endDate'];?>" class="btn btn-info">Lihat Detail Angkutan</a></td>
                                            <?php else:;?>
                                                <td></td>
                                            <?php endif;?>
                                        </tr>
                                        <?php $totalTransaksi += $item->total_transaksi; $totalBerat += $item->total_berat; $totalBiaya += $item->total_biaya;  endif;?>
                                    <?php $i++; endforeach;?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="4" style="width: 230px;">Total</th>
                                        <th style="width: 110;"><?=number_format($totalTransaksi,0,'.',',');?></th>
                                        <th style="width: 150;"><?=number_format($totalBerat,0,'.',',');?></th>
                                        <th style="width: 150;"><?=number_format($totalBiaya,0,'.',',');?></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                            <hr>
                            <h4>A. Pulang</h4>
                            <table id="datar7" class="table table-bordered table-striped dataTable dtr-inline" width="100%">
                                <thead>
                                    <tr align="center">
                                        <th rowspan="2">No</th>
                                        <th colspan="2">Etape</th>
                                        <th rowspan="2">KBM</th>
                                        <th rowspan="2">Harga</th>
                                        <th rowspan="2">Jumlah Transaksi</th>
                                        <th rowspan="2">Total Berat</th>
                                        <th rowspan="2">Biaya</th>
                                        <th rowspan="2">Keterangan</th>
                                        <th rowspan="2">Actions</th>
                                    </tr>
                                    <tr>
                                        <th>Awal</th>
                                        <th>Akhir</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; $totalTransaksi2 = 0; $totalBerat2 = 0; $totalBiaya2 = 0; foreach($dataRute as $item): ?>
                                        <?php if($item->keberangkatan == 'PULANG'): ;?>
                                        <tr>
                                            <td><?=$i;?></td>
                                            <td><?=$item->trayek_awal;?></td>
                                            <td><?=$item->trayek_akhir;?></td>
                                            <td><?=$item->kbm ." " . $item->type;?></td>
                                            <td><?=number_format($item->harga,0,',','.');?></td>
                                            <td><?=number_format($item->total_transaksi,0,',','.');?></td>
                                            <td><?=number_format($item->total_berat,0,',','.');?></td>
                                            <td><?=number_format($item->total_biaya,0,',','.');?></td>
                                            <td><?=$item->keberangkatan;?></td>
                                            <?php if($item->kode_base != "" && $item->kode_base_akhir != ""): ;?>
                                                <td align="center"><a href="../detailKendaraanTrayek/<?=$item->trayek_awal."::".$item->trayek_akhir."::".$item->kode_regional."::".$tanggal['startDate']."::".$tanggal['endDate'];?>" class="btn btn-info">Lihat Detail Angkutan</a></td>
                                            <?php else:;?>
                                                <td></td>
                                            <?php endif;?>
                                            
                                        </tr>
                                        <?php $totalTransaksi2 += $item->total_transaksi; $totalBerat2 += $item->total_berat; $totalBiaya2 += $item->total_biaya; endif;?>
                                    <?php $i++;  endforeach;?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="4" style="width: 230px;">Total</th>
                                        <th style="width: 110;"><?=number_format($totalTransaksi2,0,'.',',');?></th>
                                        <th style="width: 150;"><?=number_format($totalBerat2,0,'.',',');?></th>
                                        <th style="width: 150;"><?=number_format($totalBiaya2,0,'.',',');?></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                            <hr>
                            <table id="datar7" class="table table-bordered table-striped dataTable dtr-inline" width="100%">
                                <thead>
                                    <tr>
                                        <th colspan="4" rowspan="2"  style="width: 20px;">Total A + B</th>
                                        <th style="width: 110px;">Total Transaksi</th>
                                        <th style="width: 150px;">Total Berat</th>
                                        <th style="width: 150px;">Total Biaya</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <th><?=number_format($totalTransaksi2 + $totalTransaksi,0,'.',',');?></th>
                                        <th><?=number_format($totalBerat2 + $totalBerat,0,'.',',');?></th>
                                        <th><?=number_format($totalBiaya2 + $totalBiaya,0,'.',',');?></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
        <?php endif;?>
    </div>
    <!-- /.content -->
</div>

<script>
    document.getElementById('cetak').addEventListener('click', function(e) {
        var printContents = document.getElementById('section-to-print').innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    });

    document.getElementById('cetakCSV').addEventListener('click', function(e) {
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent( $('div[id$=section-to-print]').html()));
        e.preventDefault();
    });
</script>
<?= $this->endSection() ?>