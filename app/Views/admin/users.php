<?= $this->extend('layout') ?>

<?= $this->section('content') ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Trayek</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><a href="<?=site_url('dashboard');?>">Dashboard</a></li>
                        <li class="breadcrumb-item active">Users</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                <?php
                    $inputs = session()->getFlashdata('inputs');
                    $errors = session()->getFlashdata('errors');
                    $pesan = session()->getFlashdata('pesan');
                    $pesanError = session()->getFlashdata('pesan_error');
                    $usernameInput =  isset($inputs['username']) ? $inputs['username'] : '';
                    $passwordInput =  isset($inputs['password']) ? $inputs['password'] : '';
                    $nameInput =  isset($inputs['name']) ? $inputs['name'] : '';
                    $levelInput =  isset($inputs['level']) ? $inputs['level'] : '';

                    $usernameError =  isset($errors['username']) ? $errors['username'] : '';
                    $passwordError =  isset($errors['password']) ? $errors['password'] : '';
                    $nameError =  isset($errors['name']) ? $errors['name'] : '';
                    $levelError =  isset($errors['level']) ? $errors['level'] : '';
                ;?>
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Tambah Data User</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" action="addUser" method="POST">
                            <div class="card-body">
                                <?php if($pesan): ?>
                                    <div class="alert alert-success" role="alert">
                                        <?=$pesan;?>
                                    </div>
                                <?php endif;?>
                                <?php if($pesanError): ?>
                                    <div class="alert alert-danger" role="alert">
                                        <?=$pesanError;?>
                                    </div>
                                <?php endif;?>
                                <div class="form-group">
                                    <label for="inputUsername">Username</label>
                                    <input type="text" name="username" class="form-control" id="inputUsername" placeholder="Username" value="<?=$usernameInput;?>" required>
                                    <?php if($usernameError != '') : ?>
                                        <small id="usernameHelper" class="form-text text-danger"><?=$usernameError;?></small>
                                    <?php endif;?>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword">Password</label>
                                    <input type="password" name="password" class="form-control" id="inputPassword" placeholder="Password" value="<?=$passwordInput;?>" required>
                                    <?php if($passwordError != '') : ?>
                                        <small id="passwordHelper" class="form-text text-danger"><?=$passwordError;?></small>
                                    <?php endif;?>
                                </div>
                                <div class="form-group">
                                    <label for="inputNama">Nama</label>
                                    <input type="text" name="name" class="form-control" id="inputNama" placeholder="Username" value="<?=$nameInput;?>" required>
                                    <?php if($nameError != '') : ?>
                                        <small id="nameHelper" class="form-text text-danger"><?=$nameError;?></small>
                                    <?php endif;?>
                                </div>
                                <div class="form-group">
                                    <label for="inputLevel">Level</label>
                                    <select name="level" id="inputLevel" class="form-control">
                                        <option value="" selected disabled>PILIH</option>
                                        <option value="2">Supir</option>
                                    </select>
                                    <?php if($levelError != '') : ?>
                                        <small id="levelHelper" class="form-text text-danger"><?=$levelError;?></small>
                                    <?php endif;?>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                                <button type="reset" class="btn btn-default btn-block">Batal</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h4>Data User</h4>
                        </div>
                        <div class="card-body">
                            <table id="datar7" class="table table-bordered table-striped dataTable dtr-inline" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>User Name</th>
                                        <th>Nama</th>
                                        <th>Level</th>
                                        <th>Tanggal Daftar</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; foreach($users as $item): ?>
                                        <tr>
                                            <td><?=$i;?></td>
                                            <td><?=$item->username;?></td>
                                            <td><?=$item->name;?></td>
                                            <td><?php
                                                if ($item->level == 0) {
                                                    echo "Super Admin";
                                                }
                                                if ($item->level == 1) {
                                                    echo "Admin";
                                                }
                                                if ($item->level == 2) {
                                                    echo "Supir";
                                                }
                                            ;?></td>
                                            <td><?=$item->created_at;?></td>
                                            <td align="center">
                                                <a href="<?=site_url('dashboard/resetPassword/'.$item->id);?>" class="btn btn-primary" title="Reset Password"><i class="fas fa-recycle"></i></a>
                                                <a href="<?=site_url('dashboard/editUser/'.$item->id);?>" class="btn btn-info" title="Edit User"><i class="fas fa-edit"></i></a>
                                                <a href="<?=site_url('dashboard/deleteUser/'.$item->id);?>" class="btn btn-danger" title="Hapus User"><i class="fas fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    <?php $i++; endforeach;?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>User Name</th>
                                        <th>Nama</th>
                                        <th>Level</th>
                                        <th>Tanggal Daftar</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<?php if($levelInput != ''): ?>
    <script>
        var level = document.getElementById('inputLevel')
        setTimeout(() => {
            level.value = "<?=$levelInput;?>"
        }, 500);
    </script>
<?php endif;?>
<?= $this->endSection() ?>