<?= $this->extend('layout') ?>

<?= $this->section('style') ?>
<link rel="stylesheet" href="<?=base_url('assets/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css');?>">
<link rel="stylesheet" href="<?=base_url('assets/adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css');?>">
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Nodes</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><a href="<?= site_url('dashboard'); ?>">Dashboard</a></li>
                        <li class="breadcrumb-item active">Refferensi Nodes</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
        <div class="row">
                <div class="col">
                <?php
                    $inputs = session()->getFlashdata('inputs');
                    $errors = session()->getFlashdata('errors');
                    $pesan = session()->getFlashdata('pesan');
                    $pesanError = session()->getFlashdata('pesan_error');

                    $nodesInput =  isset($inputs['name']) ? $inputs['name'] : '';
                    $descriptionInput =  isset($inputs['description']) ? $inputs['description'] : '';

                    $nodesError =  isset($errors['name']) ? $errors['name'] : '';
                    $descriptionError =  isset($errors['description']) ? $errors['description'] : '';
                ;?>
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Tambah Data Nodes</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" action="<?=site_url('dashboard/addNodes');?>" method="POST">
                            <div class="card-body">
                                <?php if($pesan): ?>
                                    <div class="alert alert-success" role="alert">
                                        <?=$pesan;?>
                                    </div>
                                <?php endif;?>
                                <?php if($pesanError): ?>
                                    <div class="alert alert-danger" role="alert">
                                        <?=$pesanError;?>
                                    </div>
                                <?php endif;?>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="inputNodes">Nama Nodes</label>
                                        <input type="text" name="name" class="form-control" id="inputNodes" placeholder="Nama Nodes , contoh. Semarang..." value="<?=$nodesInput;?>" required>
                                        <?php if($nodesError != '') : ?>
                                            <small id="nameHelper" class="form-text text-danger"><?=$nodesError;?></small>
                                        <?php endif;?>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputDescription">Description</label>
                                        <input type="text" name="description" class="form-control" id="inputDescription" placeholder="Deskripsi" value="<?=$descriptionInput;?>" required>
                                        <?php if($descriptionError != '') : ?>
                                            <small id="descriptionHelper" class="form-text text-danger"><?=$descriptionError;?></small>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h4>Data Nodes</h4>
                        </div>
                        <div class="card-body">
                            <table id="datarNodes" class="table table-bordered table-striped dataTable dtr-inline" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nodes</th>
                                        <th>Deskripsi</th>
                                        <th width="150">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1;
                                    foreach ($nodes->getResult() as $item) : ?>
                                        <tr>
                                            <td><?= $i; ?></td>
                                            <td><?= $item->name; ?></td>
                                            <td><?= $item->description; ?></td>
                                            <td align="center">
                                                <a href="<?=site_url('dashboard/editNodes/'.$item->id);?>" class="btn btn-info"><i class="fas fa-edit"></i></a>
                                                <a href="<?=site_url('dashboard/deleteNodes/'.$item->id);?>" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    <?php $i++;
                                    endforeach; ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Nodes</th>
                                        <th>Deskripsi</th>
                                        <th width="150">Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<?= $this->endSection() ?>

<?= $this->section('javascript') ?>
<script src="<?=base_url('assets/adminlte/plugins/datatables/jquery.dataTables.min.js');?>"></script>
    <script src="<?=base_url('assets/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js');?>"></script>
    <script src="<?=base_url('assets/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js');?>"></script>
    <script src="<?=base_url('assets/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js');?>"></script>
    <script>
        $(function () {
            $('#datarNodes').DataTable({
                "responsive": true,
                "autoWidth": false,
            });
        });
</script>
<?= $this->endSection() ?>