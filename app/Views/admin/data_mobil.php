<?= $this->extend('layout') ?>

<?= $this->section('content') ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Laporan</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><a href="dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item active">Data Trayek</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <?php if(session()->get('level') == '1'): ;?>
        <div class="container-fluid" id="section-to-print">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h3>Data Kendaraan Trayek <?=$namaRegional;?></h3>
                            <h4><?=$trayek['awal'] .' - '. $trayek['akhir'];?></h4>
                            <div class="button-cetak float-right">
                                <button class="btn btn-info" id="cetak">Cetak &nbsp;<i class="fas fa-print"></i></button>
                            </div>
                            <div class="button-cetak float-right mx-3">
                                <button class="btn btn-success" id="cetakCSV">Export&nbsp;<i class="far fa-file-excel"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="datar7" class="table table-bordered table-striped dataTable dtr-inline" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nomor Polisi</th>
                                        <th>Type</th>
                                        <th>Tahun</th>
                                        <th>Jumlah Transaksi</th>
                                        <th>Total Berat</th>
                                        <th>Total Biaya</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; foreach($mobil as $item): ?>
                                        <tr>
                                            <td><?=$i;?></td>
                                            <td><?=$item->nopol;?></td>
                                            <td><?=$item->type;?></td>
                                            <td><?=$item->tahun;?></td>
                                            <td><?=number_format($item->total_transaksi);?></td>
                                            <td><?=number_format($item->total_berat);?></td>
                                            <td>Rp.<?=number_format($item->total_biaya);?></td>
                                            <td align="center"><a href="../detailTransaksiKendaraan/<?=$item->nopol?>/<?=$tanggal['awal']?>/<?=$tanggal['akhir']?>/<?=$namaRegional;?>/<?=$trayek['awal'];?>/<?=$trayek['akhir'];?>" class="btn btn-info">Lihat Detail Transaksi</a></td>
                                        </tr>
                                    <?php $i++; endforeach;?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Nomor Polisi</th>
                                        <th>Type</th>
                                        <th>Tahun</th>
                                        <th>Jumlah Transaksi</th>
                                        <th>Total Berat</th>
                                        <th>Total Biaya</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
        <?php endif;?>
    </div>
    <!-- /.content -->
</div>

<script>
    document.getElementById('cetak').addEventListener('click', function(e) {
        var printContents = document.getElementById('section-to-print').innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    });

    document.getElementById('cetakCSV').addEventListener('click', function(e) {
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent( $('div[id$=section-to-print]').html()));
        e.preventDefault();
    });
</script>
<?= $this->endSection() ?>