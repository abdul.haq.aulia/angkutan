<?= $this->extend('layout') ?>

<?= $this->section('content') ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Trayek</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><a href="<?= site_url('dashboard'); ?>">Dashboard</a></li>
                        <li class="breadcrumb-item active">Urutan Trayek Detail</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h4>Detail Trayek <?= $trayek_awal ." - ". $trayek_akhir;?> Regional <?= $regional ;?></h4>
                        </div>
                        <div class="card-body">
                            <h4>Berangkat</h4>
                            <table id="datar7" class="table table-bordered table-striped dataTable dtr-inline" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Rute Awal</th>
                                        <th>Rute Tujuan</th>
                                        <th>Supir</th>
                                        <th>Mobil</th>
                                        <th>Urutan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $i = 1;
                                        foreach ($trayek->getResult() as $item) {
                                            if ($item->keberangkatan == 'BERANGKAT') {
                                                echo "<tr>
                                                    <td>$i</td>
                                                    <td>{$item->trayek_awal}</td>
                                                    <td>{$item->trayek_akhir}</td>
                                                    <td>{$item->name}</td>
                                                    <td>{$item->nopol} -  {$item->tahun} - {$item->type}</td>
                                                    <td><input type='number' name='urutan' id='urutanrute' value='{$item->urutan}' onkeyup='simpanberangkat(this, {$item->id})'></td>
                                                </tr>";
                                                $i++;
                                            }
                                        }
                                    ;?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Rute Awal</th>
                                        <th>Rute Tujuan</th>
                                        <th>Supir</th>
                                        <th>Mobil</th>
                                        <th>Urutan</th>
                                    </tr>
                                </tfoot>
                            </table>
                            <hr>
                            <h4>Pulang</h4>
                            <table id="datar7" class="table table-bordered table-striped dataTable dtr-inline" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Rute Awal</th>
                                        <th>Rute Tujuan</th>
                                        <th>Supir</th>
                                        <th>Mobil</th>
                                        <th>Jenis</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $i = 1;
                                        foreach ($trayek->getResult() as $item) {
                                            if ($item->keberangkatan == 'PULANG') {
                                                echo "<tr>
                                                    <td>$i</td>
                                                    <td>{$item->trayek_awal}</td>
                                                    <td>{$item->trayek_akhir}</td>
                                                    <td>{$item->name}</td>
                                                    <td>{$item->nopol} -  {$item->tahun} - {$item->type}</td>
                                                    <td><input type='number' name='urutan' id='urutanrute' value='{$item->urutan}' onkeyup='simpanberangkat(this, {$item->id})'></td>
                                                </tr>";
                                                $i++;
                                            }
                                        }
                                    ;?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Rute Awal</th>
                                        <th>Rute Tujuan</th>
                                        <th>Supir</th>
                                        <th>Mobil</th>
                                        <th>Jenis</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>
    function simpanberangkat(e, id) {
        let idTrayek = id
        let urutan = e.value
        let paylod = {
            idTrayek: id,
            urutanTrayek: urutan
        }
        console.log(paylod)
        axios.post('/dashboard/trayek/updateurutan', paylod)
            .then((res) => {
                console.log(res.date)
            })
            .catch((err) => {
                console.log(err.response)
            })
    }
</script>
<?= $this->endSection() ?>
