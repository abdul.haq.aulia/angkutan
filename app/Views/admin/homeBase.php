<?= $this->extend('layout') ?>

<?= $this->section('style') ?>
<link rel="stylesheet" href="<?=base_url('assets/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css');?>">
<link rel="stylesheet" href="<?=base_url('assets/adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css');?>">
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Home Base</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><a href="<?= site_url('dashboard'); ?>">Dashboard</a></li>
                        <li class="breadcrumb-item active">Refferensi Home Base</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
        <div class="row">
                <div class="col">
                <?php
                    $inputs = session()->getFlashdata('inputs');
                    $errors = session()->getFlashdata('errors');
                    $pesan = session()->getFlashdata('pesan');
                    $pesanError = session()->getFlashdata('pesan_error');

                    $kodeInput =  isset($inputs['kode']) ? $inputs['kode'] : '';
                    $homebaseInput =  isset($inputs['home_base']) ? $inputs['home_base'] : '';
                    $alamatInput =  isset($inputs['alamat']) ? $inputs['alamat'] : '';

                    $kodeError =  isset($errors['kode']) ? $errors['kode'] : '';
                    $homebaseError =  isset($errors['home_base']) ? $errors['home_base'] : '';
                    $alamatError =  isset($errors['alamat']) ? $errors['alamat'] : '';
                ;?>
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Tambah Data Home Base</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" action="<?=site_url('dashboard/addHomeBase');?>" method="POST">
                            <div class="card-body">
                                <?php if($pesan): ?>
                                    <div class="alert alert-success" role="alert">
                                        <?=$pesan;?>
                                    </div>
                                <?php endif;?>
                                <?php if($pesanError): ?>
                                    <div class="alert alert-danger" role="alert">
                                        <?=$pesanError;?>
                                    </div>
                                <?php endif;?>
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="inputKode">Kode Home Base</label>
                                        <input type="text" name="kode" class="form-control" id="inputKode" placeholder="Kode Home Base" value="<?=$kodeInput;?>" required>
                                        <?php if($kodeError != '') : ?>
                                            <small id="kodeHelper" class="form-text text-danger"><?=$kodeError;?></small>
                                        <?php endif;?>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputHomebase">Home Base</label>
                                        <input type="text" name="home_base" class="form-control" id="inputHomebase" placeholder="Nama Home Base" value="<?=$homebaseInput;?>" required>
                                        <?php if($homebaseError != '') : ?>
                                            <small id="homebaseHelper" class="form-text text-danger"><?=$homebaseError;?></small>
                                        <?php endif;?>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputAlamat">Alamat</label>
                                        <input type="text" name="alamat" class="form-control" id="inputAlamat" placeholder="Alamat" value="<?=$alamatInput;?>" required>
                                        <?php if($alamatError != '') : ?>
                                            <small id="alamatHelper" class="form-text text-danger"><?=$alamatError;?></small>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h4>Data Trayek</h4>
                        </div>
                        <div class="card-body">
                            <table id="datar7" class="table table-bordered table-striped dataTable dtr-inline" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode Home Base</th>
                                        <th>Home Base</th>
                                        <th>Alamat</th>
                                        <th width="150">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1;
                                    foreach ($homebase->getResult() as $item) : ?>
                                        <tr>
                                            <td><?= $i; ?></td>
                                            <td><?= $item->kode; ?></td>
                                            <td><?= $item->home_base; ?></td>
                                            <td><?= $item->alamat; ?></td>
                                            <td align="center">
                                                <a href="<?=site_url('dashboard/editHomeBase/'.$item->id);?>" class="btn btn-info"><i class="fas fa-edit"></i></a>
                                                <a href="<?=site_url('dashboard/deleteHomeBase/'.$item->id);?>" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    <?php $i++;
                                    endforeach; ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode Home Base</th>
                                        <th>Home Base</th>
                                        <th>Alamat</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<?= $this->endSection() ?>

<?= $this->section('javascript') ?>
<script src="<?=base_url('assets/adminlte/plugins/datatables/jquery.dataTables.min.js');?>"></script>
    <script src="<?=base_url('assets/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js');?>"></script>
    <script src="<?=base_url('assets/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js');?>"></script>
    <script src="<?=base_url('assets/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js');?>"></script>
    <script>
        $(function () {
            $('#datar7').DataTable({
                "responsive": true,
                "autoWidth": false,
            });
        });
</script>
<?= $this->endSection() ?>