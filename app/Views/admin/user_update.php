<?= $this->extend('layout') ?>

<?= $this->section('content') ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Trayek</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><a href="<?=site_url('dashboard');?>">Dashboard</a></li>
                        <li class="breadcrumb-item active">Users</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col">
                    <?php
                        $inputs = session()->getFlashdata('inputs');
                        $errors = session()->getFlashdata('errors');
                        $pesan = session()->getFlashdata('pesan');
                        $pesanError = session()->getFlashdata('pesan_error');
                        $usernameInput =  isset($inputs['username']) ? $inputs['username'] : '';
                        $nameInput =  isset($inputs['name']) ? $inputs['name'] : '';
                        $levelInput =  isset($inputs['level']) ? $inputs['level'] : '';

                        $usernameError =  isset($errors['username']) ? $errors['username'] : '';
                        $nameError =  isset($errors['name']) ? $errors['name'] : '';
                        $levelError =  isset($errors['level']) ? $errors['level'] : '';
                        
                        if ($usernameInput != '' || isset($data->username)) {
                            $usernameInput = $data->username;
                        }

                        if ($nameInput != '' || isset($data->name)) {
                            $nameInput = $data->name;
                        }

                        if ($levelInput != '' || isset($data->level)) {
                            $levelInput = $data->level;
                        }
                    ;?>
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Update Data User</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" action="<?=site_url('dashboard/updateUser');?>" method="POST">
                            <div class="card-body">
                                <?php if($pesan): ?>
                                    <div class="alert alert-success" role="alert">
                                        <?=$pesan;?>
                                    </div>
                                <?php endif;?>
                                <?php if($pesanError): ?>
                                    <div class="alert alert-danger" role="alert">
                                        <?=$pesanError;?>
                                    </div>
                                <?php endif;?>
                                <div class="form-group">
                                    <label for="inputUsername">Username</label>
                                    <input type="text" name="username" class="form-control" id="inputUsername" placeholder="Username" value="<?=$usernameInput;?>" required readonly>
                                    <?php if($usernameError != '') : ?>
                                        <small id="usernameHelper" class="form-text text-danger"><?=$usernameError;?></small>
                                    <?php endif;?>
                                </div>
                                <div class="form-group">
                                    <label for="inputNama">Nama</label>
                                    <input type="text" name="name" class="form-control" id="inputNama" placeholder="Username" value="<?=$nameInput;?>" required>
                                    <?php if($nameError != '') : ?>
                                        <small id="nameHelper" class="form-text text-danger"><?=$nameError;?></small>
                                    <?php endif;?>
                                </div>
                                <div class="form-group">
                                    <label for="inputLevel">Level</label>
                                    <select name="level" id="inputLevel" class="form-control">
                                        <option value="" selected disabled>PILIH</option>
                                        <option value="2">Supir</option>
                                    </select>
                                    <?php if($levelError != '') : ?>
                                        <small id="levelHelper" class="form-text text-danger"><?=$levelError;?></small>
                                    <?php endif;?>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="inputLevel">Trayek</label>
                                        <select name="trayek" id="inputTrayek" class="form-control">
                                            <option value="" selected disabled>PILIH</option>
                                            <?php foreach($trayek->getResult() as $item) : ?>
                                                <option value="<?=$item->kode_awal;?>::<?=$item->kode_akhir;?>"><?=$item->home_base;?> - <?=$item->home_base_akhir;?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php if (count($trayekSupir) > 0): ?>
                <div class="row">
                    <div class="col">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Trayek User</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <h5>Trayek <?=$trayekSupir[0]->home_base." - ".$trayekSupir[0]->home_base_akhir;?></h5>
                                <table id="datar7" class="table table-bordered table-striped dataTable dtr-inline" width="100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Mobil</th>
                                            <th>Trayek Awal</th>
                                            <th>Trayek Akhir</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=1; foreach ($trayekSupir as $key => $value): ?>
                                            <tr>
                                                <td><?=$i;?></td>
                                                <td><?=$value->type."-".$value->nopol;?></td>
                                                <td><?=$value->trayek_awal;?></td>
                                                <td><?=$value->trayek_akhir;?></td>
                                            </tr>
                                        <?php $i++; endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif;?>
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>

<?php if($levelInput != ''): ?>
    <script>
        var level = document.getElementById('inputLevel')
        setTimeout(() => {
            level.value = "<?=$levelInput;?>"
        }, 500);
    </script>
<?php endif;?>

<?php if(count($trayekSupir) > 0): ?>
    <script>
        var trayek = document.getElementById('inputTrayek')
        setTimeout(() => {
            trayek.value = "<?=$trayekSupir[0]->kode_base."::".$trayekSupir[0]->kode_base_akhir;?>"
        }, 500);
    </script>
<?php endif;?>
<?= $this->endSection() ?>