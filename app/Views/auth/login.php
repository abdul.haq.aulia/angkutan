<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- font awsome -->
    <!-- Font Awsome -->
    <link href="<?=base_url('assets/fontAwsome/css/all.css');?>" rel="stylesheet">

    <style>
        html, body {
            height: 100%;
            background-color: #8bcdcd;
        }
    </style>
    <title>R7 - Login</title>
  </head>
  <body>
    <!-- Handle Message Error -->
    <?php
        $inputs = session()->getFlashdata('inputs');
        $errors = session()->getFlashdata('errors');
        $pesan = session()->getFlashdata('pesan');
        $usernameInput =  isset($inputs['username']) ? $inputs['username'] : '';
        $passwordInput =  isset($inputs['password']) ? $inputs['password'] : '';

        $usernameError =  isset($errors['username']) ? $errors['username'] : '';
        $passwordError =  isset($errors['password']) ? $errors['password'] : '';
    ;?>
    <!-- End Of Handle Message Error -->
        <div class="row h-100 justify-content-center">
            <div class="col-md-4 my-auto">
                <div class="card px-2 py-2 mx-3 shadow">
                    <div class="card-title text-center pt-3">
                        <h3>Aplikasi Pencatatan R7 Online</h3>
                    </div>
                    <div class="card-body">
                        <?php if($pesan): ?>
                            <div class="alert alert-danger" role="alert">
                                <?=$pesan;?>
                            </div>
                        <?php endif;?>
                        <form action="login" method="post">
                            <div class="form-group">
                                <label for="inputUsername">Username</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">
                                            <i class="fas fa-user"></i>
                                        </span>
                                    </div>
                                    <input type="text" name="username" class="form-control" id="inputUsername" aria-describedby="usernameHelp" placeholder="Username" value="<?=$usernameInput;?>" required aria-describedby="basic-addon1">
                                </div>
                                <?php if($usernameError == '') : ?>
                                    <small id="usernameHelp" class="form-text text-muted">Silahkan masukkan username Anda</small>
                                <?php else : ?>
                                    <small id="usernameHelp" class="form-text text-danger"><?=$usernameError;?></small>
                                <?php endif;?>
                            </div>
                            <div class="form-group pb-3">
                                <label for="inputPassword">Password</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">
                                            <i class="fas fa-key"></i>
                                        </span>
                                    </div>
                                    <input type="password" name="password" class="form-control" id="inputPassword" aria-describedby="passwordHelp" placeholder="Password" value="<?=$passwordInput;?>" required aria-describedby="basic-addon1">
                                </div>
                                <?php if($passwordError == '') : ?>
                                    <small id="usernameHelp" class="form-text text-muted">Silahkan masukkan password Anda</small>
                                <?php else : ?>
                                    <small id="usernameHelp" class="form-text text-danger"><?=$passwordError;?></small>
                                <?php endif;?>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block">Login</button>
                        </form>
                        <p class="pt-3 text-center text-muted">Copyright@vailoProject2020</p>
                    </div>
                </div>
            </div>
        </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script defer src="<?=base_url('assets/fontAwsome/js/all.js');?>"></script>
  </body>
</html>