<?php namespace App\Controllers;

use App\Models\UserModel;

class Login extends BaseController
{
	
	public function __construct()
	{
		helper('form');
        $this->form_validation = \Config\Services::validation();
	}
	
	public function index()
	{
		return view('auth/login');
	}

	public function doLogin() {
		$userModel = new UserModel();

		$username = $this->request->getPost('username');
		$password = $this->request->getPost('password');
		$passwordEcn = md5($password);
		
		$data = [
            'username'  => $username,
            'password'  => $password
		];
		
		$this->validate([
            'username' => 'required',
            'password'  => 'required'
		]);
		
		if($this->form_validation->run($data, 'login') == FALSE){
            // mengembalikan nilai input yang sudah dimasukan sebelumnya
            session()->setFlashdata('inputs', $this->request->getPost());
            // memberikan pesan error pada saat input data
            session()->setFlashdata('errors', $this->form_validation->getErrors());
            // kembali ke halaman form
            return redirect()->to('login');
        } else {
			// check data
			$user = $userModel->where('username', $username)
							->where('password', md5($password))
							->first();

			if(empty($user)) {
				session()->setFlashdata('pesan', 'Login gagal, silahkan periksa username dan password anda kembali!');
				// kembali ke halaman form
				return redirect()->to('login');
			}

			// save to session
			session()->set('id', $user->id);
			session()->set('nama', $user->name);
			session()->set('username', $user->username);
			session()->set('level', $user->level);
			session()->set('IS_LOGIN', true);
            session()->setFlashdata('success', 'Registration Login');
			// kembali ke halaman register
			if ($user->level == 1) {
				return redirect()->to('dashboard');
			} elseif($user->level = 2) {
				return redirect()->to('dashboard/add');
			}
        }
	}

	public function doLogout(){
		session_destroy();
		return redirect()->to('login');
	}
	//--------------------------------------------------------------------

}
