<?php namespace App\Controllers;

use App\Models\BaseModel;
use App\Models\MobilModel;
use App\Models\NodesModel;
use App\Models\R7Model;
use App\Models\RegionalModel;
use App\Models\TrayekModel;
use App\Models\UserModel;

class Home extends BaseController
{

	public function __construct()
	{
		helper('form');
        $this->form_validation = \Config\Services::validation();
	}

	public function index()
	{
		return redirect()->to(base_url('dashboard'));
	}

	public function dashboard() {
		$username = session()->get('username');
		$dataR7 = new R7Model();
		$table = $dataR7->getDataByUser($username);

		$new_date = new \DateTime;

		// returns Sunday, Feb 4 2018
		$start = $new_date->modify('sunday this week')->format('d');
		$end = $new_date->modify('saturday this week')->format('d');

		// alltime
		$alltime = 0;
		if (session()->get('level') == 2) {
			$alltime = $dataR7->where('username', $username)->countAll();
		}
		if (session()->get('level') == 1) {
			$alltime = $dataR7->countAll();
		}
		// month
		$month = 0;
		if (session()->get('level') == 2) {
			$month = $dataR7->where('username', $username)->where('year', date('Y'))->countAll();
		}
		if (session()->get('level') == 1) {
			$month = $dataR7->where('year', date('Y'))->countAll();
		}
		// week
		$week = 0;
		if (session()->get('level') == 2) {
			$week = $dataR7->where('username', $username)->where('year', date('Y'))->where('month', date('m'))->where('day >=', $start)->where('day <=', $end)->countAll();
		}
		if (session()->get('level') == 1) {
			$week = $dataR7->where('year', date('Y'))->where('month', date('m'))->where('day >=', $start)->where('day <=', $end)->countAll();
		}
		// day
		$day = 0;
		if (session()->get('level') == 2) {
			$day = $dataR7->where('username', $username)->where('year', date('Y'))->where('month', date('m'))->where('day', date('d'))->countAll();
		}
		if (session()->get('level') == 1) {
			$day = $dataR7->where('year', date('Y'))->where('month', date('m'))->where('day', date('d'))->countAll();
		}

		$dboRegional = new RegionalModel();
		$regional = $dboRegional->findAll();
		foreach($regional as $item) {
			$dboTrayek = new TrayekModel();
			$trayek = $dboTrayek->select('kode')->where('kode_regional', $item->kode)->get();

			if (count($trayek->getResult()) > 0) {
				$dboR7 = new R7Model();
				$dataTraye = [];
				foreach($trayek->getResult() as $itemTrayek) {
					$dataTraye[] = $itemTrayek ->kode;
				}

				$r7 = $dboR7->whereIn('kode_trayek', $dataTraye)->get();
	
				$totalTransaksi = 0;
				$totalBerat = 0;
				$totalBiaya = 0;
				foreach($r7->getResult() as $itemR7) {
					$totalTransaksi++;
					$totalBerat += $itemR7->berat;
	
					$harga = $dboTrayek->where('kode', $itemR7->kode_trayek)->first();
	
					$totalBiaya += $totalBerat * $harga->harga_perkg;
				}
	
				$item->total_r7 = $totalTransaksi;
				$item->total_berat = $totalBerat;
				$item->total_harga = $totalBiaya;
			}
		}

		$data = [
			'alltime'   => $alltime,
			'month'		=> $month,
			'week'		=> $week,
			'day'		=> $day,
		];
		return view('driver/home', compact('data', 'table', 'regional'));
	}

	public function lacak() {

		$nomor_r7 = $this->request->getPost('nomor_r7');
		
		$data = [
			'nomor_r7'  => $nomor_r7,
		];
		
		$this->validate([
			'nomor_r7' => 'required',
		]);
		
		
		if($this->form_validation->run($data, 'lacak') == FALSE){
			// mengembalikan nilai input yang sudah dimasukan sebelumnya
            session()->setFlashdata('inputs', $this->request->getPost());
            // memberikan pesan error pada saat input data
            session()->setFlashdata('errors', $this->form_validation->getErrors());
            // kembali ke halaman form
            return redirect()->to(base_url(''));
        } else {
			$data = [
				'response' => [
					'no' => 12987239847,
					'tanggal' => date('Y-m-d H:i:s')
				]
			];
            // memberikan pesan error pada saat input data
            session()->setFlashdata('hasil', $data);
            // kembali ke halaman form
            return redirect()->to(base_url(''));
        }
	}

	public function addR7(){
		$username = session()->get('username');
		$id_supir = session()->get('id');
		$dataR7 = new R7Model();
		$table = $dataR7->getDataByUserNow($username);

		$dboTrayek = new TrayekModel();
		$trayek = $dboTrayek->getTrayekBySupir($id_supir);

		return view('driver/add', compact('table', 'trayek'));
	}

	public function getReport() {
		$username = session()->get('username');
		$dataR7 = new R7Model();
		$table = $dataR7->getDataByUser($username);

		$dboRegional = new RegionalModel();
		$regional = $dboRegional->findAll();
		foreach($regional as $item) {
			$dboTrayek = new TrayekModel();
			$trayek = $dboTrayek->select('kode')->where('kode_regional', $item->kode)->get();

			if (count($trayek->getResult()) > 0) {
				$dboR7 = new R7Model();
				$dataTraye = [];
				foreach($trayek->getResult() as $itemTrayek) {
					$dataTraye[] = $itemTrayek ->kode;
				}

				$r7 = $dboR7->whereIn('kode_trayek', $dataTraye)->get();
	
				$totalTransaksi = 0;
				$totalBerat = 0;
				$totalBiaya = 0;
				foreach($r7->getResult() as $itemR7) {
					$totalTransaksi++;
					$totalBerat += $itemR7->berat;
	
					$harga = $dboTrayek->where('kode', $itemR7->kode_trayek)->first();
	
					$totalBiaya += $totalBerat * $harga->harga_perkg;
				}
	
				$item->total_r7 = $totalTransaksi;
				$item->total_berat = $totalBerat;
				$item->total_harga = $totalBiaya;
			}
		}

		return view('driver/laporan', compact('table', 'regional'));
	}

	public function update($id) {
		$dboR7 = new R7Model();
		$data = $dboR7->find($id);

		$dboTrayek = new TrayekModel();
		$trayek = $dboTrayek->getAllTrayek();

		return view('driver/update', compact('data','trayek'));
	}

	public function users() {
		$dboUser = new UserModel();
		$users = $dboUser->where('deleted_at', NULL)->findAll();
		return view('admin/users', compact('users'));
	}

	public function trayek() {
		$dboTrayek = new TrayekModel();
		$trayek = $dboTrayek->getAllTrayek();

		$dboMobil = new MobilModel();
		$mobil = $dboMobil->findAll();

		$dboRegional = new RegionalModel();
		$regional = $dboRegional->findAll();

		$dboBase = new BaseModel();
		$base = $dboBase->findAll();

		$dboSupir = new UserModel();
		$supir = $dboSupir->where('level', 2)->where('deleted_at IS NULL')->get();

		$dboNodes = new NodesModel();
		$nodes = $dboNodes->where('deleted_at IS NULL')->get();

		return view('admin/trayek', compact('trayek', 'base', 'regional', 'mobil', 'supir', 'nodes'));
	}

	public function urutanTrayek() {
		$dboTrayek = new TrayekModel();
		$trayek = $dboTrayek->getRecapTrayek();

		return view('admin/urutan_trayek', compact('trayek'));
	}

	public function urutanDetail($awal, $akhir, $regional) {
		$dboTrayek = new TrayekModel();
		$trayek = $dboTrayek->getDetailTrayek($awal, $akhir, $regional);
		$dboRegional = new RegionalModel();
		$regional = $dboRegional->where('kode', $regional)->first()->nama;
		$dboBase = new BaseModel();
		$trayek_awal = $dboBase->where('kode', $awal)->first()->home_base;
		$trayek_akhir = $dboBase->where('kode', $akhir)->first()->home_base;

		return view('admin/urutan_trayek_detail', compact('trayek', 'trayek_awal', 'trayek_akhir', 'regional'));
	}

	public function mobil() {
		$dboMobil = new MobilModel();
		$mobil = $dboMobil->where('deleted_at IS NULL', NULL)->get();
		return view('admin/mobil', compact('mobil'));
	}

	public function base() {
		$dboHomebase = new BaseModel();
		$homebase = $dboHomebase->where('deleted_at IS NULL', NULL)->get();
		return view('admin/homeBase', compact('homebase'));
	}

	public function regional() {
		$dboRegional = new RegionalModel();
		$regional = $dboRegional->where('deleted_at IS NULL', NULL)->get();
		return view('admin/regional', compact('regional'));
	}

	public function editUser($id){
		$dboUser = new UserModel();
		$data = $dboUser->find($id);
		$dboTrayek = new TrayekModel();
		$trayek = $dboTrayek->getRecapTrayek();

		$trayekSupir = [];
		if ($data->level == 2) {
			$trayekSupir = $dboTrayek->getTrayekBySupir($data->id)->getResult();
		}

		return view('admin/user_update', compact('data', 'trayek', 'trayekSupir'));
	}

	public function editTrayek($id){
		$dboTrayek = new TrayekModel();
		$data = $dboTrayek->find($id);

		$dboMobil = new MobilModel();
		$mobil = $dboMobil->findAll();

		$dboRegional = new RegionalModel();
		$regional = $dboRegional->findAll();

		$dboBase = new BaseModel();
		$base = $dboBase->findAll();

		$dboNodes = new NodesModel();
		$nodes = $dboNodes->where('deleted_at IS NULL')->get();

		$dboSupir = new UserModel();
		$supir = $dboSupir->where('level', 2)->where('deleted_at IS NULL')->get();

		return view('admin/trayek_update', compact('data', 'mobil', 'regional', 'base', 'nodes', 'supir'));
	}

	public function editMobil($id){
		$dbo = new MobilModel();
		$data = $dbo->find($id);
		return view('admin/mobil_update', compact('data'));
	}

	public function editRegional($id){
		$dbo = new RegionalModel();
		$data = $dbo->find($id);
		return view('admin/regional_update', compact('data'));
	}

	public function editHomeBase($id){
		$dbo = new BaseModel();
		$data = $dbo->find($id);
		return view('admin/homeBase_update', compact('data'));
	}

	public function nodes(){
		$dbo = new NodesModel();
		$nodes = $dbo->where('deleted_at IS NULL')->get();
		return view('admin/nodes', compact('nodes'));
	}

	public function editNodes($id){
		$dbo = new NodesModel();
		$data = $dbo->find($id);
		return view('admin/nodes_update', compact('data'));
	}
	//--------------------------------------------------------------------

}
