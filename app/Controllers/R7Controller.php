<?php namespace App\Controllers;

use App\Entities\HomeBase;
use App\Entities\Mobil;
use App\Entities\Nodes;
use App\Entities\R7;
use App\Entities\Regional;
use App\Entities\Trayek;
use App\Entities\User;
use App\Models\BaseModel;
use App\Models\MobilModel;
use App\Models\NodesModel;
use App\Models\R7Model;
use App\Models\RegionalModel;
use App\Models\TrayekModel;
use App\Models\UserModel;
use CodeIgniter\API\ResponseTrait;

class R7Controller extends BaseController
{
	use ResponseTrait;
	
	public function __construct()
	{
		helper('form');
        $this->form_validation = \Config\Services::validation();
	}

	public function addR7() {
		$username = session()->get('username');

		$nomor = $this->request->getPost('nomor');
		$jumlah = $this->request->getPost('jumlah');
		$berat = $this->request->getPost('berat');
		$trayek = $this->request->getPost('trayek');
		
		$data = [
			'nomor'  => $nomor,
			'jumlah' => $jumlah,
			'berat'  => $berat,
			'trayek' => $trayek
		];
		
		$this->validate([
            'nomor'  => 'required',
			'jumlah' => 'required',
			'berat'  => 'required',
			'trayek' => 'required',
		]);
		
		if($this->form_validation->run($data, 'addR7') == FALSE){
            // mengembalikan nilai input yang sudah dimasukan sebelumnya
            session()->setFlashdata('inputs', $this->request->getPost());
            // memberikan pesan error pada saat input data
            session()->setFlashdata('errors', $this->form_validation->getErrors());
            // kembali ke halaman form
            return redirect()->to(base_url('dashboard/add'));
        } else {
			// check data
			$dboR7 = new R7Model();
			$R7 = $dboR7->where('nomor', $nomor)
								->first();

			if (!empty($R7)) {
				session()->setFlashdata('pesan_error', "Nomor R7 <strong>$nomor</strong>, tidak dapat disimpan karena terdapat nomor yang sama.");
				return redirect()->to(base_url('dashboard/add'));
			}

			try {
				$dboR7 = new R7Model();
				$newR7 = new R7();
				$newR7->kode_trayek = $trayek;
				$newR7->user_id = $username;
				$newR7->nomor = $nomor;
				$newR7->jumlah = $jumlah;
				$newR7->berat = $berat;
				$dboR7->save($newR7);
				session()->setFlashdata('pesan', "Nomor R7 <strong>$nomor</strong> berhasil disimpan");
			} catch (\Throwable $th) {
				session()->setFlashdata('pesan_error', $th->getMessage());
			}
			
            return redirect()->to(base_url('dashboard/add'));
        }
	}

	public function updateR7() {
		$username = session()->get('username');

		$nomor = $this->request->getPost('nomor');
		$jumlah = $this->request->getPost('jumlah');
		$berat = $this->request->getPost('berat');
		$trayek = $this->request->getPost('trayek');
		
		$data = [
			'nomor'  => $nomor,
			'jumlah' => $jumlah,
			'berat'  => $berat,
			'trayek' => $trayek
		];
		
		$this->validate([
            'nomor'  => 'required',
			'jumlah' => 'required',
			'berat'  => 'required',
			'trayek' => 'required',
		]);
		
		// check data
		$dboR7 = new R7Model();
		$R7 = $dboR7->where('nomor', $nomor)
							->first();

		if($this->form_validation->run($data, 'addR7') == FALSE){
            // mengembalikan nilai input yang sudah dimasukan sebelumnya
            session()->setFlashdata('inputs', $this->request->getPost());
            // memberikan pesan error pada saat input data
            session()->setFlashdata('errors', $this->form_validation->getErrors());
            // kembali ke halaman form
            return redirect()->to(base_url('dashboard/edit/'.$R7->id));
        } else {

			if (empty($R7)) {
				session()->setFlashdata('pesan_error', "Nomor R7 <strong>$nomor</strong>, tidak dapat disimpan karena terdapat nomor yang sama.");
				return redirect()->to(base_url('dashboard/edit/'.$R7->id));
			}

			try {
				$R7->nomor = $nomor;
				$R7->jumlah = $jumlah;
				$R7->berat = $berat;
				$R7->kode_trayek = $trayek;
				$dboR7->save($R7);
				session()->setFlashdata('pesan', "Nomor R7 <strong>$nomor</strong> berhasil diubah");
			} catch (\Throwable $th) {
				session()->setFlashdata('pesan_error', $th->getMessage());
			}
			
            return redirect()->to(base_url('dashboard/add'));
        }
	}

	public function getReport($filter, $satu, $dua) {
		$dboR7 = new R7Model();
		$table = [];
		if ($filter == 'bulan') {
			$table = $dboR7->getDataByMonth($satu, $dua);
		} elseif ($filter == 'tanggal') {
			$table = $dboR7->getDataByDate($satu, $dua);
		}

		$jenis = [
			'filter' => $filter,
			'satu' => $satu,
			'dua' => $dua
		];

		return view('driver/laporan_filter', compact('table','jenis'));

	}

	public function addUser() {
		$username = $this->request->getPost('username');
		$password = $this->request->getPost('password');
		$name = $this->request->getPost('name');
		$level = $this->request->getPost('level');
		
		$data = [
			'username'  => $username,
			'password' => $password,
			'name' => $name,
			'level'  => $level,
		];
		
		$this->validate([
            'username'  => 'required',
			'password' => 'required',
			'name' => 'required',
			'level'  => 'required',
		]);
		
		if($this->form_validation->run($data, 'addUser') == FALSE){
            // mengembalikan nilai input yang sudah dimasukan sebelumnya
            session()->setFlashdata('inputs', $this->request->getPost());
            // memberikan pesan error pada saat input data
            session()->setFlashdata('errors', $this->form_validation->getErrors());
            // kembali ke halaman form
            return redirect()->to(base_url('dashboard/users'));
        } else {
			// check data
			$dboUser = new UserModel();
			$user = $dboUser->where('username', $username)
								->first();

			if (!empty($R7)) {
				session()->setFlashdata('pesan_error', "Data User <strong>$username</strong>, tidak dapat disimpan karena terdapat username yang sama.");
				return redirect()->to(base_url('dashboard/add'));
			}

			try {
				$dboUser = new UserModel();
				$newUser = new User();
				$newUser->username = $username;
				$newUser->password = md5($password);
				$newUser->name = $name;
				$newUser->level = $level;
				$dboUser->save($newUser);
				session()->setFlashdata('pesan', "Data User <strong>$username</strong> berhasil disimpan");
			} catch (\Throwable $th) {
				session()->setFlashdata('pesan_error', $th->getMessage());
			}
			
            return redirect()->to(base_url('dashboard/users'));
        }
	}

	public function updateUser() {
		$username = $this->request->getPost('username');
		$name = $this->request->getPost('name');
		$level = $this->request->getPost('level');
		$trayek = $this->request->getPost('trayek');
		$trayek = explode("::", $trayek);
		$homebase_awal = $trayek[0];
		$homebase_akhir = $trayek[1];
		
		$data = [
			'username'  => $username,
			'name' => $name,
			'level'  => $level
		];
		
		$this->validate([
            'username'  => 'required',
			'name' => 'required',
			'level'  => 'required'
		]);
		
		// check data
		$dboUwser = new UserModel();
		$user = $dboUwser->where('username', $username)
							->first();
		if($this->form_validation->run($data, 'updateUser') == FALSE){
            // mengembalikan nilai input yang sudah dimasukan sebelumnya
            session()->setFlashdata('inputs', $this->request->getPost());
            // memberikan pesan error pada saat input data
            session()->setFlashdata('errors', $this->form_validation->getErrors());
            // kembali ke halaman form
            return redirect()->to(base_url('dashboard/editUser/'.$user->id));
        } else {

			if (empty($user)) {
				session()->setFlashdata('pesan_error', "User dengan username <strong>$username</strong>, data tidak ditemukan");
				return redirect()->to(base_url('dashboard/editUser/'.$user->id));
			}

			try {
				$dboUwser->set('name', $name);
				$dboUwser->set('level', $level);
				$dboUwser->where('username', $username);
				$dboUwser->update();
				
				$dboTrayek = new TrayekModel();
				$previewsData = $dboTrayek->where('kode_supir', $user->id)->first();
				// set to null previews trayek
				$dboTrayek->set('kode_supir', NULL);
				$dboTrayek->where('kode_base', $previewsData->kode_base)
						  ->where('kode_base_akhir', $previewsData->kode_base_akhir);
				$dboTrayek->update();

				// set to new supir to trayek
				$dboTrayek->set('kode_supir', $user->id);
				$dboTrayek->where('kode_base', $homebase_awal)
						  ->where('kode_base_akhir', $homebase_akhir);
				$dboTrayek->update();
				session()->setFlashdata('pesan', "Username <strong>$username</strong> berhasil diubah");
			} catch (\Throwable $th) {
				session()->setFlashdata('inputs', $this->request->getPost());
				session()->setFlashdata('pesan_error', $th->getMessage());
			}
			
            return redirect()->to(base_url('dashboard/users'));
        }
	}

	public function deleteUser($id){
		$dboUser = new UserModel();
		$data = $dboUser->find($id);
		$data->deleted_at = date('Y-m-d H:i:s');
		$dboUser->save($data);
		session()->setFlashdata('pesan', "Data user <strong>{$data->name}</strong> berhasil dihapus");
		return redirect()->to(base_url('dashboard/users'));
	}

	public function R7Controller($id) {
		$dboUser = new UserModel();
		$data = $dboUser->find($id);
		$data->password = md5($data->username."123123");
		$dboUser->save($data);
		session()->setFlashdata('pesan', "Data Password <strong>{$data->name}</strong> telah direset!");
		return redirect()->to(base_url('dashboard/users'));
	}

	public function addTrayek() {
		$username = session()->get('username');

		// $supir = $this->request->getPost('supir');
		$keberangkatan = $this->request->getPost('keberangkatan');
		$trayekAwal = $this->request->getPost('trayekAwal');
		$trayekAkhir = $this->request->getPost('trayekAkhir');
		$kodebase = $this->request->getPost('kodebase');
		$kodebaseakhir = $this->request->getPost('kodebaseakhir');
		$koderegional = $this->request->getPost('koderegional');
		$kodemobil = $this->request->getPost('kodemobil');
		$jumlahkbm = $this->request->getPost('jumlahkbm');
		$plpi = $this->request->getPost('plpi');
		$hargaperkm = $this->request->getPost('hargaperkm');
		$hargaperkg = $this->request->getPost('hargaperkg');
		$akhirpks = $this->request->getPost('akhirpks');
		$kappks = $this->request->getPost('kappks');
		$kapreal = $this->request->getPost('kapreal');
		$keterangan = $this->request->getPost('keterangan');
		
		$data = [
			// 'supir'  => $supir,
			'keberangkatan'  => $keberangkatan,
			'trayekAwal' => $trayekAwal,
			'trayekAkhir' => $trayekAkhir,
			'kodebase'  => $kodebase,
			'kodebaseakhir'  => $kodebaseakhir,
			'koderegional' => $koderegional,
			'kodemobil' => $kodemobil,
			'jumlahkbm' => $jumlahkbm,
			'plpi' => $plpi,
			'hargaperkm' => $hargaperkm,
			'hargaperkg' => $hargaperkg,
			'akhirpks' => $akhirpks,
			'kappks' => $kappks,
			'kapreal' => $kapreal,
			'keterangan' => $keterangan
		];
		
		$this->validate([
			// 'supir'  => 'required',
			'keberangkatan' => 'required',
			'trayekAwal' => 'required',
			'trayekAkhir' => 'required',
			'kodebase'  => 'required',
			'kodebaseakhir'  => 'required',
			'koderegional' => 'required',
			'kodemobil' => 'required',
			// 'jumlahkbm' => 'required',
			// 'plpi' => 'required',
			// 'hargaperkm' => 'required',
			'hargaperkg' => 'required',
			// 'akhirpks' => 'required',
			// 'kappks' => 'required',
			// 'kapreal' => 'required',
		]);
		
		if($this->form_validation->run($data, 'addTrayek') == FALSE){
            // mengembalikan nilai input yang sudah dimasukan sebelumnya
            session()->setFlashdata('inputs', $this->request->getPost());
            // memberikan pesan error pada saat input data
            session()->setFlashdata('errors', $this->form_validation->getErrors());
            // kembali ke halaman form
            return redirect()->to(base_url('dashboard/trayek'));
        } else {
			// check data
			// $dboTrayek = new TrayekModel();
			// $R7 = $dboTrayek->where('kode', $kode)
			// 					->first();

			// if (!empty($R7)) {
			// 	session()->setFlashdata('pesan_error', "Nomor Trayek <strong>$kode</strong>, tidak dapat disimpan karena terdapat kode yang sama.");
			// 	return redirect()->to(base_url('dashboard/trayek'));
			// }
			// Generate kode
			$dboTrayek = new TrayekModel();
			$R7 = $dboTrayek->orderBy('created_at', 'DESC')
								->first();
			$number = 0;
			if (!empty($R7)) {
				$number = $R7->id;				
			}
			$lastNumber = $number + 1;
			$lastNumber = str_pad($lastNumber, 21, "0", STR_PAD_LEFT);
			$kode = 'TRY-' . $lastNumber;

			try {
				$dboTrayek = new TrayekModel();
				$newTrayek = new Trayek();
				$newTrayek->kode = $kode;
				// $newTrayek->kode_supir = $supir;
				$newTrayek->keberangkatan = $keberangkatan;
				$newTrayek->trayek_awal = $trayekAwal;
				$newTrayek->trayek_akhir = $trayekAkhir;
				$newTrayek->kode_base = $kodebase;
				$newTrayek->kode_base_akhir = $kodebaseakhir;
				$newTrayek->kode_regional = $koderegional;
				$newTrayek->kode_mobil = $kodemobil;
				$newTrayek->jumlah_kbm = $jumlahkbm;
				$newTrayek->plpi = $plpi;
				$newTrayek->harga_perkm = $hargaperkm;
				$newTrayek->harga_perkg = $hargaperkg;
				$newTrayek->akhir_pks = $akhirpks;
				$newTrayek->kap_pks = $kappks;
				$newTrayek->kap_real = $kapreal;
				$newTrayek->keterangan = $keterangan;
				$dboTrayek->save($newTrayek);
				session()->setFlashdata('pesan', "Nomor R7 <strong>$kode</strong> berhasil disimpan");
			} catch (\Throwable $th) {
				session()->setFlashdata('inputs', $this->request->getPost());
				session()->setFlashdata('pesan_error', $th->getMessage());
			}
			
            return redirect()->to(base_url('dashboard/trayek'));
        }
	}

	public function deleteTrayek($id) {
		$dboTrayek = new TrayekModel();
		$data = $dboTrayek->find($id);
		$data->deleted_at = date('Y-m-d H:i:s');
		$dboTrayek->save($data);
		session()->setFlashdata('pesan', "Data trayek <strong>{$data->kode}</strong> berhasil dihapus");
		return redirect()->to(base_url('dashboard/trayek'));
	}

	public function updateTrayek() {
		$supir = $this->request->getPost('supir');
		$keberangkatan = $this->request->getPost('keberangkatan');
		$kode = $this->request->getPost('kode');
		$trayekAwal = $this->request->getPost('trayekAwal');
		$trayekAkhir = $this->request->getPost('trayekAkhir');
		$kodebase = $this->request->getPost('kodebase');
		$kodebaseakhir = $this->request->getPost('kodebaseakhir');
		$koderegional = $this->request->getPost('koderegional');
		$kodemobil = $this->request->getPost('kodemobil');
		$jumlahkbm = $this->request->getPost('jumlahkbm');
		$plpi = $this->request->getPost('plpi');
		$hargaperkm = $this->request->getPost('hargaperkm');
		$hargaperkg = $this->request->getPost('hargaperkg');
		$akhirpks = $this->request->getPost('akhirpks');
		$kappks = $this->request->getPost('kappks');
		$kapreal = $this->request->getPost('kapreal');
		$keterangan = $this->request->getPost('keterangan');
		
		$data = [
			'kode'  => $kode,
			'supir'  => $supir,
			'keberangkatan'  => $keberangkatan,
			'trayekAwal' => $trayekAwal,
			'trayekAkhir' => $trayekAkhir,
			'kodebase'  => $kodebase,
			'kodebaseakhir'  => $kodebaseakhir,
			'koderegional' => $koderegional,
			'kodemobil' => $kodemobil,
			// 'jumlahkbm' => $jumlahkbm,
			// 'plpi' => $plpi,
			// 'hargaperkm' => $hargaperkm,
			'hargaperkg' => $hargaperkg,
			// 'akhirpks' => $akhirpks,
			// 'kappks' => $kappks,
			// 'kapreal' => $kapreal,
			'keterangan' => $keterangan
		];
		
		$this->validate([
			'supir'  => 'required',
			'keberangkatan'  => 'required',
            'kode'  => 'required',
			'trayekAwal' => 'required',
			'trayekAkhir' => 'required',
			'kodebase'  => 'required',
			'kodebaseakhir'  => 'required',
			'koderegional' => 'required',
			'kodemobil' => 'required',
			// 'jumlahkbm' => 'required',
			// 'plpi' => 'required',
			// 'hargaperkm' => 'required',
			'hargaperkg' => 'required',
			// 'akhirpks' => 'required',
			// 'kappks' => 'required',
			// 'kapreal' => 'required',
		]);

		// check data
		$dboTrayek = new TrayekModel();
		$dataTrayek = $dboTrayek->where('kode', $kode)
							->first();
		
		if($this->form_validation->run($data, 'updateTrayek') == FALSE){
            // mengembalikan nilai input yang sudah dimasukan sebelumnya
            session()->setFlashdata('inputs', $this->request->getPost());
            // memberikan pesan error pada saat input data
            session()->setFlashdata('errors', $this->form_validation->getErrors());
            // kembali ke halaman form
            return redirect()->to(base_url('dashboard/editTrayek/'. $dataTrayek->id));
        } else {

			if (empty($dataTrayek)) {
				session()->setFlashdata('pesan_error', "Nomor Trayek <strong>$kode</strong>, tidak ditemukan.");
				return redirect()->to(base_url('dashboard/editTrayek/'. $dataTrayek->id));
			}

			try {
				$dataTrayek->kode_supir = $supir;
				$dataTrayek->keberangkatan = $keberangkatan;
				$dataTrayek->trayek_awal = $trayekAwal;
				$dataTrayek->trayek_akhir = $trayekAkhir;
				$dataTrayek->kode_base = $kodebase;
				$dataTrayek->kode_base_akhir = $kodebaseakhir;
				$dataTrayek->kode_regional = $koderegional;
				$dataTrayek->kode_mobil = $kodemobil;
				$dataTrayek->jumlah_kbm = $jumlahkbm;
				$dataTrayek->plpi = $plpi;
				$dataTrayek->harga_perkm = $hargaperkm;
				$dataTrayek->harga_perkg = $hargaperkg;
				$dataTrayek->akhir_pks = $akhirpks;
				$dataTrayek->kap_pks = $kappks;
				$dataTrayek->kap_real = $kapreal;
				$dataTrayek->keterangan = $keterangan;
				// dd($dataTrayek);
				$dboTrayek->save($dataTrayek);
				session()->setFlashdata('pesan', "Trayek <strong>$kode</strong> berhasil disimpan");
			} catch (\Throwable $th) {
				session()->setFlashdata('inputs', $this->request->getPost());
				session()->setFlashdata('pesan_error', $th->getMessage());
				return redirect()->to(base_url('dashboard/editTrayek/'. $dataTrayek->id));
			}
			
            return redirect()->to(base_url('dashboard/trayek'));
        }
	}

	public function addMobil() {
		$kode = $this->request->getPost('kode');
		$type = $this->request->getPost('type');
		$tahun = $this->request->getPost('tahun');
		
		$data = [
			'kode'  => $kode,
			'type' => $type,
			'tahun'  => $tahun,
		];
		
		$this->validate([
            'kode'  => 'required',
			'type' => 'required',
			'tahun'  => 'required',
		]);
		
		if($this->form_validation->run($data, 'addMobil') == FALSE){
            // mengembalikan nilai input yang sudah dimasukan sebelumnya
            session()->setFlashdata('inputs', $this->request->getPost());
            // memberikan pesan error pada saat input data
            session()->setFlashdata('errors', $this->form_validation->getErrors());
            // kembali ke halaman form
            return redirect()->to(base_url('dashboard/refferensi/mobil'));
        } else {
			// check data
			$dboMobil = new MobilModel();
			$mobil = $dboMobil->where('kode', $kode)
								->first();

			if (!empty($mobil)) {
				session()->setFlashdata('pesan_error', "Mobil dengan Nomor Polisi <strong>$kode</strong>, tidak dapat disimpan karena terdapat Nomor yang sama.");
				return redirect()->to(base_url('dashboard/refferensi/mobil'));
			}

			try {
				$dboMobil = new MobilModel();
				$newMobil = new Mobil();
				$newMobil->kode = $kode;
				$newMobil->type = $type;
				$newMobil->tahun = $tahun;
				$dboMobil->save($newMobil);
				session()->setFlashdata('pesan', "Mobil dengan nomor <strong>$kode</strong> berhasil disimpan");
			} catch (\Throwable $th) {
				session()->setFlashdata('inputs', $this->request->getPost());
				session()->setFlashdata('pesan_error', $th->getMessage());
			}
			
            return redirect()->to(base_url('dashboard/refferensi/mobil'));
        }
	}

	public function editMobil() {
		$kode = $this->request->getPost('kode');
		$type = $this->request->getPost('type');
		$tahun = $this->request->getPost('tahun');
		
		$data = [
			'kode'  => $kode,
			'type' => $type,
			'tahun'  => $tahun,
		];
		
		$this->validate([
            'kode'  => 'required',
			'type' => 'required',
			'tahun'  => 'required',
		]);
		
		// check data
		$dboMobil = new MobilModel();
		$mobil = $dboMobil->where('kode', $kode)
							->first();

		if($this->form_validation->run($data, 'addMobil') == FALSE){
            // mengembalikan nilai input yang sudah dimasukan sebelumnya
            session()->setFlashdata('inputs', $this->request->getPost());
            // memberikan pesan error pada saat input data
            session()->setFlashdata('errors', $this->form_validation->getErrors());
            // kembali ke halaman form
            return redirect()->to(base_url('dashboard/editMobil/'.$mobil->id));
        } else {

			if (empty($mobil)) {
				session()->setFlashdata('pesan_error', "Data Mobil dengan Nomor <strong>$kode</strong>, Tidak ditemukan!");
				return redirect()->to(base_url('dashboard/editMobil/'.$mobil->id));
			}

			try {
				$mobil->type = $type;
				$mobil->tahun = $tahun;
				$dboMobil->save($mobil);
				session()->setFlashdata('pesan', "Regional dengan kode <strong>$kode</strong> berhasil disimpan");
			} catch (\Throwable $th) {
				session()->setFlashdata('inputs', $this->request->getPost());
				session()->setFlashdata('pesan_error', $th->getMessage());
				return redirect()->to(base_url('dashboard/editMobil/'.$mobil->id));
			}
			
			return redirect()->to(base_url('dashboard/refferensi/mobil'));
		}
	}

	public function deleteMobil($id) {
		$dboMobil = new MobilModel();
		$data = $dboMobil->find($id);
		$data->deleted_at = date('Y-m-d H:i:s');
		$dboMobil->save($data);
		session()->setFlashdata('pesan', "Data Mobil <strong>{$data->kode}</strong> berhasil dihapus");
		return redirect()->to(base_url('dashboard/refferensi/mobil'));
	}

	public function addRegional() {
		$kode = $this->request->getPost('kode');
		$nama = $this->request->getPost('nama');
		$alamat = $this->request->getPost('alamat');
		
		$data = [
			'kode'  => $kode,
			'nama' => $nama,
			'alamat'  => $alamat,
		];
		
		$this->validate([
            'kode'  => 'required',
			'nama' => 'required',
			'alamat'  => 'required',
		]);
		
		if($this->form_validation->run($data, 'addRegional') == FALSE){
            // mengembalikan nilai input yang sudah dimasukan sebelumnya
            session()->setFlashdata('inputs', $this->request->getPost());
            // memberikan pesan error pada saat input data
            session()->setFlashdata('errors', $this->form_validation->getErrors());
            // kembali ke halaman form
            return redirect()->to(base_url('dashboard/refferensi/regional'));
        } else {
			// check data
			$dboRegional = new RegionalModel();
			$regional = $dboRegional->where('kode', $kode)
								->first();

			if (!empty($regional)) {
				session()->setFlashdata('pesan_error', "Data regional dengan Kode <strong>$kode</strong>, tidak dapat disimpan karena terdapat Kode yang sama.");
				return redirect()->to(base_url('dashboard/refferensi/regional'));
			}

			try {
				$dboRegional = new RegionalModel();
				$newRegional = new Regional();
				$newRegional->kode = $kode;
				$newRegional->nama = $nama;
				$newRegional->alamat = $alamat;
				$dboRegional->save($newRegional);
				session()->setFlashdata('pesan', "Regional dengan kode <strong>$kode</strong> berhasil disimpan");
			} catch (\Throwable $th) {
				session()->setFlashdata('inputs', $this->request->getPost());
				session()->setFlashdata('pesan_error', $th->getMessage());
			}
			
            return redirect()->to(base_url('dashboard/refferensi/regional'));
        }
	}

	public function editRegional() {
		$kode = $this->request->getPost('kode');
		$nama = $this->request->getPost('nama');
		$alamat = $this->request->getPost('alamat');
		
		$data = [
			'kode'  => $kode,
			'nama' => $nama,
			'alamat'  => $alamat,
		];
		
		$this->validate([
            'kode'  => 'required',
			'nama' => 'required',
			'alamat'  => 'required',
		]);
		

		// check data
		$dboRegional = new RegionalModel();
		$regional = $dboRegional->where('kode', $kode)
							->first();

		if($this->form_validation->run($data, 'addRegional') == FALSE){
            // mengembalikan nilai input yang sudah dimasukan sebelumnya
            session()->setFlashdata('inputs', $this->request->getPost());
            // memberikan pesan error pada saat input data
            session()->setFlashdata('errors', $this->form_validation->getErrors());
            // kembali ke halaman form
            return redirect()->to(base_url('dashboard/editRegional/'.$regional->id));
        } else {

			if (empty($regional)) {
				session()->setFlashdata('pesan_error', "Data regional dengan Kode <strong>$kode</strong>, Tidak ditemukan!");
				return redirect()->to(base_url('dashboard/editRegional/'.$regional->id));
			}

			try {
				$regional->nama = $nama;
				$regional->alamat = $alamat;
				$dboRegional->save($regional);
				session()->setFlashdata('pesan', "Regional dengan kode <strong>$kode</strong> berhasil disimpan");
			} catch (\Throwable $th) {
				session()->setFlashdata('inputs', $this->request->getPost());
				session()->setFlashdata('pesan_error', $th->getMessage());
				return redirect()->to(base_url('dashboard/editRegional/'.$regional->id));
			}
			
			return redirect()->to(base_url('dashboard/refferensi/regional'));
		}
	}

	public function deleteRegional($id) {
		$dboRegional = new RegionalModel();
		$data = $dboRegional->find($id);
		$data->deleted_at = date('Y-m-d H:i:s');
		$dboRegional->save($data);
		session()->setFlashdata('pesan', "Data Regional <strong>{$data->kode}</strong> berhasil dihapus");
		return redirect()->to(base_url('dashboard/refferensi/regional'));
	}

	public function addHomeBase() {
		$kode = $this->request->getPost('kode');
		$home_base  = $this->request->getPost('home_base');
		$alamat = $this->request->getPost('alamat');
		
		$data = [
			'kode'  => $kode,
			'home_base' => $home_base,
			'alamat'  => $alamat,
		];
		
		$this->validate([
            'kode'  => 'required',
			'home_base' => 'required',
			'alamat'  => 'required',
		]);
		
		if($this->form_validation->run($data, 'addHomeBase') == FALSE){
            // mengembalikan nilai input yang sudah dimasukan sebelumnya
            session()->setFlashdata('inputs', $this->request->getPost());
            // memberikan pesan error pada saat input data
            session()->setFlashdata('errors', $this->form_validation->getErrors());
            // kembali ke halaman form
            return redirect()->to(base_url('dashboard/refferensi/base'));
        } else {
			// check data
			$dboBase = new BaseModel();
			$base = $dboBase->where('kode', $kode)
								->first();

			if (!empty($base)) {
				session()->setFlashdata('pesan_error', "Data Home Base dengan Kode <strong>$kode</strong>, tidak dapat disimpan karena terdapat Kode yang sama.");
				return redirect()->to(base_url('dashboard/refferensi/base'));
			}

			try {
				$dboHomebase = new BaseModel();
				$newBase = new HomeBase();
				$newBase->kode = $kode;
				$newBase->home_base = $home_base;
				$newBase->alamat = $alamat;
				$dboHomebase->save($newBase);
				session()->setFlashdata('pesan', "Home Base dengan kode <strong>$kode</strong> berhasil disimpan");
			} catch (\Throwable $th) {
				session()->setFlashdata('inputs', $this->request->getPost());
				session()->setFlashdata('pesan_error', $th->getMessage());
			}
			
            return redirect()->to(base_url('dashboard/refferensi/base'));
        }
	}

	public function editHomeBase() {
		$kode = $this->request->getPost('kode');
		$home_base = $this->request->getPost('home_base');
		$alamat = $this->request->getPost('alamat');
		
		$data = [
			'kode'  => $kode,
			'home_base' => $home_base,
			'alamat'  => $alamat,
		];
		
		$this->validate([
            'kode'  => 'required',
			'home_base' => 'required',
			'alamat'  => 'required',
		]);
		
		// check data
		$dboBase = new BaseModel();
		$base = $dboBase->where('kode', $kode)
							->first();

		if($this->form_validation->run($data, 'addHomeBase') == FALSE){
            // mengembalikan nilai input yang sudah dimasukan sebelumnya
            session()->setFlashdata('inputs', $this->request->getPost());
            // memberikan pesan error pada saat input data
            session()->setFlashdata('errors', $this->form_validation->getErrors());
            // kembali ke halaman form
            return redirect()->to(base_url('dashboard/editHomeBase/'.$base->id));
        } else {

			if (empty($base)) {
				session()->setFlashdata('pesan_error', "Data Base dengan Nomor <strong>$kode</strong>, Tidak ditemukan!");
				return redirect()->to(base_url('dashboard/editHomeBase/'.$base->id));
			}

			try {
				$base->home_base = $home_base;
				$base->alamat = $alamat;
				$dboBase->save($base);
				session()->setFlashdata('pesan', "Regional dengan kode <strong>$kode</strong> berhasil disimpan");
			} catch (\Throwable $th) {
				session()->setFlashdata('inputs', $this->request->getPost());
				session()->setFlashdata('pesan_error', $th->getMessage());
				return redirect()->to(base_url('dashboard/editHomeBase/'.$base->id));
			}
			
			return redirect()->to(base_url('dashboard/refferensi/base'));
		}
	}

	public function deleteHomeBase($id) {
		$dboBase = new BaseModel();
		$data = $dboBase->find($id);
		$data->deleted_at = date('Y-m-d H:i:s');
		$dboBase->save($data);
		session()->setFlashdata('pesan', "Data Home Base <strong>{$data->kode}</strong> berhasil dihapus");
		return redirect()->to(base_url('dashboard/refferensi/base'));
	}

	public function addNodes() {
		$name = $this->request->getPost('name');
		$description = $this->request->getPost('description');
		
		$data = [
			'name'  => $name,
			'description' => $description,
		];
		
		$this->validate([
            'name'  => 'required',
			'description' => 'required',
		]);
		
		if($this->form_validation->run($data, 'addNodes') == FALSE){
            // mengembalikan nilai input yang sudah dimasukan sebelumnya
            session()->setFlashdata('inputs', $this->request->getPost());
            // memberikan pesan error pada saat input data
            session()->setFlashdata('errors', $this->form_validation->getErrors());
            // kembali ke halaman form
            return redirect()->to(base_url('dashboard/refferensi/nodes'));
        } else {
			try {
				$dboNodes = new NodesModel();
				$newNodes = new Nodes();
				$newNodes->name = $name;
				$newNodes->description = $description;
				$dboNodes->save($newNodes);
				session()->setFlashdata('pesan', "Nodes <strong>$name</strong> berhasil disimpan");
			} catch (\Throwable $th) {
				session()->setFlashdata('inputs', $this->request->getPost());
				session()->setFlashdata('pesan_error', $th->getMessage());
			}
			
            return redirect()->to(base_url('dashboard/refferensi/nodes'));
		}
	}

	public function updateNodes() {
		$id = $this->request->getPost('id');
		$name = $this->request->getPost('name');
		$description = $this->request->getPost('description');
		
		$data = [
			'name'  => $name,
			'description' => $description,
		];
		
		$this->validate([
            'name'  => 'required',
			'description' => 'required',
		]);
		
		// check data
		$dboNodes = new NodesModel();
		$nodes = $dboNodes->where('id', $id)
							->first();
		
		if($this->form_validation->run($data, 'addNodes') == FALSE){
            // mengembalikan nilai input yang sudah dimasukan sebelumnya
            session()->setFlashdata('inputs', $this->request->getPost());
            // memberikan pesan error pada saat input data
            session()->setFlashdata('errors', $this->form_validation->getErrors());
            // kembali ke halaman form
            return redirect()->to(base_url('dashboard/editNodes/'.$nodes->id));
        } else {

			if (empty($nodes)) {
				session()->setFlashdata('pesan_error', "Data Base dengan Nomor <strong>$name</strong>, Tidak ditemukan!");
				return redirect()->to(base_url('dashboard/editNodes/'.$nodes->id));
			}

			try {
				$nodes->name = $name;
				$nodes->description = $description;
				
				$dboNodes->save($nodes);
				session()->setFlashdata('pesan', "Nodes dengan kode <strong>$name</strong> berhasil disimpan");
			} catch (\Throwable $th) {
				session()->setFlashdata('inputs', $this->request->getPost());
				session()->setFlashdata('pesan_error', $th->getMessage());
				return redirect()->to(base_url('dashboard/editNodes/'.$nodes->id));
			}
			
			return redirect()->to(base_url('dashboard/refferensi/nodes'));
		}
	}

	public function deleteNodes($id) {
		$dboNodes = new NodesModel();
		$data = $dboNodes->find($id);
		$data->deleted_at = date('Y-m-d H:i:s');
		$dboNodes->save($data);
		session()->setFlashdata('pesan', "Data Home Base <strong>{$data->kode}</strong> berhasil dihapus");
		return redirect()->to(base_url('dashboard/refferensi/nodes'));
	}

	public function detailTrayek($id, $start, $end) {
		$dboR7 = new R7Model();
		$dboTrayek = new TrayekModel();
		$trayek = $dboTrayek->select('kode_base, kode_base_akhir, kode_regional, count(kode_base) as total')
							->where('kode_regional', $id)
							->groupBy('kode_base, kode_base_akhir, kode_regional')
							->get();
		$dataTrayek = [];
		foreach ($trayek->getResult() as $item) {
			// List Data Kode Trayek
			$listDataTrayek = $dboTrayek->where('kode_base', $item->kode_base)
										->where('kode_base_akhir', $item->kode_base_akhir)
										->where('deleted_at is NULL', NULL)
										->orderBy('urutan', 'asc')
										->get();
			$totalBiaya = 0;
			$totalTransaksi = 0;
			$totalTrayek = 0;
			$totalBerat = 0;
			foreach($listDataTrayek->getResult() as $item) {
				$dataR7 = $dboR7->where('kode_trayek', $item->kode)
								->where('created_at >=', $start)
								->where('created_at <=', $end)
								->get();
				if (count($dataR7->getResult()) > 0) {
					foreach($dataR7->getResult() as $r7Item) {
						$biaya = (int)$r7Item->berat * (int)$item->harga_perkg;
						$totalTransaksi += $r7Item->jumlah;
						$totalBiaya += $biaya;
						$totalBerat += $r7Item->berat;
					}
				}
				$totalTrayek++;
			}
			$dboBase = new BaseModel();
			$base_awal = $dboBase->where('kode', $item->kode_base)->first()->home_base;
			$base_akhir = $dboBase->where('kode', $item->kode_base_akhir)->first()->home_base;
			$item->home_base_awal = $base_awal;
			$item->home_base_akhir = $base_akhir;
			$item->total_biaya = $totalBiaya;
			$item->total_transaksi = $totalTransaksi;
			$item->total_trayek = $totalTrayek;
			$item->total_berat = $totalBerat;
			$dataTrayek[] = $item;
		}
		
		$trayek = $dataTrayek;
		$dboRegional = new RegionalModel();
		$regional = $dboRegional->where('kode', $id)->first();
		$tanggal = [
			'startDate' => $start,
			'endDate' => $end
		];
		return view('admin/data_trayek', compact('trayek', 'regional', 'tanggal'));
	}

	public function detailKendaraanTrayek($trayek) {
		$dataTrayek = explode("::", $trayek);
		$awal = $dataTrayek[0];
		$akhir = $dataTrayek[1];
		$kodeRegional = $dataTrayek[2];
		$startDate = $dataTrayek[3];
		$endDate = $dataTrayek[4];

		$dboTrayek = new TrayekModel();
		$mobil = $dboTrayek->select('kode_mobil')->where('trayek_awal', $awal)
							->where('trayek_akhir', $akhir)
							->where('created_at >=', $startDate)
							->where('created_at <=', $endDate)
							->groupBy('kode_mobil')
							->get();
		$mobil = $mobil->getResult();
		
		$dboRegional = new RegionalModel();
		$regional = $dboRegional->where('kode', $kodeRegional)->first();
		$namaRegional = $regional->nama;

		foreach($mobil as $item) {
			$dboMobil = new MobilModel();
			$dataMobil = $dboMobil->where('id', $item->kode_mobil)->first();

			// hitung transaksi untuk mobil
			$dataTrayekMobil = $dboTrayek->where('trayek_awal', $awal)
										->where('trayek_akhir', $akhir)
										->where('created_at >=', $startDate)
										->where('created_at <=', $endDate)
										->get();
			$totalBerat = 0;
			$totalTransaksi = 0;
			$totalBiaya = 0;

			$dboR7 = new R7Model();
			foreach($dataTrayekMobil->getResult() as $itemTrayekMobil) {
				$dataR7 = $dboR7->where('kode_trayek', $itemTrayekMobil->kode)
								->where('created_at >=', $startDate)
								->where('created_at <=', $endDate)
								->get();
				foreach($dataR7->getResult() as $r7Data) {
					$totalBerat += $r7Data->berat;
					$totalTransaksi += $r7Data->jumlah;
					$biaya = $totalBerat * $itemTrayekMobil->harga_perkg;
					$totalBiaya += $biaya;
				}
			}
			$item->nopol = $dataMobil->kode;
			$item->type = $dataMobil->type;
			$item->tahun = $dataMobil->tahun;
			$item->total_berat = $totalBerat;
			$item->total_transaksi = $totalTransaksi;
			$item->total_biaya = $totalBiaya;
		}

		$dataTrayek = "$awal - $akhir";
		$trayek = [
			'awal' => $awal,
			'akhir' => $akhir
		];

		$tanggal = [
			'awal'  => $startDate,
			'akhir' => $endDate
		];

		return view('admin/data_mobil', compact('mobil', 'namaRegional', 'trayek', 'tanggal'));

	}

	public function detailKendaraanTrayekList($trayek) {
		$dataTrayek = explode("::", $trayek);
		$awal = $dataTrayek[0];
		$akhir = $dataTrayek[1];
		$kodeRegional = $dataTrayek[2];
		$startDate = $dataTrayek[3];
		$endDate = $dataTrayek[4];
		$nopol = $dataTrayek[5];

		$trayekLink = [
			'awal' => $awal,
			'akhir' => $akhir
		];

		$tanggal = [
			'awal'  => $startDate,
			'akhir' => $endDate
		];

		$dboMobil = new MobilModel();
		$dataMobil = $dboMobil->where('kode', $nopol)->first();

		$dboTrayek = new TrayekModel();
		$trayek = $dboTrayek->where('kode_mobil', $dataMobil->id)
							->where('created_at >=', $startDate)
							->where('created_at <=', $endDate)
							->where('deleted_at IS NULL', NULL)
							->get();
		
		$dataTrayek = [];
		$dboR7 = new R7Model();
		$totalBerat = 0;
		$totalTransaksi = 0;
		$totalBiaya = 0;
		foreach($trayek->getResult() as $item) {
			// Ambil R7
			$dataR7 =  $dboR7->where('kode_trayek', $item->kode)
							->where('created_at >=', $startDate)
							->where('created_at <=', $endDate)
							->get();
			$item->total_berat = 0;
			$item->total_transaksi = 0;
			$item->total_biaya = 0;
			foreach($dataR7->getResult() as $r7Data) {
				$totalBerat += $r7Data->berat;
				$totalTransaksi += $r7Data->jumlah;
				$biaya = $totalBerat * $item->harga_perkg;
				$totalBiaya += $biaya;

				$item->total_berat = $totalBerat;
				$item->total_transaksi = $totalTransaksi;
				$item->total_biaya = $totalBiaya;
			}
			$dataTrayek[] = $item;
		}

		return view('admin/data_mobil_trayek', compact('dataMobil', 'dataTrayek', 'trayekLink', 'tanggal'));

	}

	public function detailTrayekList($trayek) {
		$dataTrayek = explode("::", $trayek);
		$awal = $dataTrayek[0];
		$akhir = $dataTrayek[1];
		$kodeRegional = $dataTrayek[2];
		$startDate = $dataTrayek[3];
		$endDate = $dataTrayek[4];


		$dboRegional = new RegionalModel();
		$regional = $dboRegional->where('kode', $kodeRegional)->first();
		$tanggal = [
			'startDate' => $startDate,
			'endDate' => $endDate
		];

		$dboHomebase = new BaseModel();

		$trayek = [
			'asal' => $dboHomebase->where('kode', $awal)->first()->home_base,
			'akhir' => $dboHomebase->where('kode', $akhir)->first()->home_base
		];

		$rute = [];
		$dboTrayek = new TrayekModel();
		$lisRute = $dboTrayek->where('kode_regional', $kodeRegional)
								->where('kode_base', $awal)
								->where('kode_base_akhir', $akhir)
								->where('deleted_at is NULL', null)
								->orderBy('urutan', 'asc')
								->get();
		
		$dataRute = [];
		$dboR7 = new R7Model();
		$dboMobil = new MobilModel();
		foreach($lisRute->getResult() as $item) {
			$totalTransaksi = 0;
			$totalBiaya = 0;
			$totalBerat = 0;
			$dataR7 = $dboR7->where('kode_trayek', $item->kode)
						->where('created_at >=', $startDate)
						->where('created_at <=', $endDate)
						->get();
			if (count($dataR7->getResult()) > 0) {
				foreach ($dataR7->getResult() as $itemR7) {
					$totalTransaksi += $itemR7->jumlah;
					$biaya = $item->harga_perkg * $itemR7->berat;
					$totalBiaya += $biaya;
					$totalBerat += $itemR7->berat;
				}
			}
			$mobil = $dboMobil->where('id', $item->kode_mobil)->first(); 
			$item->total_transaksi = $totalTransaksi;
			$item->total_berat = $totalBerat;
			$item->total_biaya = $totalBiaya;
			$item->harga = $item->harga_perkg;
			$item->kbm = $mobil->kode;
			$item->type = $mobil->type;
			$dataRute[] =  $item;
		}

		return view('admin/data_trayek_list', compact('tanggal', 'trayek', 'dataRute', 'regional'));

	}

	public function detailTrayekListAngkutan($trayek) {
		$dataTrayek = explode("::", $trayek);
		$awal = $dataTrayek[0];
		$akhir = $dataTrayek[1];
		$kodeRegional = $dataTrayek[2];
		$startDate = $dataTrayek[3];
		$endDate = $dataTrayek[4];


		$dboRegional = new RegionalModel();
		$regional = $dboRegional->where('kode', $kodeRegional)->first();
		$tanggal = [
			'startDate' => $startDate,
			'endDate' => $endDate
		];

		$dboHomebase = new BaseModel();

		$trayek = [
			'asal' => $dboHomebase->where('kode', $awal)->first()->home_base,
			'akhir' => $dboHomebase->where('kode', $akhir)->first()->home_base,
			'kode_regional' => $kodeRegional
		];

		$rute = [];
		$dboTrayek = new TrayekModel();
		$lisRute = $dboTrayek->select('kode_mobil')->where('kode_regional', $kodeRegional)
								->where('kode_base', $awal)
								->where('kode_base_akhir', $akhir)
								->where('deleted_at is NULL', null)
								->groupBy('kode_mobil')
								->get();
		
		$dataRute = [];
		$dboR7 = new R7Model();
		$dboMobil = new MobilModel();
		foreach($lisRute->getResult() as $item) {
			$totalTransaksi = 0;
			$totalBerat = 0;
			$kodeTrayek = $dboTrayek->where('kode_mobil', $item->kode_mobil)->get();
			$listKodeTrayek = [];
			foreach($kodeTrayek->getResult() as $itemTrayek) {
				$listKodeTrayek[] = $itemTrayek->kode;
			}
			$dataR7 = $dboR7->whereIn('kode_trayek', $listKodeTrayek)
						->where('created_at >=', $startDate)
						->where('created_at <=', $endDate)
						->get();
			if (count($dataR7->getResult()) > 0) {
				foreach ($dataR7->getResult() as $itemR7) {
					$totalTransaksi += $itemR7->jumlah;
					$totalBerat += $itemR7->berat;
				}
			}
			$mobil = $dboMobil->where('id', $item->kode_mobil)->first(); 
			$item->total_transaksi = $totalTransaksi;
			$item->total_berat = $totalBerat;
			$item->kbm = $mobil->kode;
			$item->type = $mobil->type;
			$dataRute[] =  $item;
		}

		return view('admin/data_trayek_list_angkutan', compact('tanggal', 'trayek', 'dataRute', 'regional'));

	}

	public function detailTransaksiKendaraan($nopol, $awal, $akhir, $namaRegional, $ruteAwal, $ruteAkhir) {
		$dboMobil = new MobilModel();
		$mobil = $dboMobil->where('kode', $nopol)->first();

		$tanggal = [
			'startDate' => $awal,
			'endDate'   => $akhir
		];

		$rute = [
			'awal' => $ruteAwal,
			'akhir' => $ruteAkhir
		];

		$dboTrayek = new TrayekModel();
		$trayek = $dboTrayek->where('kode_mobil', $mobil->id)
							->where('created_at >=', $awal)
							->where('created_at <=', $akhir)
							->where('trayek_awal', $ruteAwal)
							->where('trayek_awal', $ruteAkhir)
							->get();
		
		$kodeTrayek = [];
		foreach($trayek->getResult() as $item) {
			$kodeTrayek[] = $item->kode;
		}
		$dboR7 = new R7Model();
		$r7 = $dboR7->whereIn('kode_trayek', $kodeTrayek)->get();

		$dataR7 = [];
		foreach ($r7->getResult() as $itemR7) {
			$dboUser = new UserModel();
			$user = $dboUser->where('username', $itemR7->user_id)->first();
			$itemR7->nama_sopir = $user->name;
			$itemR7->sopir_id = $user->id;

			$dboTrayek = new TrayekModel();
			$dataTrayek = $dboTrayek->where('kode', $itemR7->kode_trayek)->first();
			$itemR7->harga_perkg = $dataTrayek->harga_perkg;
			$itemR7->total = $itemR7->berat * $dataTrayek->harga_perkg;
			$dataR7[] = $itemR7;
		}

		// $dataTrayek = [];
		// foreach($trayek->getResult() as $item) {
		// 	$dataTrayek[] = $item->kode;
		// }

		// $dboR7 = new R7Model();
		// $r7 = $dboR7->whereIn('kode_trayek', $dataTrayek)->get();

		// foreach ($r7->getResult() as $itemR7) {
		// 	$dboUser = new UserModel();
		// 	$user = $dboUser->where('username', $itemR7->user_id)->first();
		// 	$itemR7->nama_sopir = $user->name;
		// 	$itemR7->sopir_id = $user->id;

		// 	$dboTrayek = new TrayekModel();
		// 	$dataTrayek = $dboTrayek->where('kode', $itemR7->kode_trayek)->first();
		// 	$itemR7->harga_perkg = $dataTrayek->harga_perkg;
		// 	$itemR7->total = $itemR7->berat * $dataTrayek->harga_perkg;
		// }

		return view('admin/data_r7_permobil', compact('mobil', 'trayek', 'dataR7', 'namaRegional', 'tanggal', 'rute'));
		// return view('admin/data_r7_mobil', compact('mobil', 'trayek', 'r7'));
	}

	public function detailR7Driver($supir_id) {
		$dboUser = new UserModel();
		$user = $dboUser->where('id', $supir_id)->first();

		$dboR7 = new R7Model();
		$r7 = $dboR7->where('user_id', $user->username)->get();
		
		foreach ($r7->getResult() as $itemR7) {
			$dboTrayek = new TrayekModel();
			$dataTrayek = $dboTrayek->where('kode', $itemR7->kode_trayek)->first();
			$itemR7->trayek_awal = $dataTrayek->trayek_awal;
			$itemR7->trayek_akhir= $dataTrayek->trayek_akhir;
			$itemR7->harga_perkg = $dataTrayek->harga_perkg;
			$itemR7->total = $itemR7->berat * $dataTrayek->harga_perkg;

			$dboMobil = new MobilModel();
			$mobil = $dboMobil->where('id', $dataTrayek->kode_mobil)->first();
			$itemR7->nopol = $mobil->kode;
			$itemR7->type_mobil = $mobil->type;
			$itemR7->tahun = $mobil->tahun;
		}
		return view('admin/data_r7_supir', compact('user', 'r7'));
	}

	public function getR7mobile() {
		$input = $this->request->getJSON();
		$mulai = $input->mulai;
		$akhir = $input->akhir;
		$nopol = $input->nopol;

		$dboMobil = new MobilModel();
		$mobil = $dboMobil->where('kode', $nopol)->first();

		$dboTrayek = new TrayekModel();
		$trayek = $dboTrayek->where('kode_mobil', $mobil->id)->get();
		$dataTrayek = [];
		foreach($trayek->getResult() as $item) {
			$dataTrayek[] = $item->kode;
		}

		$dboR7 = new R7Model();
		$dataR7 = $dboR7->whereIn('kode_trayek', $dataTrayek)
						->where('DATE_FORMAT(`r7`.`created_at`, "%Y-%m-%d") >=', date('Y-m-d',strtotime($mulai)))
						->where('DATE_FORMAT(`r7`.`created_at`, "%Y-%m-%d") <=', date('Y-m-d',strtotime($akhir)))
						->get();

		$dataR7 = $dataR7->getResult();
		if (count($dataR7) == 0) {
			return $this->respond([], 404);
		}

		foreach($dataR7 as $items) {
			$dboTrayek = new TrayekModel();
			$trayek = $dboTrayek->where('kode', $items->kode_trayek)->first();
			$items->trayek_awal = $trayek->trayek_awal;
			$items->trayek_akhir = $trayek->trayek_akhir;
			$items->harga_perkg = $trayek->harga_perkg;
			$items->harga_perkm = $trayek->harga_perkm;

			$dboUser = new UserModel();
			$user = $dboUser->where('id', $trayek->kode_supir)->first();
			$items->nama_supir = $user->name;

			$dboMobil = new MobilModel();
			$mobil = $dboMobil->where('id', $trayek->kode_mobil)->first();
			$items->nopol = $mobil->kode;
		}
		return $this->respond($dataR7, 200);
	}

	public function updateUrutan() {
		$input = $this->request->getJSON();
		$idTrayek = $input->idTrayek;
		$urutanTrayek = $input->urutanTrayek;

		$response = [
			'id_trayek' => $idTrayek,
			'urutan_trayek' => $urutanTrayek
		];

		$dboTrayek = new TrayekModel();
		$dataTrayek = $dboTrayek->where('id', $idTrayek)
							->first();
		if (empty($dataTrayek)) {
			return $this->respond([
				'message' => 'data not found'
			], 500);
		}

		$dataTrayek->urutan = $urutanTrayek;
		$dboTrayek->save($dataTrayek);

		return $this->respond($dataTrayek, 200);

	}

	/**
	 * Datatable Regional
	 */
	public function dtRegional() {
		$startDate = $this->request->getPost('startDate');
		$endDate = $this->request->getPost('endDate');

		$dboRegional = new RegionalModel();
		$regional = $dboRegional->findAll();
		$dataRegional = [];
		$i = 1;
		foreach($regional as $item) {
			$dboTrayek = new TrayekModel();
			$trayek = $dboTrayek->select('kode')->where('kode_regional', $item->kode)->get();

			if (count($trayek->getResult()) > 0) {
				$dboR7 = new R7Model();
				$dataTraye = [];
				foreach($trayek->getResult() as $itemTrayek) {
					$dataTraye[] = $itemTrayek ->kode;
				}

				$r7 = $dboR7->whereIn('kode_trayek', $dataTraye)
							->where('created_at >=', $startDate)
							->where('created_at <=', $endDate)
							->get();
	
				$totalTransaksi = 0;
				$totalBerat = 0;
				$totalBiaya = 0;
				foreach($r7->getResult() as $itemR7) {
					$totalTransaksi++;
					$totalBerat += $itemR7->berat;
	
					$harga = $dboTrayek->where('kode', $itemR7->kode_trayek)->first();
	
					$totalBiaya += $totalBerat * $harga->harga_perkg;
				}
	
				$item->total_r7 = $totalTransaksi;
				$item->total_berat = $totalBerat;
				$item->total_harga = $totalBiaya;
				$button = '';
				if ($totalTransaksi > 0) {
					$button = '<a href="./detailTrayek/'.$item->kode.'/'.$startDate.'/'.$endDate.'" class="btn btn-info btn-block">Lihat Detail Trayek</a>';
				}
				$dataRegional[] = [
					$i,
					$item->nama,
					$item->alamat,
					number_format($totalTransaksi,0,',','.'),
					number_format($totalBerat,0,',','.'),
					number_format($totalBiaya,0,',','.'),
					$button
				];
			} else {
				$dataRegional[] = [
					$i,
					$item->nama,
					$item->alamat,
					0,
					0,
					0,
					''
				];
			}
			$i++;
		}


		$response = [
			'data' => $dataRegional
		];
		return $this->respond($response, 200, 'success');

	}
	//--------------------------------------------------------------------

}
