<?php

namespace Config;

class Validation
{
	//--------------------------------------------------------------------
	// Setup
	//--------------------------------------------------------------------

	/**
	 * Stores the classes that contain the
	 * rules that are available.
	 *
	 * @var array
	 */
	public $ruleSets = [
		\CodeIgniter\Validation\Rules::class,
		\CodeIgniter\Validation\FormatRules::class,
		\CodeIgniter\Validation\FileRules::class,
		\CodeIgniter\Validation\CreditCardRules::class,
	];

	/**
	 * Specifies the views that are used to display the
	 * errors.
	 *
	 * @var array
	 */
	public $templates = [
		'list'   => 'CodeIgniter\Validation\Views\list',
		'single' => 'CodeIgniter\Validation\Views\single',
	];

	//--------------------------------------------------------------------
	// Rules
	//--------------------------------------------------------------------
	// Login
	public $login = [
		'username' => 'required|alpha_numeric|min_length[5]|max_length[50]',
		'password' => 'required|min_length[8]|max_length[50]'
	];

	// Lacak
	public $lacak = [
		'nomor_r7' => 'required',
	];

	// Add R7
	public $addR7 = [
		'nomor'  => 'required',
		'jumlah' => 'required',
		'berat'  => 'required',
		'trayek' => 'required',
	];

	// Add User
	public $addUser = [
		'username'  => 'required',
		'password' => 'required',
		'name'  => 'required',
		'level' => 'required',
	];

	// Add User
	public $updateUser = [
		'username'  => 'required',
		'name'  => 'required',
		'level' => 'required',
	];

	// Add Mobil
	public $addMobil = [
		'kode'  => 'required',
		'type'  => 'required',
		'tahun' => 'required',
	];

	// Add Regional
	public $addRegional = [
		'kode'  => 'required',
		'nama'  => 'required',
		'alamat' => 'required',
	];

	// Add Base
	public $addHomeBase = [
		'kode'  => 'required',
		'home_base'  => 'required',
		'alamat' => 'required',
	];

	// Add Base
	public $addNodes = [
		'name'  => 'required',
		'description'  => 'required',
	];

	// Add User
	public $addTrayek = [
		// 'supir'  => 'required',
		'keberangkatan'  => 'required',
		'trayekAwal' => 'required',
		'trayekAkhir' => 'required',
		'kodebase'  => 'required',
		'kodebaseakhir'  => 'required',
		'koderegional' => 'required',
		'kodemobil' => 'required',
		// 'jumlahkbm' => 'required',
		// 'plpi' => 'required',
		// 'hargaperkm' => 'required',
		'hargaperkg' => 'required',
		// 'akhirpks' => 'required',
		// 'kappks' => 'required',
		// 'kapreal' => 'required',
	];

	// Add User
	public $updateTrayek = [
		// 'supir' => 'required',
		'keberangkatan'  => 'required',
		'trayekAwal' => 'required',
		'trayekAkhir' => 'required',
		'kodebase'  => 'required',
		'kodebaseakhir' => 'required',
		'koderegional' => 'required',
		'kodemobil' => 'required',
		// 'jumlahkbm' => 'required',
		// 'plpi' => 'required',
		// 'hargaperkm' => 'required',
		'hargaperkg' => 'required',
		// 'akhirpks' => 'required',
		// 'kappks' => 'required',
		// 'kapreal' => 'required',
	];

	public $error = [
		'username' => [
			'required'      => 'Username wajib diisi',
			'alpha_numeric' => 'Username hanya boleh diisi dengan huruf dan angka',
			'min_length'    => 'Username minimal terdiri dari 5 karakter',
			'max_length'    => 'Username maksimal terdiri dari 50 karakter'
		],
		'password' => [
			'required'      => 'Password wajib diisi',
			'min_length'    => 'Password minimal terdiri dari 8 karakter',
			'max_length'    => 'Password maksimal terdiri dari 50 karakter'
		],
		'name' => [
			'required'		=> 'Nama Harus Diisi'
		],
		'description' => [
			'required'		=> 'Deskripsi Harus Diisi'
		],
		'level' => [
			'required'		=> 'Level harus diisi'
		],
		'nomor_r7' => [
			'required'		=> 'Nomo R7 harus diisi'
		],
		'nomor' => [
			'required'		=> 'Nomo R7 harus diisi'
		],
		'jumlah' => [
			'required'		=> 'Jumlah kantong harus diisi'
		],
		'berat' => [
			'required'		=> 'Total berat harus diisi'
		],
		'trayek_awal' => [
			'required'		=> 'Trayek harus diisi'
		],
		'trayek_akhir' => [
			'required'		=> 'Trayek harus diisi'
		],
		'kode'  => [
			'required' => 'Kode harus diisi'
		],
		'kodebase'  => [
			'required' => 'Base Awal base harus diisi'
		],
		'kodebaseakhir'  => [
			'required' => 'Base Akhir base harus diisi'
		],
		'koderegional' => [
			'required' => 'Regional Harus Diisi'
		],
		'kodemobil' => [
			'required' => 'Mobil Harus diisi'
		],
		'jumlahkbm' => [
			'required' => 'Jumlah KBM harus diisi'
		],
		'plpi' => [
			'required' => 'PLPI harus diisi'
		],
		'hargaperkm' => [
			'required' => 'Harga per KM harus diisi'
		],
		'hargaperkg' => [
			'required' => 'Harga per KG harus diisi'
		],
		'akhirpks' => [
			'required' => 'Tanggal akhir PKS harus diisi'
		],
		'kappks' => [
			'required' => 'Kap PKS Harus diisi'
		],
		'kapreal' => [
			'required' => 'Kap Real Harus diisi'
		],
		'alamat' => [
			'required' => 'Alamat Harus diisi'
		],
		'nama' => [
			'required' => 'Nama harus diisi'
		],
		'type' => [
			'required' => 'Type harus diisi'
		],
		'home_base' => [
			'required' => 'Home Base harus diisi'
		],
		'keberangkatan' => [
			'required' => 'Jenis Keberangkatan harus diisi'
		]
	];
}
