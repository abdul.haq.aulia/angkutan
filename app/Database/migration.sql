/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 10.4.11-MariaDB : Database - r7
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`epiz_26628382_ertujuh` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `epiz_26628382_ertujuh`;

/*Table structure for table `home_base` */

DROP TABLE IF EXISTS `home_base`;

CREATE TABLE `home_base` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(100) NOT NULL,
  `home_base` varchar(100) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `home_base` */

insert  into `home_base`(`id`,`kode`,`home_base`,`alamat`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'CLG','CILEGON','','2020-08-30 17:37:45','2020-08-30 17:37:45',NULL),
(2,'SM','SEMARANG','','2020-08-30 17:37:45','2020-08-30 17:37:45',NULL),
(3,'YK','YOGYAKARTA','','2020-08-30 17:37:45','2020-08-30 17:37:45',NULL),
(4,'JMB','JAMBI','','2020-08-30 17:37:45','2020-08-30 17:37:45',NULL),
(5,'MUA','MUARAENIM','','2020-08-30 17:37:45','2020-08-30 17:37:45',NULL),
(6,'SYB','SURABAYA','','2020-08-30 17:37:45','2020-08-30 17:37:45',NULL),
(7,'BD','BANDUNG','','2020-08-30 17:37:45','2020-08-30 17:37:45',NULL),
(8,'JKT','JAKARTA','','2020-08-30 17:37:45','2020-08-30 17:37:45',NULL),
(9,'MRK','Merauke Indonesia','Jl. Brawijaya NO. 168','2020-08-31 03:02:55','2020-08-31 03:06:35','2020-08-31 03:06:35');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `version` varchar(255) NOT NULL,
  `class` text NOT NULL,
  `group` varchar(255) NOT NULL,
  `namespace` varchar(255) NOT NULL,
  `time` int(11) NOT NULL,
  `batch` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`version`,`class`,`group`,`namespace`,`time`,`batch`) values 
(1,'2020-08-27-045313','App\\Database\\Migrations\\Users','default','App',1598507722,1),
(2,'2020-08-27-045436','App\\Database\\Migrations\\R7','default','App',1598507843,2),
(4,'2020-08-29-204558','App\\Database\\Migrations\\HomeBase','default','App',1598735416,3),
(5,'2020-08-29-204619','App\\Database\\Migrations\\Mobil','default','App',1598735416,3),
(6,'2020-08-29-204623','App\\Database\\Migrations\\Regional','default','App',1598735416,3),
(7,'2020-08-29-205435','App\\Database\\Migrations\\UpdateTrayek','default','App',1598735416,3),
(8,'2020-08-29-205927','App\\Database\\Migrations\\Trayek','default','App',1598735416,3);

/*Table structure for table `mobil` */

DROP TABLE IF EXISTS `mobil`;

CREATE TABLE `mobil` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `tahun` varchar(5) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `mobil` */

insert  into `mobil`(`id`,`kode`,`type`,`tahun`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'D-8579-EM\r\n','Daihatsu Granmax BV','2014','2020-08-30 17:38:10','2020-08-30 17:38:10',NULL),
(2,'D-8023-EJ\r\n','Mits. FE 304 (PS 100-4 Ban)\r\n','2014\r','2020-08-30 17:38:10','2020-08-30 17:38:10',NULL),
(3,'D-8240-EM\r\n','FE  74 PS 125\r\n','2014\r','2020-08-30 17:38:10','2020-08-30 17:38:10',NULL),
(4,'D-8238-EM\r\n','FE  74 PS 125 (TMT 19 apr 2018 )\r\n','2014\r','2020-08-30 17:38:10','2020-08-30 17:38:10',NULL),
(5,'D-8458-EN\r\n','FE 71 ( TMT 9 APR 2018 )','2014','2020-08-30 17:38:10','2020-08-30 17:38:10',NULL),
(6,'D 8554 EN\r\n','FE 71 ( TMT 9 JAN 2019 )\r\n','2013\r','2020-08-30 17:38:10','2020-08-30 17:38:10',NULL),
(7,'asdfasdfa','Ubah','2021','2020-08-31 02:54:03','2020-08-31 02:56:56','2020-08-31 02:56:56');

/*Table structure for table `r7` */

DROP TABLE IF EXISTS `r7`;

CREATE TABLE `r7` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `kode_trayek` varchar(100) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `nomor` varchar(100) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `berat` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `r7` */

insert  into `r7`(`id`,`kode_trayek`,`user_id`,`nomor`,`jumlah`,`berat`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'TRY-01','99087678','112233',17,120,'2020-08-30 04:33:00','2020-08-30 04:33:00',NULL),
(2,'TRY-02','99087678','123asdfads',10,100,'2020-08-31 03:40:29','2020-08-31 03:40:29',NULL);

/*Table structure for table `regional` */

DROP TABLE IF EXISTS `regional`;

CREATE TABLE `regional` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/*Data for the table `regional` */

insert  into `regional`(`id`,`kode`,`nama`,`alamat`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'I','REGIONAL I','','2020-08-30 17:38:36','2020-08-30 17:38:36',NULL),
(2,'II','REGIONAL II','','2020-08-30 17:38:36','2020-08-30 17:38:36',NULL),
(3,'III','REGIONAL III','','2020-08-30 17:38:36','2020-08-30 17:38:36',NULL),
(4,'IV','REGIONAL IV','','2020-08-30 17:38:36','2020-08-30 17:38:36',NULL),
(5,'V','REGIONAL V','','2020-08-30 17:38:36','2020-08-30 17:38:36',NULL),
(6,'VI','REGIONAL VI','','2020-08-30 17:38:36','2020-08-30 17:38:36',NULL),
(7,'VII','REGIONAL VII','','2020-08-30 17:38:36','2020-08-30 17:38:36',NULL),
(8,'VIII','REGIONAL VIII','','2020-08-30 17:38:36','2020-08-30 17:38:36',NULL),
(9,'IX','REGIONAL IX','','2020-08-30 17:38:36','2020-08-30 17:38:36',NULL),
(10,'X','REGIONAL X','','2020-08-30 17:38:36','2020-08-30 17:38:36',NULL),
(11,'XI','REGIONAL XI','','2020-08-30 17:38:36','2020-08-30 17:38:36',NULL),
(12,'XIII','Regional 13','Indonesia 13','2020-08-31 03:07:34','2020-08-31 03:08:35','2020-08-31 03:08:35');

/*Table structure for table `trayek` */

DROP TABLE IF EXISTS `trayek`;

CREATE TABLE `trayek` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(25) NOT NULL,
  `kode_base` varchar(5) NOT NULL,
  `kode_regional` varchar(10) DEFAULT NULL,
  `kode_mobil` varchar(10) NOT NULL,
  `trayek` varchar(250) NOT NULL,
  `plpi` varchar(250) NOT NULL,
  `harga_perkm` varchar(250) NOT NULL,
  `jumlah_kbm` varchar(250) NOT NULL,
  `akhir_pks` date NOT NULL,
  `kap_pks` varchar(250) NOT NULL,
  `kap_real` varchar(250) NOT NULL,
  `keterangan` varchar(250) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `trayek` */

insert  into `trayek`(`id`,`kode`,`kode_base`,`kode_regional`,`kode_mobil`,`trayek`,`plpi`,`harga_perkm`,`jumlah_kbm`,`akhir_pks`,`kap_pks`,`kap_real`,`keterangan`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'TRY-01','SM','II','1','KP CLG-KP CABANG / SORE','500','27580','2','2019-02-28','2','2','REG 1 Ubah','2020-08-30 17:39:02','2020-08-31 00:59:20',NULL),
(2,'TRY-02','CLG','V','2','KP CLG-KP SG-PDG-RKS 1','100','6135','1','2021-02-28','2','4','REG V','2020-08-31 00:02:21','2020-08-31 00:02:21',NULL),
(3,'TRY-03','CLG','V','3','KP SG-PDC TAMBUN (MLM)','298','4180','1','2021-02-28','6','6','REG V','2020-08-31 00:14:02','2020-08-31 00:14:02',NULL),
(4,'TRY-04','CLG','V','4','KP SG-PDC TAMBUN (Pagi)','298','4180','1','2021-02-28','6','6','REG V','2020-08-31 00:16:59','2020-08-31 00:16:59',NULL),
(5,'TRY-05','CLG','V','5','CLG-SR-JKTSH (Pagi)','216','4600','1','2020-08-31','4','4','REG V','2020-08-31 00:19:20','2020-08-31 00:36:26',NULL);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(250) NOT NULL,
  `name` varchar(100) NOT NULL,
  `level` int(1) DEFAULT 2,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`id`,`username`,`password`,`name`,`level`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'99087678','8752195d604d356af0a18ffd36d25568','Supir 1',2,'2020-08-27 13:09:57','2020-08-30 11:21:38',NULL),
(2,'admin','164842ce17fb7a7925f8b7baa3b9a00a','Administrator',1,'2020-08-27 13:10:49','2020-08-30 11:21:43',NULL),
(3,'asdfasd','9a6d548e6b27387ab56f5f76c5b0732f','Joni',2,'2020-08-30 05:52:49','2020-08-30 17:01:17',NULL);

/*Table structure for table `v_r7` */

DROP TABLE IF EXISTS `v_r7`;

/*!50001 DROP VIEW IF EXISTS `v_r7` */;
/*!50001 DROP TABLE IF EXISTS `v_r7` */;

/*!50001 CREATE TABLE  `v_r7`(
 `id` int(5) unsigned ,
 `kode_trayek` varchar(100) ,
 `nomor` varchar(100) ,
 `jumlah` int(11) ,
 `berat` float ,
 `home_base` varchar(100) ,
 `nama` varchar(100) ,
 `nopol` varchar(100) ,
 `type` varchar(100) ,
 `tahun` varchar(5) ,
 `trayek` varchar(250) ,
 `plpi` varchar(250) ,
 `harga_perkm` varchar(250) ,
 `jumlah_kbm` varchar(250) ,
 `akhir_pks` date ,
 `kap_pks` varchar(250) ,
 `kap_real` varchar(250) ,
 `keterangan` varchar(250) ,
 `username` varchar(100) ,
 `name` varchar(100) ,
 `created_at` timestamp ,
 `year` int(4) ,
 `month` int(2) ,
 `day` int(2) 
)*/;

/*Table structure for table `v_trayek` */

DROP TABLE IF EXISTS `v_trayek`;

/*!50001 DROP VIEW IF EXISTS `v_trayek` */;
/*!50001 DROP TABLE IF EXISTS `v_trayek` */;

/*!50001 CREATE TABLE  `v_trayek`(
 `id` int(5) unsigned ,
 `kode` varchar(25) ,
 `kode_base` varchar(5) ,
 `home_base` varchar(100) ,
 `alamat_base` varchar(200) ,
 `kode_regional` varchar(10) ,
 `nama` varchar(100) ,
 `alamat_regional` varchar(200) ,
 `kode_mobil` varchar(10) ,
 `nopol` varchar(100) ,
 `type` varchar(100) ,
 `tahun` varchar(5) ,
 `trayek` varchar(250) ,
 `plpi` varchar(250) ,
 `harga_perkm` varchar(250) ,
 `jumlah_kbm` varchar(250) ,
 `akhir_pks` date ,
 `kap_pks` varchar(250) ,
 `kap_real` varchar(250) ,
 `keterangan` varchar(250) ,
 `deleted_at` timestamp 
)*/;

/*View structure for view v_r7 */

/*!50001 DROP TABLE IF EXISTS `v_r7` */;
/*!50001 DROP VIEW IF EXISTS `v_r7` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`epiz_26628382`@`sql109.epizy.com` SQL SECURITY DEFINER VIEW `v_r7` AS select `r7`.`id` AS `id`,`r7`.`kode_trayek` AS `kode_trayek`,`r7`.`nomor` AS `nomor`,`r7`.`jumlah` AS `jumlah`,`r7`.`berat` AS `berat`,`v_trayek`.`home_base` AS `home_base`,`v_trayek`.`nama` AS `nama`,`v_trayek`.`nopol` AS `nopol`,`v_trayek`.`type` AS `type`,`v_trayek`.`tahun` AS `tahun`,`v_trayek`.`trayek` AS `trayek`,`v_trayek`.`plpi` AS `plpi`,`v_trayek`.`harga_perkm` AS `harga_perkm`,`v_trayek`.`jumlah_kbm` AS `jumlah_kbm`,`v_trayek`.`akhir_pks` AS `akhir_pks`,`v_trayek`.`kap_pks` AS `kap_pks`,`v_trayek`.`kap_real` AS `kap_real`,`v_trayek`.`keterangan` AS `keterangan`,`users`.`username` AS `username`,`users`.`name` AS `name`,`r7`.`created_at` AS `created_at`,year(`r7`.`created_at`) AS `year`,month(`r7`.`created_at`) AS `month`,dayofmonth(`r7`.`created_at`) AS `day` from ((`r7` left join `users` on(`r7`.`user_id` = `users`.`username`)) left join `v_trayek` on(`r7`.`kode_trayek` = `v_trayek`.`kode`)) */;

/*View structure for view v_trayek */

/*!50001 DROP TABLE IF EXISTS `v_trayek` */;
/*!50001 DROP VIEW IF EXISTS `v_trayek` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`epiz_26628382`@`sql109.epizy.com` SQL SECURITY DEFINER VIEW `v_trayek` AS select `trayek`.`id` AS `id`,`trayek`.`kode` AS `kode`,`trayek`.`kode_base` AS `kode_base`,`home_base`.`home_base` AS `home_base`,`home_base`.`alamat` AS `alamat_base`,`trayek`.`kode_regional` AS `kode_regional`,`regional`.`nama` AS `nama`,`regional`.`alamat` AS `alamat_regional`,`trayek`.`kode_mobil` AS `kode_mobil`,`mobil`.`kode` AS `nopol`,`mobil`.`type` AS `type`,`mobil`.`tahun` AS `tahun`,`trayek`.`trayek` AS `trayek`,`trayek`.`plpi` AS `plpi`,`trayek`.`harga_perkm` AS `harga_perkm`,`trayek`.`jumlah_kbm` AS `jumlah_kbm`,`trayek`.`akhir_pks` AS `akhir_pks`,`trayek`.`kap_pks` AS `kap_pks`,`trayek`.`kap_real` AS `kap_real`,`trayek`.`keterangan` AS `keterangan`,`trayek`.`deleted_at` AS `deleted_at` from (((`trayek` left join `home_base` on(`trayek`.`kode_base` = `home_base`.`kode`)) join `regional` on(`trayek`.`kode_regional` = `regional`.`kode`)) left join `mobil` on(`trayek`.`kode_mobil` = `mobil`.`id`)) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
