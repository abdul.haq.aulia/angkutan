<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class R7 extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id'          => [
					'type'           => 'INT',
					'constraint'     => 5,
					'unsigned'       => true,
					'auto_increment' => true,
			],
			'kode_trayek'       => [
					'type'           => 'VARCHAR',
					'constraint'     => '100',
			],
			'user_id' => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'nomor' => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'jumlah' => [
				'type'           => 'INT',
				'constraint'     => '11',
			],
			'berat' => [
				'type'           => 'FLOAT',
				'constraint'     => '11',
			],
			'created_at' => [
				'type'           => 'TIMESTAMP',
			],
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('r7');
	}

	public function down()
	{
		$this->forge->dropTable('r7');
	}
}
