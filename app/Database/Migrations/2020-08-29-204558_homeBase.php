<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class HomeBase extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id'          => [
					'type'           => 'INT',
					'constraint'     => 5,
					'unsigned'       => true,
					'auto_increment' => true,
			],
			'kode'       => [
					'type'           => 'VARCHAR',
					'constraint'     => '100',
			],
			'home_base' => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'alamat' => [
				'type'           => 'VARCHAR',
				'constraint'     => '200',
			]
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('home_base');
	}

	public function down()
	{
		$this->forge->dropTable('home_base');
	}
}
