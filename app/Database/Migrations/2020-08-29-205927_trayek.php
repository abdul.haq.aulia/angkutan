<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Trayek extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id'          => [
					'type'           => 'INT',
					'constraint'     => 5,
					'unsigned'       => true,
					'auto_increment' => true,
			],
			'kode'       => [
					'type'           => 'VARCHAR',
					'constraint'     => '25',
			],
			'kode_supir'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '25',
			],
			'kode_mobil' => [
				'type'           => 'VARCHAR',
				'constraint'     => '10',
			],
			'kode_regional' => [
				'type'           => 'VARCHAR',
				'constraint'     => '10',
			],
			'kode_base' => [
				'type'           => 'VARCHAR',
				'constraint'     => '5',
			],
			'trayek_awal' => [
				'type'           => 'VARCHAR',
				'constraint'     => '250',
			],
			'trayek_akhir' => [
				'type'           => 'VARCHAR',
				'constraint'     => '250',
			],
			'plpi' => [
				'type'           => 'VARCHAR',
				'constraint'     => '250',
			],
			'harga_perkm' => [
				'type'           => 'VARCHAR',
				'constraint'     => '250',
			],
			'jumlah_kbm' => [
				'type'           => 'VARCHAR',
				'constraint'     => '250',
			],
			'akhir_pks' => [
				'type'           => 'DATE',
			],
			'kap_pks' => [
				'type'           => 'VARCHAR',
				'constraint'     => '250',
			],
			'kap_real' => [
				'type'           => 'VARCHAR',
				'constraint'     => '250',
			],
			'keterangan' => [
				'type'           => 'VARCHAR',
				'constraint'     => '250',
			],
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('trayek');
	}

	public function down()
	{
		$this->forge->dropTable('trayek');
	}
}
