<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Regional extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id'          => [
					'type'           => 'INT',
					'constraint'     => 5,
					'unsigned'       => true,
					'auto_increment' => true,
			],
			'kode'       => [
					'type'           => 'VARCHAR',
					'constraint'     => '100',
			],
			'nama' => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'alamat' => [
				'type'           => 'VARCHAR',
				'constraint'     => '200',
			]
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('regional');
	}

	public function down()
	{
		$this->forge->dropTable('regional');
	}
}
