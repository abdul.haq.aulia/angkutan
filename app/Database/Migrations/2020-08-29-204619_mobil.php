<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Mobil extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id'          => [
					'type'           => 'INT',
					'constraint'     => 5,
					'unsigned'       => true,
					'auto_increment' => true,
			],
			'kode'       => [
					'type'           => 'VARCHAR',
					'constraint'     => '100',
			],
			'type' => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'tahun' => [
				'type'           => 'VARCHAR',
				'constraint'     => '5',
			]
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('mobil');
	}

	public function down()
	{
		$this->forge->dropTable('mobil');
	}
}
