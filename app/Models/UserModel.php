<?php namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table         = 'users';
    protected $allowedFields = [
        'username', 'password', 'name', 'level', 'deleted_at'
    ];
    protected $returnType    = 'App\Entities\User';
    protected $useTimestamps = true;
}