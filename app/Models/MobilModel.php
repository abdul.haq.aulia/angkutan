<?php namespace App\Models;

use CodeIgniter\Model;

class MobilModel extends Model
{
    protected $table         = 'mobil';
    protected $allowedFields = [
        'kode', 'type', 'tahun', 'deleted_at'
    ];
    protected $returnType    = 'App\Entities\Mobil';
    protected $useTimestamps = true;
}