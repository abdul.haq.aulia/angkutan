<?php namespace App\Models;

use CodeIgniter\Model;

class TrayekModel extends Model
{
    protected $table         = 'trayek';
    protected $allowedFields = [
        'kode', 'kode_supir', 'trayek_awal', 'trayek_akhir', 'kode_base', 'kode_base_akhir', 'kode_regional', 'kode_mobil', 'jumlah_kbm', 'plpi', 'harga_perkm', 'harga_perkg', 'akhir_pks', 'kap_pks', 'kap_real', 'keterangan', 'deleted_at', 'keberangkatan', 'urutan'
    ];
    protected $returnType    = 'App\Entities\Trayek';
    protected $useTimestamps = true;

    // public function getAllTrayek() {
    //     $db      = \Config\Database::connect();
    //     $builder = $db->table('v_trayek')->where('deleted_at IS NULL', NULL);
    //     return $builder->get();
    // }

    public function getAllTrayek() {
        $db      = \Config\Database::connect();
        $builder = $db->table('trayek');
        $builder->select('`trayek`.`id`            AS `id`,
        `trayek`.`kode`          AS `kode`,
        `trayek`.`kode_supir`          AS `kode_supir`,
        `trayek`.`keberangkatan`          AS `keberangkatan`,
        `users`.`name`          AS `name`,
        `trayek`.`kode_base`     AS `kode_base`,
        `trayek`.`kode_base_akhir`     AS `kode_base_akhir`,
        hba.`home_base`  AS `home_base`,
        hba.`alamat`     AS `alamat_base`,
        hbak.`home_base`  AS `home_base_akhir`,
        hbak.`alamat`     AS `alamat_base_akhir`,
        `trayek`.`kode_regional` AS `kode_regional`,
        `regional`.`nama`        AS `nama`,
        `regional`.`alamat`      AS `alamat_regional`,
        `trayek`.`kode_mobil`    AS `kode_mobil`,
        `mobil`.`kode`           AS `nopol`,
        `mobil`.`type`           AS `type`,
        `mobil`.`tahun`          AS `tahun`,
        `trayek`.`trayek_awal`        AS `trayek_awal`,
        `trayek`.`trayek_akhir`        AS `trayek_akhir`,
        `trayek`.`plpi`          AS `plpi`,
        `trayek`.`harga_perkm`   AS `harga_perkm`,
        `trayek`.`harga_perkg`   AS `harga_perkg`,
        `trayek`.`jumlah_kbm`    AS `jumlah_kbm`,
        `trayek`.`akhir_pks`     AS `akhir_pks`,
        `trayek`.`kap_pks`       AS `kap_pks`,
        `trayek`.`kap_real`      AS `kap_real`,
        `trayek`.`keterangan`    AS `keterangan`,
        `trayek`.`deleted_at`    AS `deleted_at`');
        $builder->join('home_base hba', '`trayek`.`kode_base` = hba.`kode`', 'left');
        $builder->join('home_base hbak', '`trayek`.`kode_base_akhir` = hbak.`kode`', 'left');
        $builder->join('regional', '`trayek`.`kode_regional` = `regional`.`kode`');
        $builder->join('mobil', '`trayek`.`kode_mobil` = `mobil`.`id`', 'left');
        $builder->join('users', '`trayek`.`kode_supir` = `users`.`id`', 'left');
        $builder->where('`trayek`.`deleted_at` IS NULL', NULL);
        $query = $builder->get();
        return $query;
    }

    public function getTrayekBySupir($id_supir) {
        $db      = \Config\Database::connect();
        $builder = $db->table('trayek');
        $builder->select('`trayek`.`id`            AS `id`,
        `trayek`.`kode`          AS `kode`,
        `trayek`.`urutan`          AS `urutan`,
        `trayek`.`kode_supir`          AS `kode_supir`,
        `trayek`.`keberangkatan`          AS `keberangkatan`,
        `users`.`name`          AS `name`,
        `trayek`.`kode_base`     AS `kode_base`,
        `trayek`.`kode_base_akhir`     AS `kode_base_akhir`,
        hba.`home_base`  AS `home_base`,
        hba.`alamat`     AS `alamat_base`,
        hbak.`home_base`  AS `home_base_akhir`,
        hbak.`alamat`     AS `alamat_base_akhir`,
        `trayek`.`kode_regional` AS `kode_regional`,
        `regional`.`nama`        AS `nama`,
        `regional`.`alamat`      AS `alamat_regional`,
        `trayek`.`kode_mobil`    AS `kode_mobil`,
        `mobil`.`kode`           AS `nopol`,
        `mobil`.`type`           AS `type`,
        `mobil`.`tahun`          AS `tahun`,
        `trayek`.`trayek_awal`        AS `trayek_awal`,
        `trayek`.`trayek_akhir`        AS `trayek_akhir`,
        `trayek`.`plpi`          AS `plpi`,
        `trayek`.`harga_perkm`   AS `harga_perkm`,
        `trayek`.`harga_perkg`   AS `harga_perkg`,
        `trayek`.`jumlah_kbm`    AS `jumlah_kbm`,
        `trayek`.`akhir_pks`     AS `akhir_pks`,
        `trayek`.`kap_pks`       AS `kap_pks`,
        `trayek`.`kap_real`      AS `kap_real`,
        `trayek`.`keterangan`    AS `keterangan`,
        `trayek`.`deleted_at`    AS `deleted_at`');
        $builder->join('home_base hba', '`trayek`.`kode_base` = hba.`kode`', 'left');
        $builder->join('home_base hbak', '`trayek`.`kode_base` = hbak.`kode`', 'left');
        $builder->join('regional', '`trayek`.`kode_regional` = `regional`.`kode`');
        $builder->join('mobil', '`trayek`.`kode_mobil` = `mobil`.`id`', 'left');
        $builder->join('users', '`trayek`.`kode_supir` = `users`.`id`', 'left');
        $builder->orderBy('urutan', 'asc');
        $query = $builder->where('kode_supir', $id_supir)->get();
        return $query;
    }

    public function getRecapTrayek() {
        $db      = \Config\Database::connect();
        $builder = $db->table('trayek');
        $builder->select('hba.`kode`  AS `kode_awal`,
        hba.`home_base`  AS `home_base`,
        hbak.`kode`  AS `kode_akhir`,
        hbak.`home_base`  AS `home_base_akhir`,
        `regional`.`kode`        AS `kode_regional`,
        `regional`.`nama`        AS `nama_regional`,
        count(*) as total');
        $builder->join('home_base hba', '`trayek`.`kode_base` = hba.`kode`', 'left');
        $builder->join('home_base hbak', '`trayek`.`kode_base_akhir` = hbak.`kode`', 'left');
        $builder->join('regional', '`trayek`.`kode_regional` = `regional`.`kode`');
        // $builder->join('users', '`trayek`.`kode_supir` = `users`.`id`', 'left');
        $builder->where('`trayek`.`deleted_at` IS NULL', NULL);
        $builder->groupBy('`hba`.`home_base`, hba.`kode`, hbak.`kode`, `hbak`.`home_base`, `regional`.`nama`, `regional`.`kode`');
        $query = $builder->get();
        return $query;
    }

    public function getDetailTrayek($kode_awal, $kode_akhir, $kode_regional) {
        $db      = \Config\Database::connect();
        $builder = $db->table('trayek');
        $builder->select('`trayek`.`id`            AS `id`,
        `trayek`.`kode`          AS `kode`,
        `trayek`.`urutan`          AS `urutan`,
        `trayek`.`kode_supir`          AS `kode_supir`,
        `trayek`.`keberangkatan`          AS `keberangkatan`,
        `users`.`name`          AS `name`,
        `trayek`.`kode_base`     AS `kode_base`,
        `trayek`.`kode_base_akhir`     AS `kode_base_akhir`,
        hba.`home_base`  AS `home_base`,
        hba.`alamat`     AS `alamat_base`,
        hbak.`home_base`  AS `home_base_akhir`,
        hbak.`alamat`     AS `alamat_base_akhir`,
        `trayek`.`kode_regional` AS `kode_regional`,
        `regional`.`nama`        AS `nama`,
        `regional`.`alamat`      AS `alamat_regional`,
        `trayek`.`kode_mobil`    AS `kode_mobil`,
        `mobil`.`kode`           AS `nopol`,
        `mobil`.`type`           AS `type`,
        `mobil`.`tahun`          AS `tahun`,
        `trayek`.`trayek_awal`        AS `trayek_awal`,
        `trayek`.`trayek_akhir`        AS `trayek_akhir`,
        `trayek`.`plpi`          AS `plpi`,
        `trayek`.`harga_perkm`   AS `harga_perkm`,
        `trayek`.`harga_perkg`   AS `harga_perkg`,
        `trayek`.`jumlah_kbm`    AS `jumlah_kbm`,
        `trayek`.`akhir_pks`     AS `akhir_pks`,
        `trayek`.`kap_pks`       AS `kap_pks`,
        `trayek`.`kap_real`      AS `kap_real`,
        `trayek`.`keterangan`    AS `keterangan`,
        `trayek`.`deleted_at`    AS `deleted_at`');
        $builder->join('home_base hba', '`trayek`.`kode_base` = hba.`kode`', 'left');
        $builder->join('home_base hbak', '`trayek`.`kode_base_akhir` = hbak.`kode`', 'left');
        $builder->join('regional', '`trayek`.`kode_regional` = `regional`.`kode`');
        $builder->join('mobil', '`trayek`.`kode_mobil` = `mobil`.`id`', 'left');
        $builder->join('users', '`trayek`.`kode_supir` = `users`.`id`', 'left');
        $builder->where('`trayek`.`deleted_at` IS NULL', NULL);
        $builder->where('`trayek`.`kode_base`', $kode_awal);
        $builder->where('`trayek`.`kode_base_akhir`', $kode_akhir);
        $builder->where('`trayek`.`kode_regional`', $kode_regional);
        $builder->orderBy('`users`.`name`', 'urutan', 'ASC');
        $query = $builder->get();
        return $query;
    }
}