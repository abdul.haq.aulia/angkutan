<?php namespace App\Models;

use CodeIgniter\Model;

class NodesModel extends Model
{
    protected $table         = 'nodes';
    protected $allowedFields = [
        'name', 'description', 'deleted_at'
    ];
    protected $returnType    = 'App\Entities\Nodes';
    protected $useTimestamps = true;
}