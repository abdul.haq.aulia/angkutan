<?php namespace App\Models;

use CodeIgniter\Model;

class R7Model extends Model
{
    protected $table         = 'r7';
    protected $allowedFields = [
        'kode_trayek', 'user_id', 'nomor', 'jumlah', 'berat', 'deleted_at'
    ];
    protected $returnType    = 'App\Entities\R7';
    protected $useTimestamps = true;

    // public function getDataByUser($user_id) {
    //     $db      = \Config\Database::connect();
    //     $builder = $db->table('v_r7');
    //     $result = [];
    //     if (session()->get('level') == 2) {
    //         $result = $builder->where('username', $user_id)->get();
    //     }

    //     if (session()->get('level') == 1) {
    //         $result = $builder->get();
    //     }
    //     return $result;
    // }

    public function getDataByUser($user_id) {
        $db      = \Config\Database::connect();
        $builder = $db->table('r7');
        $builder->select('`r7`.`id`
        , `r7`.`kode_trayek`
        , `r7`.`nomor`
        , `r7`.`jumlah`
        , `r7`.`berat`
        , `r7`.`created_at`
        , `home_base`.`home_base`
        , `regional`.`nama`
        , `mobil`.`kode` AS `nopol`
        , `mobil`.`type`
        , `mobil`.`tahun`
        , `trayek`.`trayek_awal`
        , `trayek`.`trayek_akhir`
        , `trayek`.`plpi`
        , `trayek`.`harga_perkm`
        , `trayek`.`harga_perkg`
        , `trayek`.`jumlah_kbm`
        , `trayek`.`akhir_pks`
        , `trayek`.`kap_pks`
        , `trayek`.`kap_real`
        , `trayek`.`keterangan`
        , `users`.`username`
        , `users`.`name`,
        YEAR(`r7`.`created_at`)  AS `year`,
      MONTH(`r7`.`created_at`) AS `month`,
      DAYOFMONTH(`r7`.`created_at`) AS `day`');
        $builder->join('trayek', '`r7`.`kode_trayek` = `trayek`.`kode`', 'left');
        $builder->join('users', '`r7`.`user_id` = `users`.`username`', 'left');
        $builder->join('home_base', '`trayek`.`kode_base` = `home_base`.`kode`', 'left');
        $builder->join('mobil', '`trayek`.`kode_mobil` = `mobil`.`id`', 'left');
        $builder->join('regional', '`trayek`.`kode_regional` = `regional`.`kode`');
        $result = [];
        if (session()->get('level') == 2) {
            $result = $builder->where('`users`.`username`', $user_id)->get();
        }

        if (session()->get('level') == 1) {
            $result = $builder->get();
        }
        return $result;
    }

    // public function getDataByUserNow($user_id) {
    //     $db      = \Config\Database::connect();
    //     $builder = $db->table('v_r7');
    //     return $builder->where('username', $user_id)->where('year', date('Y'))->where('month', date('m'))->where('day', date('d'))->get();
    // }

    public function getDataByUserNow($user_id) {
        $db      = \Config\Database::connect();
        $builder = $db->table('r7');
        $builder->select('`r7`.`id`
        , `r7`.`kode_trayek`
        , `r7`.`nomor`
        , `r7`.`jumlah`
        , `r7`.`berat`
        , `r7`.`created_at`
        , `home_base`.`home_base`
        , `regional`.`nama`
        , `mobil`.`kode` AS `nopol`
        , `mobil`.`type`
        , `mobil`.`tahun`
        , `trayek`.`trayek_awal`
        , `trayek`.`trayek_akhir`
        , `trayek`.`plpi`
        , `trayek`.`harga_perkm`
        , `trayek`.`harga_perkg`
        , `trayek`.`jumlah_kbm`
        , `trayek`.`akhir_pks`
        , `trayek`.`kap_pks`
        , `trayek`.`kap_real`
        , `trayek`.`keterangan`
        , `users`.`username`
        , `users`.`name`,
        YEAR(`r7`.`created_at`)  AS `year`,
      MONTH(`r7`.`created_at`) AS `month`,
      DAYOFMONTH(`r7`.`created_at`) AS `day`');
        $builder->join('trayek', '`r7`.`kode_trayek` = `trayek`.`kode`', 'left');
        $builder->join('users', '`r7`.`user_id` = `users`.`username`', 'left');
        $builder->join('home_base', '`trayek`.`kode_base` = `home_base`.`kode`', 'left');
        $builder->join('mobil', '`trayek`.`kode_mobil` = `mobil`.`id`', 'left');
        $builder->join('regional', '`trayek`.`kode_regional` = `regional`.`kode`');
        $result = [];
        if (session()->get('level') == 2) {
            $result = $builder->where('`users`.`username`', $user_id)->where('YEAR(`r7`.`created_at`)', date('Y'))->where('MONTH(`r7`.`created_at`)', date('m'))->where('DAYOFMONTH(`r7`.`created_at`)', date('d'))->get();
        }

        if (session()->get('level') == 1) {
            $result = $builder->where('YEAR(`r7`.`created_at`)', date('Y'))->where('MONTH(`r7`.`created_at`)', date('m'))->where('DAYOFMONTH(`r7`.`created_at`)', date('d'))->get();
        }
        return $result;
    }

    // public function getDataById($id){
    //     $db      = \Config\Database::connect();
    //     $builder = $db->table('v_r7');
    //     return $builder->where('id', $id)->get();
    // }

    public function getDataById($id){
        $db      = \Config\Database::connect();
        $builder = $db->table('r7');
        $builder->select('`r7`.`id`
        , `r7`.`kode_trayek`
        , `r7`.`nomor`
        , `r7`.`jumlah`
        , `r7`.`berat`
        , `r7`.`created_at`
        , `home_base`.`home_base`
        , `regional`.`nama`
        , `mobil`.`kode` AS `nopol`
        , `mobil`.`type`
        , `mobil`.`tahun`
        , `trayek`.`trayek_awal`
        , `trayek`.`trayek_akhir`
        , `trayek`.`plpi`
        , `trayek`.`harga_perkm`
        , `trayek`.`harga_perkg`
        , `trayek`.`jumlah_kbm`
        , `trayek`.`akhir_pks`
        , `trayek`.`kap_pks`
        , `trayek`.`kap_real`
        , `trayek`.`keterangan`
        , `users`.`username`
        , `users`.`name`,
        YEAR(`r7`.`created_at`)  AS `year`,
      MONTH(`r7`.`created_at`) AS `month`,
      DAYOFMONTH(`r7`.`created_at`) AS `day`');
        $builder->join('trayek', '`r7`.`kode_trayek` = `trayek`.`kode`', 'left');
        $builder->join('users', '`r7`.`user_id` = `users`.`username`', 'left');
        $builder->join('home_base', '`trayek`.`kode_base` = `home_base`.`kode`', 'left');
        $builder->join('mobil', '`trayek`.`kode_mobil` = `mobil`.`id`', 'left');
        $builder->join('regional', '`trayek`.`kode_regional` = `regional`.`kode`');
        return $builder->where('`r7`.`id`', $id)->get();
    }

    // public function getDataByDate($start, $end){
    //     $db      = \Config\Database::connect();
        
    //     $builder = $db->table('v_r7');
    //     return $builder->where('DATE_FORMAT(created_at, "%Y-%m-%d") >=', date('Y-m-d',strtotime($start)))
    //                     ->where('DATE_FORMAT(created_at, "%Y-%m-%d") <=', date('Y-m-d',strtotime($end)))
    //                     ->where('username', session()->get('username'))->get();
    // }

    public function getDataByDate($start, $end){
        $db      = \Config\Database::connect();
        $builder = $db->table('r7');
        $builder->select('`r7`.`id`
        , `r7`.`kode_trayek`
        , `r7`.`nomor`
        , `r7`.`jumlah`
        , `r7`.`berat`
        , `r7`.`created_at`
        , `home_base`.`home_base`
        , `regional`.`nama`
        , `mobil`.`kode` AS `nopol`
        , `mobil`.`type`
        , `mobil`.`tahun`
        , `trayek`.`trayek_awal`
        , `trayek`.`trayek_akhir`
        , `trayek`.`plpi`
        , `trayek`.`harga_perkm`
        , `trayek`.`harga_perkg`
        , `trayek`.`jumlah_kbm`
        , `trayek`.`akhir_pks`
        , `trayek`.`kap_pks`
        , `trayek`.`kap_real`
        , `trayek`.`keterangan`
        , `users`.`username`
        , `users`.`name`,
        YEAR(`r7`.`created_at`)  AS `year`,
      MONTH(`r7`.`created_at`) AS `month`,
      DAYOFMONTH(`r7`.`created_at`) AS `day`');
        $builder->join('trayek', '`r7`.`kode_trayek` = `trayek`.`kode`', 'left');
        $builder->join('users', '`r7`.`user_id` = `users`.`username`', 'left');
        $builder->join('home_base', '`trayek`.`kode_base` = `home_base`.`kode`', 'left');
        $builder->join('mobil', '`trayek`.`kode_mobil` = `mobil`.`id`', 'left');
        $builder->join('regional', '`trayek`.`kode_regional` = `regional`.`kode`');
        $result = [];
        if (session()->get('level') == 2) {
            $result = $builder->where('DATE_FORMAT(`r7`.`created_at`, "%Y-%m-%d") >=', date('Y-m-d',strtotime($start)))
                    ->where('DATE_FORMAT(`r7`.`created_at`, "%Y-%m-%d") <=', date('Y-m-d',strtotime($end)))
                    ->where('`users`.`usernames`', session()->get('username'))->get();
        }

        if (session()->get('level') == 1) {
            $result = $builder->where('DATE_FORMAT(`r7`.`created_at`, "%Y-%m-%d") >=', date('Y-m-d',strtotime($start)))
                    ->where('DATE_FORMAT(`r7`.`created_at`, "%Y-%m-%d") <=', date('Y-m-d',strtotime($end)))
                    ->get();
        }
        return $result;
    }

    // public function getDataByMonth($year, $month){
    //     $db      = \Config\Database::connect();
    //     $builder = $db->table('v_r7');
    //     $result = [];
    //     if (session()->get('level') == 2) {
    //         $result = $builder->where('year', $year)->where('month', $month)->where('username', session()->get('username'))->get();
    //     }

    //     if (session()->get('level') == 1) {
    //         $result = $builder->where('year', $year)->where('month', $month)->get();
    //     }
    //     return $result;
    // }

    public function getDataByMonth($year, $month){
        $db      = \Config\Database::connect();
        $builder = $db->table('r7');
        $builder->select('`r7`.`id`
        , `r7`.`kode_trayek`
        , `r7`.`nomor`
        , `r7`.`jumlah`
        , `r7`.`berat`
        , `r7`.`created_at`
        , `home_base`.`home_base`
        , `regional`.`nama`
        , `mobil`.`kode` AS `nopol`
        , `mobil`.`type`
        , `mobil`.`tahun`
        , `trayek`.`trayek_awal`
        , `trayek`.`trayek_akhir`
        , `trayek`.`plpi`
        , `trayek`.`harga_perkm`
        , `trayek`.`harga_perkg`
        , `trayek`.`jumlah_kbm`
        , `trayek`.`akhir_pks`
        , `trayek`.`kap_pks`
        , `trayek`.`kap_real`
        , `trayek`.`keterangan`
        , `users`.`username`
        , `users`.`name`,
        YEAR(`r7`.`created_at`)  AS `year`,
      MONTH(`r7`.`created_at`) AS `month`,
      DAYOFMONTH(`r7`.`created_at`) AS `day`');
        $builder->join('trayek', '`r7`.`kode_trayek` = `trayek`.`kode`', 'left');
        $builder->join('users', '`r7`.`user_id` = `users`.`username`', 'left');
        $builder->join('home_base', '`trayek`.`kode_base` = `home_base`.`kode`', 'left');
        $builder->join('mobil', '`trayek`.`kode_mobil` = `mobil`.`id`', 'left');
        $builder->join('regional', '`trayek`.`kode_regional` = `regional`.`kode`');
        $result = [];
        if (session()->get('level') == 2) {
            $result = $builder->where('YEAR(`r7`.`created_at`)', $year)->where('MONTH(`r7`.`created_at`)', $month)->where('`users`.`username`', session()->get('username'))->get();
        }

        if (session()->get('level') == 1) {
            $result = $builder->where('YEAR(`r7`.`created_at`)', $year)->where('MONTH(`r7`.`created_at`)', $month)->get();
        }
        return $result;
    }
}