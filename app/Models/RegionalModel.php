<?php namespace App\Models;

use CodeIgniter\Model;

class RegionalModel extends Model
{
    protected $table         = 'regional';
    protected $allowedFields = [
        'kode', 'nama', 'alamat', 'deleted_at'
    ];
    protected $returnType    = 'App\Entities\Regional';
    protected $useTimestamps = true;
}