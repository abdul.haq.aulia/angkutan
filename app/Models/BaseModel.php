<?php namespace App\Models;

use CodeIgniter\Model;

class BaseModel extends Model
{
    protected $table         = 'home_base';
    protected $allowedFields = [
        'kode', 'home_base', 'alamat', 'deleted_at'
    ];
    protected $returnType    = 'App\Entities\HomeBase';
    protected $useTimestamps = true;
}